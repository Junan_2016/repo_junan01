package util;

import java.text.DecimalFormat;

public class Constantes {
	public final static String INDICADOR_MEDIA_MOVIL = "INDICADOR_MEDIA_MOVIL";
	public final static String INDICADOR_MOMENTUM = "INDICADOR_MOMENTUM";
	public final static String INDICADOR_ATR = "INDICADOR_ATR";
	public final static String INDICADOR_MACD = "INDICADOR_MACD";
	public final static String INDICADOR_RSI = "INDICADOR_RSI";
	public final static String INDICADOR_OBV = "INDICADOR_OBV";
	public final static String INDICADOR_STOCH = "INDICADOR_STOCH";
	public final static String INDICADOR_BOLLINGER_BANDS = "INDICADOR_BOLLINGER_BANDS";
}
