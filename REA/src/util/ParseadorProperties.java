package util;

public class ParseadorProperties {
	
	private static String delimitador = ",";
	private static String [] arregloString = null;
	
	public static String[] parsearPropeties(String properties) {
		
		arregloString = properties.split(delimitador);
		/*
		for(String elemento : arregloString){
			System.out.println(elemento);
		}
		*/
		return arregloString;		
	}

	public static double parsearStringIOL_Double(String originalValue,String atributo) {
		
		//Old => "AR$ 363.065,00"; => NEW: AR$ 363,065.00
		String newValue = null;
		newValue = originalValue.replace("AR$ ", "");
		newValue = newValue.replace(",", "");
		
		double valor = Double.parseDouble(newValue);
		
		System.out.println(atributo + " - originalValue: " + originalValue +  " - valor: " + valor);
		
		return valor;
	}
	

	public static double parsearCadenaCotizacionIOL_Double(String cadenaOriginal,String atributo) {
		
		//String cadenaOriginal = "{\"date\":\"2015-07-28T14:03:45.000Z\",\"price\":\"24.00\",\"moneda\":\"AR$\"}";
    	
    	String cadenaNew = null;
    	int indexBorrar = cadenaOriginal.indexOf(",");
    	String cadenaBorrar = cadenaOriginal.substring(0, indexBorrar+1);
    	cadenaNew = cadenaOriginal.replace(cadenaBorrar, "");
    	
    	indexBorrar = cadenaNew.indexOf(",");
    	cadenaBorrar = cadenaNew.substring(indexBorrar,cadenaNew.length());
    	cadenaNew = cadenaNew.replace(cadenaBorrar, "");
    	
    	indexBorrar = cadenaNew.indexOf(":");	
    	cadenaBorrar = cadenaNew.substring(0,indexBorrar+1);
    	cadenaNew = cadenaNew.replace(cadenaBorrar, "");
    	
    	cadenaNew = cadenaNew.replaceAll("\"", "");
    	
    	double valor2 = Double.parseDouble(cadenaNew);
    	
    	System.out.println(atributo + " - cadenaOriginal: " + cadenaOriginal + " - valor2: " + valor2);
    	
		return valor2;
	}
	
}
