package indicador;

import java.util.List;

import pojo.CalculoMetrica;
import pojo.Cotizacion;
import util.Constantes;

import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;



public class IndicadorATR {
	
    /**
     * 
     * @param listaCotizaciones
     * @return
     */
     
    public List <CalculoMetrica> calcular_ATR(List <Cotizacion> listaCotizaciones){
    	
    	if(listaCotizaciones == null || listaCotizaciones.size() == 0) return null;

    	int PERIODO_ATR = 14;
    	int DIAS_TOTALES = listaCotizaciones.size();
    	double[] precio_cierre_array = new double[DIAS_TOTALES];	//precio_cierre de cada fecha
        double[] resultado_array = new double[DIAS_TOTALES]; 		//se guardan el resultado de cada fecha
        double[] precio_high_array = new double[DIAS_TOTALES];
        double[] precio_low_array = new double[DIAS_TOTALES];
        
        MInteger indice_inicial_cargar_datos = new MInteger();
        MInteger longitud_array = new MInteger();
            	
    	Utils.cargaPreciosEnArray(precio_cierre_array, listaCotizaciones);
    	Utils.cargaPreciosHighEnArray(precio_high_array, listaCotizaciones);
    	Utils.cargaPreciosLowEnArray(precio_low_array, listaCotizaciones);
    	
    	Core c = new Core();
    	RetCode retCode = c.atr(0, (precio_cierre_array.length-1), precio_high_array, precio_low_array, precio_cierre_array, PERIODO_ATR, indice_inicial_cargar_datos, longitud_array, resultado_array);
         
        List <CalculoMetrica> listaCalculoMetricas = null;

        if (retCode == RetCode.Success) {
            listaCalculoMetricas = Utils.creacionlistaCalculoMetricasPrecios(
            		precio_cierre_array,
            		resultado_array,
            		Constantes.INDICADOR_ATR,
            		listaCotizaciones,
            		indice_inicial_cargar_datos
            );
        } else {
  	      System.out.println("Error");
  	    }
            
        return listaCalculoMetricas;
    }


    
}
