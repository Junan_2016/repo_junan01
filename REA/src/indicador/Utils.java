package indicador;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.tictactec.ta.lib.MInteger;

import pojo.CalculoMetrica;
import pojo.CalculoMetricaBand;
import pojo.CalculoMetricaSTOCH;

import pojo.Cotizacion;

public class Utils {
	
	/**
	 * 
	 * @param datosCalculo_array
	 * @param precio_cierre_array
	 * @param resultado_MACD_array
	 * @param resultado_MACDSignal_array
	 * @param resultado_MACDHist_array
	 * @param resultado_array
	 * @param inicio_array
	 * @param longitud_array
	 * @param nombre_indicador
	 * @param listaCotizaciones
	 * @return
	 */
	
	public static List<CalculoMetrica> creacionlistaCalculoMetricasMACD(
			double[] precio_cierre_array,
			double[] resultado_MACD_array,
			double[] resultado_MACDSignal_array,
			double[] resultado_MACDHist_array,
			String nombre_indicador,
			List<Cotizacion> listaCotizaciones,
			MInteger indice_inicial_cargar_datos
			) {
		
		List<CalculoMetrica> lista = new ArrayList<CalculoMetrica>();
        CalculoMetrica vCalculoMetrica = null;
        
        for (int i = 0; i<listaCotizaciones.size(); i++) {
        	vCalculoMetrica = new CalculoMetrica();
        	
        	int k = i - indice_inicial_cargar_datos.value;
    		if(k >= 0){
        		vCalculoMetrica.setFecha_apertura(listaCotizaciones.get(i).getFecha());
        		vCalculoMetrica.setPrecio_cierre(listaCotizaciones.get(i).getPrecio_cierre());
        		vCalculoMetrica.setIndicador_nombre(nombre_indicador);
            	vCalculoMetrica.setIndicador_valor(resultado_MACD_array[k]);
    		}
            lista.add(vCalculoMetrica);	
        }
/*	    
        System.out.println(
        		new SimpleDateFormat("yyyy-MM-dd").format(vCalculoMetrica.getFecha_apertura()) + " - " +
        		vCalculoMetrica.getIndicador_nombre() + ": " + 
        		vCalculoMetrica.getIndicador_valor());
*/        
	    return lista;
    }
		
    /**
     * 
     * @param precio_cierre_array
     * @param resultado_array
     * @param inicio_array
     * @param longitud_array
     * @param listaCotizaciones
     * @return
     */
	public static List<CalculoMetrica> creacionlistaCalculoMetricasPrecios(
			double[] precio_cierre_array,
			double[] resultado_array,
			String nombre_indicador,
			List<Cotizacion> listaCotizaciones,
			MInteger indice_inicial_cargar_datos
			) {
		
		List<CalculoMetrica> lista = new ArrayList<CalculoMetrica>();
        CalculoMetrica vCalculoMetrica = null;
        
        //Carga util
        for (int i=0; i<listaCotizaciones.size(); i++) {
        		vCalculoMetrica = new CalculoMetrica();
        		
        		int k = i - indice_inicial_cargar_datos.value;
        		if(k >= 0){
            		vCalculoMetrica.setFecha_apertura(listaCotizaciones.get(i).getFecha());
            		vCalculoMetrica.setPrecio_cierre(listaCotizaciones.get(i).getPrecio_cierre());
            		vCalculoMetrica.setIndicador_nombre(nombre_indicador);
                	vCalculoMetrica.setIndicador_valor(resultado_array[k]); 
        		}
            	lista.add(vCalculoMetrica);	
        }
/*	    
        System.out.println(
        		new SimpleDateFormat("yyyy-MM-dd").format(vCalculoMetrica.getFecha_apertura()) + " - " + 
        		vCalculoMetrica.getIndicador_nombre() + ": " +
        		vCalculoMetrica.getIndicador_valor() + 
        		" - Precio: " + vCalculoMetrica.getPrecio_cierre());
*/        
	    return lista;
    }
	
	
	/**
     * 
     * @param precio_cierre_array
     * @param resultado_array
     * @param inicio_array
     * @param longitud_array
     * @param listaCotizaciones
     * @return
     */
	public static List<CalculoMetricaSTOCH> creacionlistaCalculoMetricasStoch(
			double[] precio_cierre_array,
			double[] resultado_outSlowK,
			double[] resultado_outSlowD,
			String nombre_indicador,
			List<Cotizacion> listaCotizaciones,
			MInteger indice_inicial_cargar_datos
			) {
		
		List<CalculoMetricaSTOCH> lista = new ArrayList<CalculoMetricaSTOCH>();
        CalculoMetricaSTOCH vCalculoMetricaSTOCH = null;
        
        //Carga util
        for (int i=0; i<listaCotizaciones.size(); i++) {
        	vCalculoMetricaSTOCH = new CalculoMetricaSTOCH();
        		
        		int k = i - indice_inicial_cargar_datos.value;
        		if(k >= 0){
        			vCalculoMetricaSTOCH.setFecha_apertura(listaCotizaciones.get(i).getFecha());
        			vCalculoMetricaSTOCH.setPrecio_cierre(listaCotizaciones.get(i).getPrecio_cierre());
        			vCalculoMetricaSTOCH.setIndicador_nombre(nombre_indicador);
            		
        			vCalculoMetricaSTOCH.setValor_main(resultado_outSlowK[k]);
        			vCalculoMetricaSTOCH.setValor_signal(resultado_outSlowD[k]); 
        		}
            	lista.add(vCalculoMetricaSTOCH);	
        }
	    
        if(false){        
    		System.out.println("Ultima Cotizacion Valida ->>" +
        		new SimpleDateFormat("yyyy-MM-dd").format(vCalculoMetricaSTOCH.getFecha_apertura()) + " - " + 
        		vCalculoMetricaSTOCH.getIndicador_nombre() + ": " +
        		vCalculoMetricaSTOCH.getIndicador_valor() + 
        		" - Precio: " + vCalculoMetricaSTOCH.getPrecio_cierre());
        } 
        
        return lista;
    }
	
	/**
     * 
     * @param precio_cierre_array
     * @param resultado_array
     * @param inicio_array
     * @param longitud_array
     * @param listaCotizaciones
     * @return
     */
	public static List<CalculoMetricaBand> creacionlistaCalculoMetricasBollingerBands(
			double[] precio_cierre_array,
			double[] resultado_outRealUpperBand,
			double[] resultado_outRealMiddleBand,
			double[] resultado_outRealLowerBand,
			String nombre_indicador,
			List<Cotizacion> listaCotizaciones,
			MInteger indice_inicial_cargar_datos
			) {
		
		List<CalculoMetricaBand> lista = new ArrayList<CalculoMetricaBand>();
        CalculoMetricaBand vCalculoMetricaBand = null;
        
        //Carga util
        for (int i=0; i<listaCotizaciones.size(); i++) {
        	vCalculoMetricaBand = new CalculoMetricaBand();
        		
        		int k = i - indice_inicial_cargar_datos.value;
        		if(k >= 0){
        			vCalculoMetricaBand.setFecha_apertura(listaCotizaciones.get(i).getFecha());
        			vCalculoMetricaBand.setPrecio_cierre(listaCotizaciones.get(i).getPrecio_cierre());
        			vCalculoMetricaBand.setIndicador_nombre(nombre_indicador);
        			vCalculoMetricaBand.setValor_banda_superior(resultado_outRealUpperBand[k]);
        			vCalculoMetricaBand.setValor_banda_media(resultado_outRealMiddleBand[k]);
        			vCalculoMetricaBand.setValor_banda_inferior(resultado_outRealLowerBand[k]);
        		}
            	lista.add(vCalculoMetricaBand);	
        }
	    
        if(false){        
    		System.out.println("Ultima Cotizacion Valida ->> " +
        		new SimpleDateFormat("yyyy-MM-dd").format(vCalculoMetricaBand.getFecha_apertura()) + " - " + 
        		vCalculoMetricaBand.getIndicador_nombre() + 
        		" - getValor_banda_inferior: " + vCalculoMetricaBand.getValor_banda_inferior() +
        		" - getValor_banda_media: " + vCalculoMetricaBand.getValor_banda_media() + 
        		" - getValor_banda_superior: " + vCalculoMetricaBand.getValor_banda_superior() 
        		);
        } 
        
        return lista;
    }

	
    /**
     * 
     * @param precio_cierre_array
     * @param resultado_array
     * @param inicio_array
     * @param longitud_array
     * @param listaCotizaciones
     * @return
     */
	public static List<CalculoMetrica> creacionlistaCalculoMetricasOBV(
			double[] resultado_array,
			String nombre_indicador,
			List<Cotizacion> listaCotizaciones,
			MInteger indice_inicial_cargar_datos
			) {
		
		List<CalculoMetrica> lista = new ArrayList<CalculoMetrica>();
        CalculoMetrica vCalculoMetrica = null;
        for (int i=0; i<listaCotizaciones.size(); i++) {
        	vCalculoMetrica = new CalculoMetrica();
        	
        	int k = i - indice_inicial_cargar_datos.value;
    		if(k >= 0){
        		vCalculoMetrica.setFecha_apertura(listaCotizaciones.get(i).getFecha());
        		vCalculoMetrica.setPrecio_cierre(listaCotizaciones.get(i).getPrecio_cierre());
        		vCalculoMetrica.setIndicador_nombre(nombre_indicador);
            	vCalculoMetrica.setIndicador_valor(resultado_array[k]);
    		}
            lista.add(vCalculoMetrica);	
        }
/*	    
        System.out.println(
        		new SimpleDateFormat("yyyy-MM-dd").format(vCalculoMetrica.getFecha_apertura()) + " - " +
        		vCalculoMetrica.getIndicador_nombre() + ": " + 
        		vCalculoMetrica.getIndicador_valor());
*/      
	    return lista;
    
    }
	
	
	/**
	 * 
	 * @param precio_cierre_array
	 * @param listaCotizaciones
	 */
	public static void cargaPreciosEnArray(double[] precio_cierre_array, List<Cotizacion> listaCotizaciones) {
	
		for (int j=0; j<listaCotizaciones.size(); j++){
			precio_cierre_array[j] = listaCotizaciones.get(j).getPrecio_cierre();
		}
	}
	
	/**
	 * 
	 * @param precio_high_array
	 * @param listaCotizaciones
	 */
	public static void cargaPreciosHighEnArray(double[] precio_high_array, List<Cotizacion> listaCotizaciones) {
		for (int j=0; j<listaCotizaciones.size(); j++){
			precio_high_array[j] = listaCotizaciones.get(j).getPrecio_maximo();
		}
	}
	
	/**
	 * 
	 * @param precio_low_array
	 * @param listaCotizaciones
	 */
	public static void cargaPreciosLowEnArray(double[] precio_low_array, List<Cotizacion> listaCotizaciones) {
		for (int j=0; j<listaCotizaciones.size(); j++){
			precio_low_array[j] = listaCotizaciones.get(j).getPrecio_minimo();
		}
	}

	/**
	 * 
	 * @param volumen_array
	 * @param listaCotizaciones
	 */
	public static void cargaVolumenEnArray(double[] volumen_array, List<Cotizacion> listaCotizaciones){
		if (volumen_array.length > listaCotizaciones.size()) return;
		
		for (int j=0; j<volumen_array.length; j++){
			volumen_array[j] = listaCotizaciones.get(j).getVolumen();
		}
		
	}
	
}
