package indicador;

import java.util.List;
import pojo.CalculoMetrica;
import pojo.Cotizacion;
import util.Constantes;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;


public class IndicadorConvergenciaDivergenciaMediaMovil {
	
    /**
     * 
     * @param listaCotizaciones
     * @return
     */
     
    public List <CalculoMetrica> calcular_iMACD(List <Cotizacion> listaCotizaciones){
    	
    	if(listaCotizaciones == null || listaCotizaciones.size() == 0) return null;

    	int DIAS_TOTALES = listaCotizaciones.size();
    	    	
    	double[] precio_cierre_array = new double[DIAS_TOTALES];	//precio_cierre de cada fecha
        double[] resultado_MACD_array = new double[DIAS_TOTALES]; 		//se guardan el resultado de cada fecha
        double[] resultado_MACDSignal_array = new double[DIAS_TOTALES]; 
        double[] resultado_MACDHist_array = new double[DIAS_TOTALES]; 
        
        MInteger indice_inicial_cargar_datos = new MInteger();
        MInteger longitud_array = new MInteger();
            	
    	Utils.cargaPreciosEnArray(precio_cierre_array, listaCotizaciones);
    	    	
    	Core c = new Core();
        RetCode retCode = c.macd(
        		0,
        		(precio_cierre_array.length-1), 
        		precio_cierre_array,
        		9,
        		18,
        		9,
        		indice_inicial_cargar_datos, 
        		longitud_array, 
        		resultado_MACD_array,
        		resultado_MACDSignal_array,
        		resultado_MACDHist_array
        		);
   	   
        List <CalculoMetrica> listaCalculoMetricas = null;

        if (retCode == RetCode.Success) {
            //System.out.println("Calcular_iMACD - inicio_array:" + inicio_array.value);
            //System.out.println("Calcular_iMACD - longitud_array:" + longitud_array.value);
			
            listaCalculoMetricas = Utils.creacionlistaCalculoMetricasMACD(
            		precio_cierre_array,
            		resultado_MACD_array,
            		resultado_MACDSignal_array,
            		resultado_MACDHist_array,
            		Constantes.INDICADOR_MACD,
            		listaCotizaciones,
            		indice_inicial_cargar_datos
            );
        } else {
  	      System.out.println("Error");
  	    }
            
        return listaCalculoMetricas;
    }


    
}
