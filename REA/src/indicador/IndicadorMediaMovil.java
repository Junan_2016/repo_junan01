package indicador;

import java.util.List;

import pojo.CalculoMetrica;
import pojo.Cotizacion;
import util.Constantes;

import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;



public class IndicadorMediaMovil {
	
    /**
     * 
     * @param listaCotizaciones
     * @return
     */
     
    public List <CalculoMetrica> calcular_EMA(List <Cotizacion> listaCotizaciones,int PERIODO_IMA){
    	
    	if(listaCotizaciones == null || listaCotizaciones.size() == 0) return null;

    	int DIAS_TOTALES = listaCotizaciones.size();
    	double[] precio_cierre_array = new double[DIAS_TOTALES];	//precio_cierre de cada fecha
        double[] resultado_array = new double[DIAS_TOTALES]; 		//se guardan el resultado de cada fecha
        
        MInteger indice_inicial_cargar_datos = new MInteger();
        MInteger longitud_array = new MInteger();
            	
    	Utils.cargaPreciosEnArray(precio_cierre_array, listaCotizaciones);
    	
    	Core c = new Core();
        RetCode retCode = c.ema(0, (precio_cierre_array.length-1), precio_cierre_array, PERIODO_IMA, indice_inicial_cargar_datos, longitud_array, resultado_array);
   	   
        List <CalculoMetrica> listaCalculoMetricas = null;

        if (retCode == RetCode.Success) {
            //System.out.println("Calcular_iMA - inicio_array:" + inicio_array.value);
            //System.out.println("Calcular_iMA - longitud_array:" + longitud_array.value);

            listaCalculoMetricas = Utils.creacionlistaCalculoMetricasPrecios(
            		precio_cierre_array,
            		resultado_array,
            		Constantes.INDICADOR_MEDIA_MOVIL,
            		listaCotizaciones,
            		indice_inicial_cargar_datos
            );
        } else {
  	      System.out.println("Error");
  	    }
            
        return listaCalculoMetricas;
    }


    
}
