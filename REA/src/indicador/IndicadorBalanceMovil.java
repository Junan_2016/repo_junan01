package indicador;

import java.util.List;

import pojo.CalculoMetrica;
import pojo.Cotizacion;
import util.Constantes;

import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;



public class IndicadorBalanceMovil {
	
    /**
     * 
     * @param listaCotizaciones
     * @return
     */
     
    public List <CalculoMetrica> calcular_IOBL(List <Cotizacion> listaCotizaciones){
    	
    	if(listaCotizaciones == null || listaCotizaciones.size() == 0) return null;

    	int DIAS_TOTALES = listaCotizaciones.size();
   	    	
    	double[] volumen_array = new double[DIAS_TOTALES];			//volumen de cada fecha
        double[] resultado_array = new double[DIAS_TOTALES]; 		//se guardan el resultado de cada fecha
        double[] precio_array = new double[DIAS_TOTALES];	
        
        MInteger indice_inicial_cargar_datos = new MInteger();
        MInteger longitud_array = new MInteger();
            	
    	Utils.cargaVolumenEnArray(volumen_array, listaCotizaciones);
    	Utils.cargaPreciosEnArray(precio_array, listaCotizaciones);
    	
    	Core c = new Core();
        RetCode retCode = c.obv(0, (volumen_array.length-1), precio_array, volumen_array, indice_inicial_cargar_datos, longitud_array, resultado_array);
   	   
        List <CalculoMetrica> listaCalculoMetricas = null;

        if (retCode == RetCode.Success) {
            //System.out.println("Calcular_iOBV - inicio_array:" + inicio_array.value);
            //System.out.println("Calcular_iOBV - longitud_array:" + longitud_array.value);

            listaCalculoMetricas = Utils.creacionlistaCalculoMetricasOBV(
            		resultado_array,
            		Constantes.INDICADOR_OBV,
            		listaCotizaciones,
            		indice_inicial_cargar_datos
            );
        } else {
  	      System.out.println("Error");
  	    }
            
        return listaCalculoMetricas;
    }


    
}
