package indicador;

import java.util.List;
import pojo.CalculoMetricaSTOCH;
import pojo.Cotizacion;
import util.Constantes;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MAType;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

public class IndicadorStochastic {
	
    /**
     * 
     * @param listaCotizaciones
     * @return
     */
     
    public List <CalculoMetricaSTOCH> calcular_STOCH(List <Cotizacion> listaCotizaciones){
    	
    	if(listaCotizaciones == null || listaCotizaciones.size() == 0) return null;

    	int DIAS_TOTALES = listaCotizaciones.size();
    	double[] array_inClose = new double[DIAS_TOTALES];	//close de cada fecha
    	double[] array_inHigh = new double[DIAS_TOTALES];	//high de cada fecha
    	double[] array_inLow = new double[DIAS_TOTALES];	//low de cada fecha
        double[] resultado_outSlowK = new double[DIAS_TOTALES]; 		//se guardan el resultado de cada fecha
        double[] resultado_outSlowD = new double[DIAS_TOTALES]; 
        
        MInteger indice_inicial_cargar_datos = new MInteger();
        MInteger longitud_array = new MInteger();
            	
    	Utils.cargaPreciosEnArray(array_inClose, listaCotizaciones);
    	Utils.cargaPreciosHighEnArray(array_inHigh, listaCotizaciones);
    	Utils.cargaPreciosLowEnArray(array_inLow, listaCotizaciones);
    	
    	Core c = new Core();
    	RetCode retCode = c.stoch(0, (array_inClose.length-1), array_inHigh, array_inLow, array_inClose, 100, 1, MAType.Ema, 20, MAType.Ema, indice_inicial_cargar_datos, longitud_array, resultado_outSlowK, resultado_outSlowD);
   	    
        List <CalculoMetricaSTOCH> listaCalculoMetricas = null;

        if (retCode == RetCode.Success) {
            //System.out.println("Calcular_iMA - inicio_array:" + inicio_array.value);
            //System.out.println("Calcular_iMA - longitud_array:" + longitud_array.value);

            listaCalculoMetricas = Utils.creacionlistaCalculoMetricasStoch(
            		array_inClose,
            		resultado_outSlowK,
            		resultado_outSlowD,
            		Constantes.INDICADOR_STOCH,
            		listaCotizaciones,
            		indice_inicial_cargar_datos
            );
        } else {
  	      System.out.println("Error");
  	    }
            
        return listaCalculoMetricas;
    }


    
}
