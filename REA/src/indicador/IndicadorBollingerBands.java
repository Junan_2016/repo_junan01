package indicador;

import java.util.List;
import pojo.CalculoMetricaBand;
import pojo.Cotizacion;
import util.Constantes;
import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MAType;
import com.tictactec.ta.lib.MInteger;
import com.tictactec.ta.lib.RetCode;

public class IndicadorBollingerBands {    
	
	/**
     * 
     * @param listaCotizaciones
     * @return
     */
    public List <CalculoMetricaBand> calcular_BANDS(List <Cotizacion> listaCotizaciones){
    	
    	if(listaCotizaciones == null || listaCotizaciones.size() == 0) return null;

    	int DIAS_TOTALES = listaCotizaciones.size();
    	double[] precio_cierre_array = new double[DIAS_TOTALES]; 		 //se guardan el resultado de cada fecha
    	double[] resultado_outRealUpperBand = new double[DIAS_TOTALES];	 //Resultados Banda Superior
    	double[] resultado_outRealMiddleBand = new double[DIAS_TOTALES]; //Resultados Banda Media
    	double[] resultado_outRealLowerBand = new double[DIAS_TOTALES];	 //Resultados Banda Inferior
        
        MInteger indice_inicial_cargar_datos = new MInteger();
        MInteger longitud_array = new MInteger();
            	
    	Utils.cargaPreciosEnArray(precio_cierre_array, listaCotizaciones);
    	
    	Core c = new Core();
        RetCode retCode = c.bbands(0, (precio_cierre_array.length - 1),
				precio_cierre_array,
				10, 1, 1, 
				MAType.Ema,
				indice_inicial_cargar_datos,
				longitud_array,
				resultado_outRealUpperBand,
				resultado_outRealMiddleBand,
				resultado_outRealLowerBand);

        List <CalculoMetricaBand> listaCalculoMetricas = null;

        if (retCode == RetCode.Success) {
            //System.out.println("Calcular_iMA - inicio_array:" + inicio_array.value);
            //System.out.println("Calcular_iMA - longitud_array:" + longitud_array.value);

            listaCalculoMetricas = Utils.creacionlistaCalculoMetricasBollingerBands(
            		precio_cierre_array,
            		resultado_outRealUpperBand,
    				resultado_outRealMiddleBand,
    				resultado_outRealLowerBand,
            		Constantes.INDICADOR_BOLLINGER_BANDS,
            		listaCotizaciones,
            		indice_inicial_cargar_datos
            );
        } else {
  	      System.out.println("Error");
  	    }
            
        return listaCalculoMetricas;
    }


    
}
