package control;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Posicion_PropertiesController {

	static private Properties vProperties; 
	
    static {
    	try {
    		InputStream is = new Posicion_PropertiesController().getClass().getResourceAsStream("/REA_posicion.properties");
    		vProperties = new Properties();
    		vProperties.load(is);
	    } catch (IOException e) {
			System.out.println("Error la cargar el fichero de propiedades: " + e.getMessage());
		}
    }

    public static final String SIMBOLOS = vProperties.getProperty("SIMBOLOS");
    public static final String SIMULA_PRECIOS_FORZADOS = vProperties.getProperty("SIMULA_PRECIOS_FORZADOS");
    public static final String FECHA_DESDE_PROCESO = vProperties.getProperty("FECHA_DESDE_PROCESO");
    public static final String FECHA_HASTA_PROCESO = vProperties.getProperty("FECHA_HASTA_PROCESO");
    public static final String SIMBOLOS_YH = vProperties.getProperty("SIMBOLOS_YH");
        
}
