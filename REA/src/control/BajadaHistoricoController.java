package control;

import java.util.List;

import pojo.Cotizacion;

public class BajadaHistoricoController {
	
	public static  List<Cotizacion> BajadaHistoricoLogica(String rutaArchivo){
		final List<Cotizacion> lista = logica.BajadaHistoricoLogica.convertCSVtoList(rutaArchivo);	
		return lista;
	}

}
