package control;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import logica.DownloadLogic;
import logica.MetodosDeLogica;
import logica.TendenciaLogic;
import pojo.Cotizacion;
import pojo.Resumen;
import util.MetodosFechas;


public class TendenciaController {
	
	final private static DecimalFormat df = new DecimalFormat("0.00");
	
	public static void rastreandoTendenciaSASC(){
			
		List <Cotizacion> listaPapelesCotizables = getSimbolos();
		List <Cotizacion> listaPapelesCotizablesYH = getSimbolosYH();
		
		List <Resumen> resumenes = new ArrayList<Resumen>(); 
		Resumen resumen = null;

		//Validar si se simulara
		if (listaPapelesCotizables == null || listaPapelesCotizables.size()<=0){
			System.out.println("Se fuerza SIMULACION porque SIMBOLOS esta comentado en properties");
			listaPapelesCotizables = getListaCotizacion_for_REA();
			
			if(REA_PropertiesController.SIMULA_RV != null && REA_PropertiesController.SIMULA_RV.equals("1")) {
				System.out.println("Obtiene valores de SIMULA_RV");
				listaPapelesCotizables = BajadaRavaController.obtenerEspeciesSeleccionadas();
			}
		}
/* Funcionalidad de simulacion no valida para YH		
		if (listaPapelesCotizablesYH == null || listaPapelesCotizablesYH.size()<=0){
			System.out.println("Se fuerza SIMULACION porque SIMBOLOS_YH esta comentado en properties");
			listaPapelesCotizablesYH = getListaCotizacion_for_REA();
		}
*/
		//bajada de los archivos csv
		REA_DownloadController.bajarArchivosCSV(listaPapelesCotizables);
		REA_DownloadController.bajarArchivos_YH_CSV(listaPapelesCotizablesYH);
				
		System.out.println("****************************************");
		
		for(Cotizacion vCotizacion : listaPapelesCotizables){
			
			String rutaCarpetaBajadas = REA_PropertiesController.RUTA_CARPETA_BAJADAS_CSV;
			String nombreArchivo = DownloadLogic.obtenerNombreArchivo(vCotizacion.getNombrePapel());	
			String rutaArchivo = rutaCarpetaBajadas + nombreArchivo;
			File archivo = new File(rutaArchivo);
			System.out.println("PAPEL: " + nombreArchivo);
			
			if (archivo.exists()){
				List<Cotizacion> listaCotizaciones = getListaCotizaciones(rutaArchivo);
				simularCotizacionDiaria(vCotizacion,listaCotizaciones);
				agregaNombrePapel(vCotizacion,listaCotizaciones);
				
				final String nombrePapel = listaCotizaciones.get(0).getNombrePapel();
		    	final List <Cotizacion> cotizacionesHardcode = MetodosDeLogica.buscaCotizacionHardcode(nombrePapel);
		    	if(cotizacionesHardcode != null && cotizacionesHardcode.size() > 0)
		    		listaCotizaciones = agregaCotizacionesHardcodeadas(listaCotizaciones,cotizacionesHardcode);

		    	TendenciaLogic tendencia = new TendenciaLogic();

		    	//Papel Asociado Sector
		    	final String papelAsociadoSector = MetodosDeLogica.getPapelSector(vCotizacion.getNombrePapel());
		    	if(papelAsociadoSector != null){
		    		System.out.println("PAPEL ASOCIADO SECTOR: " + papelAsociadoSector);

			    	//Buscar papel sector
			    	String rutaArchivoPapelSector = DownloadLogic.obtenerNombreArchivo(papelAsociadoSector);	
			    	rutaArchivoPapelSector = rutaCarpetaBajadas + rutaArchivoPapelSector;
			    	List<Cotizacion> listaCotizacionesPapelSector = getListaCotizaciones(rutaArchivoPapelSector);
			    	AdministradorIndicadores.generaMetricasPapel(papelAsociadoSector,listaCotizacionesPapelSector);
			    				    	
		    	}
		    	resumen =  tendencia.execute(listaCotizaciones);
		    	
		    	System.out.println("RASTREADOR SASC INDICA COMPRAR " + vCotizacion.getNombrePapel() + ": "+ resumen.haySegnalIngreso());
				resumenes.add(resumen);
				
			}else{
				System.out.println("Archivo inexistente");
			}
			
			System.out.println("****************************************");
		}
		
		//Cotizaciones YH
		if(listaPapelesCotizablesYH != null){
			for(Cotizacion vCotizacionYH : listaPapelesCotizablesYH){
				
				String rutaCarpetaBajadas = REA_PropertiesController.RUTA_CARPETA_BAJADAS_CSV;
				String nombreArchivo = DownloadLogic.obtenerNombreArchivo(vCotizacionYH.getNombrePapel());	
				String rutaArchivo = rutaCarpetaBajadas + nombreArchivo;
				File archivo = new File(rutaArchivo);
				System.out.println("PAPEL: " + nombreArchivo);
				
				if (archivo.exists()){
					List<Cotizacion> listaCotizaciones = getListaCotizaciones(rutaArchivo);
					simularCotizacionDiaria(vCotizacionYH,listaCotizaciones);
					agregaNombrePapel(vCotizacionYH,listaCotizaciones);
					
					final String nombrePapel = listaCotizaciones.get(0).getNombrePapel();
			    	final List <Cotizacion> cotizacionesHardcode = MetodosDeLogica.buscaCotizacionHardcode(nombrePapel);
			    	if(cotizacionesHardcode != null && cotizacionesHardcode.size() > 0)
			    		listaCotizaciones = agregaCotizacionesHardcodeadas(listaCotizaciones,cotizacionesHardcode);

			    	TendenciaLogic tendencia = new TendenciaLogic();

			    	//Papel Asociado Sector
			    	final String papelAsociadoSector = MetodosDeLogica.getPapelSector(vCotizacionYH.getNombrePapel());
			    	if(papelAsociadoSector != null){
			    		System.out.println("PAPEL ASOCIADO SECTOR: " + papelAsociadoSector);

				    	//Buscar papel sector
				    	String rutaArchivoPapelSector = DownloadLogic.obtenerNombreArchivo(papelAsociadoSector);	
				    	rutaArchivoPapelSector = rutaCarpetaBajadas + rutaArchivoPapelSector;
				    	List<Cotizacion> listaCotizacionesPapelSector = getListaCotizaciones(rutaArchivoPapelSector);
				    	AdministradorIndicadores.generaMetricasPapel(papelAsociadoSector,listaCotizacionesPapelSector);
				    				    	
			    	}
			    	resumen =  tendencia.execute(listaCotizaciones);
			    	
			    	System.out.println("RASTREADOR SASC INDICA COMPRAR " + vCotizacionYH.getNombrePapel() + ": "+ resumen.haySegnalIngreso());
					resumenes.add(resumen);
					
				}else{
					System.out.println("Archivo inexistente");
				}
				
				System.out.println("****************************************");
			}	
		}
				
		System.out.println("************* RESUMEN ****************************************************************************************************");

		if (resumenes != null){
			
			for (Resumen r : resumenes){
			    System.out.println(
			    		r.getSimbolo_papel() + " - " + r.getOperacion() + 
			    		r.getResultado() + r.getDesglose_resultado() + 
			    		"- Unid:" + r.getUnidades_operadas() +
			    		"- DiasCarga:" + r.getDiasCargando() +
			    		"- Precio:" + df.format(r.getUltimo_precio_papel()) +
			    		"- Efect:" + r.getAciertos() + "%" +
			    		"- ResAcum:" + r.getResultadoTotal()			    		
			    		);	
			}
		}	
			
		System.out.println("**************************************************************************************************************************");
	}

    private static void agregaNombrePapel(Cotizacion vCotizacion,List<Cotizacion> listaCotizaciones) {
		for(Cotizacion aCotizacion : listaCotizaciones){
			aCotizacion.setNombrePapel(vCotizacion.getNombrePapel());
		}
	}
    
    private static List<Cotizacion> agregaCotizacionesHardcodeadas(List<Cotizacion> listaCotizaciones,  List <Cotizacion> cotizacionesHardcode) {
    	List<Cotizacion> newListaCotizaciones = null;
    	if(cotizacionesHardcode == null || cotizacionesHardcode.size() == 0) return listaCotizaciones;
   	
    	for(Cotizacion vCotizacion : cotizacionesHardcode){

    		for(int m=0; m<listaCotizaciones.size(); m++){
    			if(m== 2506)
    				System.out.println();
    			    			
    			//item que no es ultimo
    			if(m+1 > listaCotizaciones.size()){
        			if(vCotizacion.getFecha().after(listaCotizaciones.get(m).getFecha()) &&
        	    			   vCotizacion.getFecha().before(listaCotizaciones.get(m+1).getFecha()))
        	    			{
        	    				newListaCotizaciones = new ArrayList <Cotizacion>();
        	    				newListaCotizaciones.addAll(listaCotizaciones.subList(0, m+1));
        	    				newListaCotizaciones.add(vCotizacion);
        	    				newListaCotizaciones.addAll(listaCotizaciones.subList(m+1, listaCotizaciones.size()));
        	    				listaCotizaciones = newListaCotizaciones; //se actualiza en cada iteracion
        	    				break;
        	    			}
    			}else{
    				if(m+1 == listaCotizaciones.size()){
    					//ultimo item    				
        				newListaCotizaciones = new ArrayList <Cotizacion>();
        				newListaCotizaciones.addAll(listaCotizaciones.subList(0, m+1));
        				newListaCotizaciones.add(vCotizacion);
        				newListaCotizaciones.addAll(listaCotizaciones.subList(m+1, listaCotizaciones.size()));
        				listaCotizaciones = newListaCotizaciones; //se actualiza en cada iteracion
        				break;
    				}
    			}
      		}
    	}
    	return listaCotizaciones;
  	}

	/**
	 * 
	 */
	private static List <Cotizacion> getSimbolos() {
		String[] simbolos = DownloadLogic.obtenerSimbolos();	
		
		if(simbolos == null){
			return null;
		}
		
		List <Cotizacion> listaPapelesCotizables = new ArrayList<Cotizacion>();
		Cotizacion vCotizacion = null;
		
		for(String codigoPapel: simbolos){
			vCotizacion = new Cotizacion();
			vCotizacion.setNombrePapel(codigoPapel);
	
			listaPapelesCotizables.add(vCotizacion);
		}
		return listaPapelesCotizables;
	}
	
	/**
	 * 
	 */
	private static List <Cotizacion> getSimbolosYH() {
		String[] simbolos = DownloadLogic.obtenerSimbolos_YH();	
		
		if(simbolos == null){
			return null;
		}
		
		List <Cotizacion> listaPapelesCotizables = new ArrayList<Cotizacion>();
		Cotizacion vCotizacion = null;
		
		for(String codigoPapel: simbolos){
			vCotizacion = new Cotizacion();
			vCotizacion.setNombrePapel(codigoPapel);
	
			listaPapelesCotizables.add(vCotizacion);
		}
		return listaPapelesCotizables;
	}
		
	public static List<Cotizacion> getListaCotizacion_for_REA(){

			List<Cotizacion> listaPapelesCotizables = BajadaController.getPapelParseado();
						
			return listaPapelesCotizables;
	}
	
	private static void simularCotizacionDiaria(Cotizacion vCotizacion, List<Cotizacion> listaCotizaciones) {
		if(vCotizacion.getPrecio_cierre() != 0.0){
			listaCotizaciones.add(vCotizacion);
			//System.out.println("Se agrego cotizacion al final - papel: " + vCotizacion.getNombrePapel());
		}		
	}
	
	private static List<Cotizacion> getListaCotizaciones(String rutaArchivo) {
		//return CotizacionesMOCK.getListaCotizaciones();
		return BajadaHistoricoController.BajadaHistoricoLogica(rutaArchivo);
	}

}
