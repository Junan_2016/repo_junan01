package control;

import indicador.IndicadorATR;
import indicador.IndicadorBalanceMovil;
import indicador.IndicadorBollingerBands;
import indicador.IndicadorFuerzaRelativa;
import indicador.IndicadorMomentum;
import indicador.IndicadorStochastic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pojo.CalculoMetrica;
import pojo.CalculoMetricaBand;
import pojo.CalculoMetricaSTOCH;
import pojo.Cotizacion;
import pojo.MetricasPapel;

public class AdministradorIndicadores {

	private static Map <String,MetricasPapel> MapMetricasPapel = new HashMap <String,MetricasPapel>();
	
	/**
	 * 
	 * @param listaCotizaciones
	 * @return
	 */
	public static MetricasPapel generaMetricasPapel(String nombrePapel, List <Cotizacion> listaCotizaciones){
		
		if(MapMetricasPapel.get(nombrePapel) != null){
			return MapMetricasPapel.get(nombrePapel);
		}
				
		MapMetricasPapel.put(nombrePapel, generaDatosIndicadores(nombrePapel, listaCotizaciones));
		System.out.println("GENERANDO METRICAS: " + nombrePapel);
		return MapMetricasPapel.get(nombrePapel);
	}
	
	/**
	 * 
	 * @param nombrePapel
	 * @param listaCotizaciones
	 * @return
	 */
	private static MetricasPapel generaDatosIndicadores(String nombrePapel, List <Cotizacion> listaCotizaciones){
				
		//Calculos OBV
		IndicadorBalanceMovil vIndicadorBalanceMovil = new IndicadorBalanceMovil();
		List <CalculoMetrica> listaCalculoMetrica_OBV= vIndicadorBalanceMovil.calcular_IOBL(listaCotizaciones);	
			
		//Calculos STOCH
	    IndicadorStochastic vIndicadorStochastic = new IndicadorStochastic();
		List <CalculoMetricaSTOCH> listaCalculoMetrica_STOCH= vIndicadorStochastic.calcular_STOCH(listaCotizaciones);	
	
		//Calculos BANDAS BOLLINGER
	    IndicadorBollingerBands vIndicadorBollingerBands = new IndicadorBollingerBands();
		List <CalculoMetricaBand> listaCalculoMetrica_BAND= vIndicadorBollingerBands.calcular_BANDS(listaCotizaciones);	
		
		//Calculos ATR
	    IndicadorATR vIndicadorATR = new IndicadorATR();
		List <CalculoMetrica> listaCalculoMetrica_ATR = vIndicadorATR.calcular_ATR(listaCotizaciones);	

		//Calculos MMT_15
		IndicadorMomentum VIndicadorMomentum = new IndicadorMomentum();
		List <CalculoMetrica> listaCalculoMetrica_MMT_15 = VIndicadorMomentum.calcular_MMT(listaCotizaciones,15);

		//Calculos MMT_20
		List <CalculoMetrica> listaCalculoMetrica_MMT_20 = VIndicadorMomentum.calcular_MMT(listaCotizaciones,20);
						
		MetricasPapel vMetricasPapel = new MetricasPapel(
				nombrePapel,
				listaCotizaciones,
				listaCalculoMetrica_OBV,
				listaCalculoMetrica_STOCH,
				listaCalculoMetrica_BAND,
				listaCalculoMetrica_ATR,
				listaCalculoMetrica_MMT_15,
				listaCalculoMetrica_MMT_20
		);
		
		return vMetricasPapel;
		
	}
	
	
}
