package control;

import java.util.ArrayList;
import java.util.List;

import pojo.Cotizacion;
import pojo.Papel;

import logica.BajadaLogic;

public class BajadaController {
				
	public static List<Cotizacion> getPapelParseado(){

		//SIMULA_PRECIOS_FORZADOS=APBR:51.3-GGAL:0
		
		List <Papel> listaPapeles = new ArrayList<Papel>();
		if(Posicion_PropertiesController.SIMULA_PRECIOS_FORZADOS != null){
			String[] cadenaPrecios = Posicion_PropertiesController.SIMULA_PRECIOS_FORZADOS.split("-");
			
			for(String cadena: cadenaPrecios){
				final String simboloPapel = cadena.split(":")[0];
				final String precioPapel = cadena.split(":")[1];
							
				Papel vPapel = new Papel();
				vPapel.setSimbolo_papel(simboloPapel);
				vPapel.setPrecio_papel(Double.parseDouble(precioPapel));			
				
				listaPapeles.add(vPapel);
			};
		}
		
		List<Cotizacion> listaCotizacion = new ArrayList <Cotizacion>();
		String rutaArchivo = null;
		
		for(Papel vPapel: listaPapeles){
			
			System.out.println("*** Parseo de " + vPapel.getSimbolo_papel());
			rutaArchivo = REA_PropertiesController.RUTA_CARPETA_BAJADAS_CSV + vPapel.getSimbolo_papel() + "_t_files/GetTituloVote.html"; 
			Cotizacion vCotizacion = BajadaLogic.procesaCotizacionHTML_from_file(rutaArchivo);
			
			if(vCotizacion != null){
				vCotizacion.setNombrePapel(vPapel.getSimbolo_papel());
				
				if(vPapel.getPrecio_papel() != 0){
					vCotizacion.setPrecio_cierre(vPapel.getPrecio_papel()); //Precio del properties
					vCotizacion.setPrecio_maximo(vPapel.getPrecio_papel()); //PrecMax del properties
					System.out.println("Redefinieron - Precio: " + vPapel.getPrecio_papel() + " - PrecMax: " + vPapel.getPrecio_papel());
				}
				listaCotizacion.add(vCotizacion);
			}
		}
		
		return listaCotizacion;
	}
	
}
