package control;

import java.util.ArrayList;
import java.util.List;

import logica.BajadaRavaLogic;
import logica.MetodosDeLogica;
import pojo.Cotizacion;
import util.ParseadorProperties;


public class BajadaRavaController {
	
	static List<Cotizacion> listaEspeciesTemp = null;
	static List<Cotizacion> listaEspecies = new ArrayList<Cotizacion> ();
	
	private static String propertiesSimbolos = null;
	private static String [] simbolos = null;
	
	
	/**
	 * 
	 * @return List<Cotizacion>
	 */
	public static List<Cotizacion> obtenerEspeciesSeleccionadas() {
		
		obtenerSimbolosRV();
		obtenerEspecies();
		
		if (listaEspeciesTemp != null) {
			
			for (String vSimbolo : simbolos) {	
				
				for(Cotizacion vEspecie : listaEspeciesTemp){
					
					if(vEspecie.getNombrePapel().equalsIgnoreCase(vSimbolo.toString())) {
						listaEspecies.add(vEspecie);
					}
				}
			}
				
		}
		else{
			System.out.println("listaEspeciesTemp está vacía");
		}
		return listaEspecies;
		
	}
	
	/**
	 * 
	 */
	private static void obtenerSimbolosRV() {
		
		propertiesSimbolos = REA_PropertiesController.SIMBOLOS_RV;
		
		if (propertiesSimbolos != null){
			
			simbolos = ParseadorProperties.parsearPropeties(propertiesSimbolos);

		}else{
			
			System.out.println("Properties SIMBOLOS_RV es nula");
		}

	}
	
	/**
	 * 
	 */
	private static void obtenerEspecies() {
		
		String[] tipoEspeciesProperties = REA_PropertiesController.TIPO_ESPECIE.split(",");
		
		for(String especie: tipoEspeciesProperties){
			
			listaEspeciesTemp = BajadaRavaLogic.procesaHTML_from_file(especie);
			
			if (listaEspeciesTemp.size()<=0){
				System.err.println("lista especie: " + especie + "está vacía.");
			}
			
			else{
				
				if(MetodosDeLogica.aplica_TrazeoFull()) {
					//salida especies		
					System.out.println("------------------------ ESPECIE:" + especie + " ----------------------------");
					for(Cotizacion vEspecie : listaEspeciesTemp ){

						System.out.println(vEspecie.getNombrePapel() + ":");
						System.out.println(vEspecie.toString());
						System.out.println("--------------------------------------------------------------------------");
					}	
				}

			}
					
		}
				
	}
	


	
}
