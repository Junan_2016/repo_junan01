package control;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class REA_PropertiesController {

	static private Properties vProperties; 
	
    static {
    	try {
    		InputStream is = new REA_PropertiesController().getClass().getResourceAsStream("/REA.properties");
    		vProperties = new Properties();
    		vProperties.load(is);
	    } catch (IOException e) {
			System.out.println("Error la cargar el fichero de propiedades: " + e.getMessage());
		}
    }

    public static final String RUTA_ARCHIVO_BAJADA_CSV_MT = vProperties.getProperty("RUTA_ARCHIVO_BAJADA_CSV_MT");
    public static final String RUTA_CARPETA_BAJADAS_CSV = vProperties.getProperty("RUTA_CARPETA_BAJADAS_CSV");
    public static final String URL_DOWNLOAD_CSV = vProperties.getProperty("URL_DOWNLOAD_CSV");
    public static final String URL_DOWNLOAD_YH_CSV = vProperties.getProperty("URL_DOWNLOAD_YH_CSV");
    public static final String SIMBOLO_R1 = vProperties.getProperty("SIMBOLO_R1");
    public static final String SIMBOLO_R2 = vProperties.getProperty("SIMBOLO_R2");
    public static final String SIMBOLO_R3 = vProperties.getProperty("SIMBOLO_R3");
    public static final String SIMBOLO_R4 = vProperties.getProperty("SIMBOLO_R4");
    public static final String FUERTE_STOCH_ASC = vProperties.getProperty("FUERTE_STOCH_ASC");
    public static final String FUERTE_STOCH_DESC = vProperties.getProperty("FUERTE_STOCH_DESC");
    public static final String FECHA_CIERRE_FORZADO = vProperties.getProperty("FECHA_CIERRE_FORZADO");
    public static final String TIME_FRAMES = vProperties.getProperty("TIME_FRAMES");
    public static final String CANTIDAD_TIME_FRAMES_MOSTRADOS = vProperties.getProperty("CANTIDAD_TIME_FRAMES_MOSTRADOS");
    public static final String DIAS_MARCO_TEMPORAL = vProperties.getProperty("DIAS_MARCO_TEMPORAL");
    public static final String BONETES = vProperties.getProperty("BONETES");
    public static final String GIROS_AUTORIZADOS = vProperties.getProperty("GIROS_AUTORIZADOS");
    public static final String CORTAR_FECHAS_EJERCICIOS = vProperties.getProperty("CORTAR_FECHAS_EJERCICIOS");
    public static final String DESVIOS_MAXIMOS_ALCANZADOS = vProperties.getProperty("DESVIOS_MAXIMOS_ALCANZADOS");
    public static final String SIN_RENDIMIENTOS = vProperties.getProperty("SIN_RENDIMIENTOS");
    public static final String PORCENTAJE_PELIGRO_INVISIBLE = vProperties.getProperty("PORCENTAJE_PELIGRO_INVISIBLE");
    public static final String NO_APLICA_ELLIOTT = vProperties.getProperty("NO_APLICA_ELLIOTT");
    public static final String TRAZEO_FULL = vProperties.getProperty("TRAZEO_FULL");
    public static final String RUTA_ATR = vProperties.getProperty("RUTA_ATR");
    
    //Propiedades para bajada RAVA
    public static final String SIMULA_RV = vProperties.getProperty("SIMULA_RV");
    public static final String SIMBOLOS_RV = vProperties.getProperty("SIMBOLOS_RV");
    public static final String URL_DOWNLOAD_HTML = vProperties.getProperty("URL_DOWNLOAD_HTML");
    public static final String TIPO_ESPECIE = vProperties.getProperty("TIPO_ESPECIE");
    
    public static final String DELTA_PORCENTAJE_PRECIO = vProperties.getProperty("DELTA_PORCENTAJE_PRECIO");
    public static final String NO_APLICA_CARGA_BLOQUE = vProperties.getProperty("NO_APLICA_CARGA_BLOQUE");
    public static final String NO_RESCATE_SEMILLAS = vProperties.getProperty("NO_RESCATE_SEMILLAS");
    public static final String PORCENTAJE_RESCATE = vProperties.getProperty("PORCENTAJE_RESCATE");
    public static final String MESES_PERMITIDOS_ELLIOTT = vProperties.getProperty("MESES_PERMITIDOS_E");
    public static final String PAPELES_SECTOR = vProperties.getProperty("PAPELES_SECTOR");
    public static final String NO_APLICA_CARGA_MULTI_BLOQUE = vProperties.getProperty("NO_APLICA_CARGA_MULTI_BLOQUE");
        
    
}
