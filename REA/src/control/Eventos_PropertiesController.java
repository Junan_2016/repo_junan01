package control;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Eventos_PropertiesController {

	static private Properties vProperties; 
	
    static {
    	try {
    		InputStream is = new Eventos_PropertiesController().getClass().getResourceAsStream("/REA_eventos.properties");
    		vProperties = new Properties();
    		vProperties.load(is);
	    } catch (IOException e) {
			System.out.println("Error la cargar el fichero de propiedades: " + e.getMessage());
		}
    }

    public static final String EVENTOS = vProperties.getProperty("EVENTOS");
    public static final String COTIZACIONES_HARDCODE = vProperties.getProperty("COTIZACIONES_HARDCODE");    
}
