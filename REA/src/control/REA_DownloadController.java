/**
 * http://lineadecodigo.com/java/como-descargar-un-archivo-en-java/
 */
package control;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import pojo.Cotizacion;
import logica.DownloadLogic;
import logica.FormatearCSV_YH_Logica;
import logica.MetodosDeLogica;

public class REA_DownloadController {
	
	private static String nombreArchivo = null; 
	private static String urlSimbolo = null;
	
	//Directorio destino para las descargas
	private static String folder = null;
	private static File dir = null;
	private static File file = null;
			
	/**
	 * Download CSV files
	 * @param listaPapelesCotizables
	 */
	public static void bajarArchivosCSV(List <Cotizacion> listaPapelesCotizables) {

		crearDirectorio ();	

		for(Cotizacion vCotizacion : listaPapelesCotizables){

			urlSimbolo = DownloadLogic.obtenerURLSimbolo(vCotizacion.getNombrePapel());
			//System.out.println(urlSimbolo);
			
			nombreArchivo = DownloadLogic.obtenerNombreArchivo(vCotizacion.getNombrePapel());
			//System.out.println(nombreArchivo);
			
			System.out.println("\n **************** " + vCotizacion.getNombrePapel() + " **************** \n");
			downloadCSV (urlSimbolo, nombreArchivo);

			
			//Papel asociado sector
			final String papelAsociadoSector = MetodosDeLogica.getPapelSector(vCotizacion.getNombrePapel());
	    	if(papelAsociadoSector != null){
	    		
	    		urlSimbolo = DownloadLogic.obtenerURLSimbolo(papelAsociadoSector);
				//System.out.println(urlSimbolo);
				
				nombreArchivo = DownloadLogic.obtenerNombreArchivo(papelAsociadoSector);
				//System.out.println(nombreArchivo);

				downloadCSV (urlSimbolo, nombreArchivo);
	    	}
			
		}
	}
	
	/**
	 * Download YH CSV files
	 * @param listaPapelesCotizablesYH
	 */
	public static void bajarArchivos_YH_CSV(List <Cotizacion> listaPapelesCotizablesYH) {

		if(listaPapelesCotizablesYH == null)
			return;		
				
		crearDirectorio ();	

		for(Cotizacion vCotizacion : listaPapelesCotizablesYH){

			urlSimbolo = DownloadLogic.obtenerURLSimboloYH(vCotizacion.getNombrePapel());
			//System.out.println(urlSimbolo);
			
			nombreArchivo = DownloadLogic.obtenerNombreArchivo(vCotizacion.getNombrePapel());
			//System.out.println(nombreArchivo);
			
			System.out.println("\n **************** " + vCotizacion.getNombrePapel() + " **************** \n");
			downloadCSV (urlSimbolo, nombreArchivo);

			//formateo propio YH
			FormatearCSV_YH_Logica.formatCSV_YH(folder + "/" +nombreArchivo);
			
			//Papel asociado sector
			final String papelAsociadoSector = MetodosDeLogica.getPapelSector(vCotizacion.getNombrePapel());
	    	if(papelAsociadoSector != null){
	    		
	    		urlSimbolo = DownloadLogic.obtenerURLSimbolo(papelAsociadoSector);
				//System.out.println(urlSimbolo);
				
				nombreArchivo = DownloadLogic.obtenerNombreArchivo(papelAsociadoSector);
				//System.out.println(nombreArchivo);

				downloadCSV (urlSimbolo, nombreArchivo);
				
				//formateo propio YH
				FormatearCSV_YH_Logica.formatCSV_YH(folder + "/" +nombreArchivo);
	    	}
			
		}
	}

	/**
	 * 
	 */
	public static void crearDirectorio (){
		 
		folder = REA_PropertiesController.RUTA_CARPETA_BAJADAS_CSV;
		
		if (folder != null){
			
			//Crea el directorio de destino en caso de que no exista
			dir = new File(folder);
			 
			if (!dir.exists())
			  if (!dir.mkdir())
			    return; // no se pudo crear la carpeta de destino
		}else{
			System.out.println("Properties RUTA_CARPETA_BAJADAS_CSV es nula");
		}
		
	}

	/**
	 * 
	 * @param url
	 * @param name
	 */
	public static void downloadCSV (String url, String name){
		
		URLConnection conn = null;
		InputStream in = null;
		OutputStream out = null;
				
		try {		
			
			System.out.println(">> Nombre: " + name);
			System.out.println(">> Directorio: " + dir);

			//Crea el archivo destino, en caso de existir lo elimina
			file = new File(folder + name);
			
			if (!file.exists()){
				
				System.out.println(">> URL: " + url);
				//System.out.println(">> tamaño: " + conn.getContentLength() + " bytes");
			
				//Abrir conexion
				conn = new URL(url).openConnection();
				conn.connect();	
				
				//Abrir los Stream
				in = conn.getInputStream();
				out = new FileOutputStream(file);
				
				//Leer y escribir hasta encontrar el fin del archivo
				int b = 0;
				while (b != -1) {
				  b = in.read();
				  if (b != -1)
				    out.write(b);
				}
					
				//Cerrar los streams
				out.close();
				in.close();
			
			}else{
				
				System.out.println(">> Archivo existente");
				
			}
			
		} catch (MalformedURLException e) {
			System.out.println("La URL: " + url + " no es valida!");
		} catch (IOException e) {
			e.printStackTrace();
			e.getMessage();
		}		

	}

}
