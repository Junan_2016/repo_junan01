package control;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;


public class SeguridadController {
	
	private static String semilla = "rea";
	private static String entradaTeclado = "";

	
	/**
	 * Obtiene hash del ingreso por teclado concatenado con la semilla
	 * @return
	 */
	public static String getHash() {
		
    	getTextoPorTeclado();        
        //System.out.println ("Entrada recibida por teclado es: \"" + entradaTeclado +"\"");
        //System.out.println(getMD5(semilla.concat(entradaTeclado)));        
        return getMD5(semilla.concat(entradaTeclado));
        
	}


    /**
     * Entrada por teclado
     * @param input
     */
    public static void getTextoPorTeclado() {
    	
        System.out.println ("texto:");
        Scanner entradaEscaner = new Scanner (System.in); 	//Creación de un objeto Scanner
        
        entradaTeclado = entradaEscaner.nextLine ();

    }
    
    
    /**
	 * Método MD5
	 * @param input
	 * @return
	 */
    public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
    
    
	/**
	 * Compara el hash de la semilla+texto de entrada con semilla+clave
	 * @param hashEntrada
	 * @return
	 */
    public static boolean comparaHash(String hashEntrada){
    	
    	if (hashEntrada.equals(getMD5(semilla.concat("super"))) || hashEntrada.equals(getMD5(semilla.concat("qew"))))
    		return true;
    	else
    		return false;
    	
    }


}
