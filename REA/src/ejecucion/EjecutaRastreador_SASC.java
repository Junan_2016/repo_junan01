package ejecucion;

import control.SeguridadController;
import control.TendenciaController;

public class EjecutaRastreador_SASC {

	/**
	 * Bajada de los archivos CSV
	 * Rastrear tendencias ascendentes
	 * @param args
	 */
	public static void main(String[] args) {
		
		if (SeguridadController.comparaHash(SeguridadController.getHash())){
			
			try {
				
				Thread.sleep(2000);
				TendenciaController.rastreandoTendenciaSASC();
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
		else
			System.err.println("Texto incorrecto.");
	}


}