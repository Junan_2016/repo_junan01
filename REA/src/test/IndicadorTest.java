package test;

import static org.junit.Assert.*;
import indicador.IndicadorBollingerBands;
import indicador.IndicadorStochastic;
import org.junit.BeforeClass;
import org.junit.Test;
import control.BajadaHistoricoController;

public class IndicadorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test_IndicadoresIndicadorStochastic() {
		
		String rutaArchivo = "/home/julio/BajadasHTML/hist_APBR_2015-05-24.csv";
		
		IndicadorStochastic xxx = new IndicadorStochastic();
		boolean HAY_DATOS = xxx.calcular_STOCH(BajadaHistoricoController.BajadaHistoricoLogica(rutaArchivo)).size()>0;		
		assertEquals(true, HAY_DATOS);
		
		IndicadorBollingerBands yyy = new IndicadorBollingerBands();
		HAY_DATOS = yyy.calcular_BANDS(BajadaHistoricoController.BajadaHistoricoLogica(rutaArchivo)).size()>0;		
		
	}

	@Test
	public void test_IndicadorBollingerBands() {
		
		String rutaArchivo = "/home/julio/BajadasHTML/hist_APBR_2015-05-24.csv";
				
		IndicadorBollingerBands yyy = new IndicadorBollingerBands();
		boolean HAY_DATOS = yyy.calcular_BANDS(BajadaHistoricoController.BajadaHistoricoLogica(rutaArchivo)).size()>0;		
		assertEquals(true, HAY_DATOS);
	}

	
	

	
}
