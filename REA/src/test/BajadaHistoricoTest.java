package test;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import pojo.Cotizacion;
import control.BajadaHistoricoController;

public class BajadaHistoricoTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() {
		String rutaArchivo = "/home/nanci/BajadasHTML/";
		java.util.List<Cotizacion> lista = BajadaHistoricoController.BajadaHistoricoLogica(rutaArchivo);
		assertEquals(true, lista.size() > 0);
	}

}
