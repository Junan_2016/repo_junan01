package logica;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import pojo.Cotizacion;

public class CotizacionParser {
  
	public static Cotizacion parseEntry(String[] data) {
		
		Date fecha = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			fecha = formatter.parse(data[0].replace("\"", ""));
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		double precio_apertura = Double.parseDouble(data[1].replace("\"", "")); 
		double precio_maximo = Double.parseDouble(data[2].replace("\"", "")); 
		double precio_minimo = Double.parseDouble(data[3].replace("\"", ""));
		double precio_cierre = Double.parseDouble(data[4].replace("\"", ""));
		double volumen = Double.parseDouble(data[5].replace("\"", ""));
		
		Cotizacion cotizacion = new Cotizacion();
		cotizacion.setFecha(fecha);
		cotizacion.setPrecio_apertura(precio_apertura);
		cotizacion.setPrecio_maximo(precio_maximo);
		cotizacion.setPrecio_minimo(precio_minimo);
		cotizacion.setPrecio_cierre(precio_cierre);
		cotizacion.setVolumen(volumen);
				
		return cotizacion;
	}

}