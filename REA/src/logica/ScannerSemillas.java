package logica;

import java.util.List;

import control.REA_PropertiesController;
import pojo.Cotizacion;
import pojo.UnidadOperada;
import util.MetodosFechas;

public class ScannerSemillas {

	/**
	 * 
	 * @param rescateForzado
	 * @param listaCotizaciones
	 * @param j
	 * @param listaUnidadesOperadas
	 */
	public static void rescatarSemillas(
			boolean rescateForzado,
			List<Cotizacion> listaCotizaciones, 
			int j, 
			List<UnidadOperada> listaUnidadesOperadas,
			boolean es_W_cargaGrosa)
	{
		
		if(!MetodosDeLogica.aplica_RescateSemillas() || es_W_cargaGrosa)
			return;
					
		final double porcentajeRescateTeorico = MetodosDeLogica.getPORCENTAJE_RESCATE();
								
		if(listaUnidadesOperadas == null || listaUnidadesOperadas.size()==0)
			return;
		
		Cotizacion cotizacionActual = listaCotizaciones.get(j);
		final String fechaString = MetodosFechas.Date_to_String(cotizacionActual.getFecha()); 
		
		for(UnidadOperada operada: listaUnidadesOperadas){
			
			if(fechaString.equalsIgnoreCase(MetodosFechas.Date_to_String(operada.getFecha())))
				continue;
			
			
			if(!operada.estaRescatadaSemilla()){
				double resultado =  cotizacionActual.getPrecio_cierre() - operada.getPrecioOriginal();
				double porcentajeResultado = resultado*100/operada.getPrecioOriginal();
							
				if(porcentajeResultado > porcentajeRescateTeorico){
					operada.setEstaRescatadaSemilla(true);
					operada.setResultadoRescate(resultado);
					
					if(MetodosDeLogica.aplica_TrazeoFull())
						System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- ASC->>RESCATE SEMILLA: " + operada);
				}else{
					if(rescateForzado){
						operada.setEstaRescatadaSemilla(true);
						operada.setRescateForzado(true);
						operada.setResultadoRescate(resultado); //no importa si va a perdida
						
						System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- ASC->>RESCATE FORZADO SEMILLA: " + operada);
					}
				}
			}else{
				
				if(operada.fueRescateForzado()){
										
					double resultado =  cotizacionActual.getPrecio_cierre() - operada.getPrecioOriginal();
					double porcentajeResultado = resultado*100/operada.getPrecioOriginal();
								
					if(resultado > operada.getResultadoRescate() && porcentajeResultado > porcentajeRescateTeorico){
						operada.setEstaRescatadaSemilla(true);
						operada.setRescateForzado(false);
						operada.setResultadoRescate(resultado);
						
						if(MetodosDeLogica.aplica_TrazeoFull())
							System.out.println(
								MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
								"- ASC->>RE-RESCATE SEMILLA: " + operada);
					
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @param listaUnidadesOperadas
	 * @return
	 */
	public static long cantSemillasRescatadas(List<UnidadOperada> listaUnidadesOperadas){
		
		if(REA_PropertiesController.NO_RESCATE_SEMILLAS == null)
			return 0;
		
		long cantidadUnidades = 0;
		for(UnidadOperada operada: listaUnidadesOperadas){
			if(operada.estaRescatadaSemilla()){
				cantidadUnidades = cantidadUnidades + operada.getCantidad();
			}	
		}
		return cantidadUnidades;
		
	}
	
	/**
	 * 
	 * @param listaUnidadesOperadas
	 * @return
	 */
	
	public static long cantSemillasNoRescatadas(List<UnidadOperada> listaUnidadesOperadas){
		
		if(REA_PropertiesController.NO_RESCATE_SEMILLAS == null)
			return MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas);
		
		return MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) - cantSemillasRescatadas(listaUnidadesOperadas); 
		
	}

	
}
