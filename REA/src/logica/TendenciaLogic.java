package logica;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import control.AdministradorIndicadores;
import control.Posicion_PropertiesController;
import control.REA_PropertiesController;
import pojo.CalculoMetrica;
import pojo.CalculoMetricaBand;
import pojo.CalculoMetricaSTOCH;
import pojo.Cotizacion;
import pojo.Ficha;
import pojo.FichaPiñon;
import pojo.Medida;
import pojo.MetricasPapel;
import pojo.Operacion;
import pojo.Rendimiento;
import pojo.Resumen;
import pojo.UnidadOperada;
import util.MetodosFechas;

public class TendenciaLogic {

final private DecimalFormat df = new DecimalFormat("0.00");
final private DecimalFormat df_enteros = new DecimalFormat("0");

final private double delta_ATR = 0.05;

int contador_salida_elefante_rojo = 0;
int sleep_salida_elefante_rojo = 1;
int hs_0 = 0;
int hs_24 = 1;
int hs_48 = 2;

List<Operacion> lista_resultados_operaciones = null;
List<Cotizacion> listaCotizacionesDentroRangoFechas = null;
List<UnidadOperada> listaUnidadesOperadas = null;
List<FichaPiñon> listaFichasPiñon = null;
List<FichaPiñon> listaFichasPiñonHistorico = null;
List<Ficha> listaFichas = null;
List<Rendimiento> listaRendimientos = null;

boolean REGLA_COMENZAR_CIERRE_1 = false;
boolean FUERTE_AGOTAMIENTO = false;
boolean PELIGRO_INVISIBLE = false;
boolean PRE_EVENTO = false;
boolean REGLA_DESCARGA_BSUPERIOR = false;
boolean REGLA_TARGET_PREVENTIVO = false;
boolean TOCAMOS_BSUPERIOR = false;
boolean DESCARGA_VENCIMIENTO = false;
boolean DESCARGA_POR_SALIDA = false;
boolean descargaPrevia = false;

double resultadoMoney = 0;
double resultadoJuego = 0;
double valor_fuerte_stoch_ASC = 0;
double valor_fuerte_stoch_DESC = 0;
double DIAS_MARCO_TEMPORAL = 1; //valor default 1
double STOCH_ANTERIOR = 0.0;
double STOCH_PRE_ANTERIOR = 0.0;
double ATR_SEMILLA = 0;
double ULTIMO_TOP_PRICE = 0;
double ACTUAL_TOP_PRICE = 0;
double precioDescarga = 0;
double DESCARGA_VENCIMIENTO_PRECIO = 0;
double delta_porcentaje_precio;
double balanceAcumulado = 0;
double precio_operacion_original_DESC = 0; 
double precio_operacion_original_ASC = 0;
double ultimoMejorOBV_ASC = 0;

Operacion ultimaOperacionAbierta;
ElliottData vElliottData = null;
Rendimiento vRendimiento = null;
Medida maxRSI_compra = null;
Medida minRSI_compra = null;

Calendar FECHA_CIERRE_FORZADO = null;
String[] timeFrame = null;;
String nombrePapel = null;
String fechaStringCorteDebug = "";
boolean hayNuevoMaximo = false;
int CONT_BLOQUEO_CARGA_NUEVOS_MINIMOS = 0;
int CARGA_BLOQUE = 10;
int CARGA_CONTAINER = 50;
boolean YA_CARGAMOS_BY_ELLIOTT = false;
boolean CARGA_BLOQUE_APAGADA = false;
int CONT_ELEF_MINI_ROJO_ASC = 0;
boolean DESCARGA_GRUESA_ASC = false;
double balanceAnterior = 0;
int diasCargando = 0;
int diasTranscurridos = 0;
String GUION_MEDIO = "-";
double semillaSTOCH = 0;
int pasosGiroStoch = 0;
String fechaDesdeBuscarSemillaSTOCH = null;
int contDivergenciaBajista = 0;

	/**
	 * 
	 * @param listaCotizaciones
	 * @return
	 */
	public Resumen execute(List <Cotizacion>listaCotizaciones){
		
		nombrePapel = listaCotizaciones.get(0).getNombrePapel();
		final MetricasPapel vMetricasPapel = AdministradorIndicadores.generaMetricasPapel(nombrePapel,listaCotizaciones);
		final List <CalculoMetrica> listaCalculoMetrica_OBV = vMetricasPapel.getListaCalculoMetrica_OBV();	
		final List <CalculoMetricaSTOCH> listaCalculoMetrica_STOCH = vMetricasPapel.getListaCalculoMetrica_STOCH(); 	
		final List <CalculoMetricaBand> listaCalculoMetrica_BAND= vMetricasPapel.getListaCalculoMetrica_BAND();	
		final List <CalculoMetrica> listaCalculoMetrica_ATR = vMetricasPapel.getListaCalculoMetrica_ATR();	
		final List <CalculoMetrica> listaCalculoMetrica_MMT_15 = vMetricasPapel.getListaCalculoMetrica_MMT_15();
		final List <CalculoMetrica> listaCalculoMetrica_MMT_20 = vMetricasPapel.getListaCalculoMetrica_MMT_20();
		
		MetricasPapel vMetricasPapelSector = AdministradorIndicadores.generaMetricasPapel(MetodosDeLogica.getPapelSector(nombrePapel), null);
		
		timeFrame = REA_PropertiesController.TIME_FRAMES.split("-"); 
				
		int longitudListaCotizaciones = 0;
		boolean short_activo = false;
		boolean compra_activa = false;
		Resumen resumen = null;
		
		//Refresco		
		DIAS_MARCO_TEMPORAL=1;
		lista_resultados_operaciones = new ArrayList<Operacion>();
		listaCotizacionesDentroRangoFechas = new ArrayList<Cotizacion>();
		listaFichasPiñonHistorico = new ArrayList<FichaPiñon>();	
		listaRendimientos = new ArrayList<Rendimiento>();
								
		valor_fuerte_stoch_ASC = getFuerteStochPapel(true, listaCotizaciones.get(0).getNombrePapel());
		valor_fuerte_stoch_DESC = getFuerteStochPapel(false, listaCotizaciones.get(0).getNombrePapel());
		delta_porcentaje_precio = MetodosDeLogica.get_DELTA_PORCENTAJE_PRECIO(nombrePapel);
		
		if(REA_PropertiesController.DIAS_MARCO_TEMPORAL != null){
			DIAS_MARCO_TEMPORAL = Integer.parseInt(REA_PropertiesController.DIAS_MARCO_TEMPORAL);
		}
		
		FECHA_CIERRE_FORZADO = null;
		if(REA_PropertiesController.FECHA_CIERRE_FORZADO != null){
			FECHA_CIERRE_FORZADO = MetodosFechas.crearFechaString(REA_PropertiesController.FECHA_CIERRE_FORZADO.split("-"));
		}
		
		if(MetodosDeLogica.aplica_TrazeoFull()) {			
			System.out.println("DIAS_MARCO_TEMPORAL:" + DIAS_MARCO_TEMPORAL);
			System.out.println("DESVIO_MAXIMO del " + nombrePapel + ": " + MetodosDeLogica.getDesviosMaximosAlcanzados(nombrePapel));
			System.out.println("ELLIOTT en " + nombrePapel + ": " + MetodosDeLogica.aplica_Elliott(nombrePapel));
			System.out.println("APLICA TRAZEO FULL: " + MetodosDeLogica.aplica_TrazeoFull());
			System.out.println("PORCENTAJE PELIGRO INVISIBLE: " + MetodosDeLogica.retornar_PorcentajePeligroInvisible(nombrePapel));
			System.out.println("RUTA ATR: " + MetodosDeLogica.aplica_RutaATR(nombrePapel));
			System.out.println("DELTA PORCENTAJE PRECIO: " + delta_porcentaje_precio);
			System.out.println("APLICA_CARGA_BLOQUE: " + MetodosDeLogica.aplica_cargaBloque());
			System.out.println("APLICA_RESCATE SEMILLAS: " + MetodosDeLogica.aplica_RescateSemillas());
			System.out.println("PORCENTAJE RESCATE: " + MetodosDeLogica.getPORCENTAJE_RESCATE());
			System.out.println("MESES_PERMITIDOS_E: " + MetodosDeLogica.getMesesPermitidosElliott());
		}
				
	    //Definicion minimos lectura	
		int[] longitudes_array = new int[3];
		longitudes_array[0] = listaCalculoMetrica_OBV.size();
		longitudes_array[1] = listaCalculoMetrica_STOCH.size();
		longitudes_array[2] = listaCalculoMetrica_BAND.size();
	
		longitudListaCotizaciones = calculoMinimos(longitudes_array);		
		
		Calendar vCalendarDesde = MetodosFechas.crearFechaString(Posicion_PropertiesController.FECHA_DESDE_PROCESO.split("-"));
		Calendar vCalendarHasta = MetodosFechas.crearFechaString(Posicion_PropertiesController.FECHA_HASTA_PROCESO.split("-"));
		
		int contadorDiasSemana = 0;
		
		for (int j=0; j<longitudListaCotizaciones; j++){
			
		  //fechaStringCorteDebug = "2016-04-05";
		  if(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()).equals(fechaStringCorteDebug)){
			System.out.println("");
	      }
			
		  if( ! ((listaCotizaciones.get(j).getFecha().after(vCalendarDesde.getTime())) &&
		     (listaCotizaciones.get(j).getFecha().before(vCalendarHasta.getTime()))))
		  {
			continue; //no estan dentro del rango  
		  }
		  
		  contadorDiasSemana++;
		  if(contadorDiasSemana < DIAS_MARCO_TEMPORAL && j < longitudListaCotizaciones-1 ){
			  continue;
		  }
		  contadorDiasSemana = 0; //para recontar dias de semana
		  
		  listaCotizacionesDentroRangoFechas.add(listaCotizaciones.get(j));
		  
		  //1-Control nulidad valores previos 
		  int m_1 = j-1;
		  int m_2 = j-2;
		  if (j==0 ||j==1 ||j==2) m_1 = m_2 = j; 
		  
		  //2-Shorteamos?
		  if(!short_activo && !compra_activa ){
			  short_activo = shorteamos_SASC(
								  j,
								  listaCotizaciones,
								  listaCalculoMetrica_STOCH,
								  listaCalculoMetrica_BAND,
								  listaCalculoMetrica_ATR,
								  listaCalculoMetrica_MMT_15,
								  listaCalculoMetrica_OBV,
								  true
						 	);
			  if(short_activo) {//salvar ganancia
				  //limpiarPiñonesHistoricos(listaCotizaciones.get(j).getPrecio_cierre());
				  continue;
			  }
		  }	
			  
		   //3-Cerramos Short?
		   if (short_activo){
			   short_activo = !cerramosShort_SASC(
								   j,m_1,m_2,
								   listaCotizaciones,
								   listaCalculoMetrica_STOCH,
								   listaCalculoMetrica_BAND,
								   listaCalculoMetrica_ATR,
								   listaCalculoMetrica_MMT_20,
								   listaCalculoMetrica_MMT_15,
								   listaCalculoMetrica_OBV
							   );
			   
			   if(short_activo){//salvar ganancia
				   //limpiarPiñonesHistoricos(listaCotizaciones.get(j).getPrecio_cierre());
			   }
			   
		   }
	 
		   //4- Compramos?
		   boolean recienComprada = false;
		   if(!compra_activa && !short_activo){
			   compra_activa = compramos_SASC(
								   j,
								   listaCotizaciones,
								   listaCalculoMetrica_STOCH,
								   listaCalculoMetrica_ATR,
								   listaCalculoMetrica_BAND,
								   listaCalculoMetrica_MMT_20,
								   listaCalculoMetrica_MMT_15,
								   listaCalculoMetrica_OBV,
								   true
							   );
			   if(compra_activa) recienComprada = true;
		   }
			
			//5- Cerramos COMPRA?
			if (compra_activa && !recienComprada){
				compra_activa = !cerramosCompra_SASC(
										j,
										listaCotizaciones,
										listaCalculoMetrica_OBV,
										listaCalculoMetrica_STOCH,
										listaCalculoMetrica_BAND,
										listaCalculoMetrica_ATR,
										listaCalculoMetrica_MMT_15,
										listaCalculoMetrica_MMT_20,
										vMetricasPapelSector
								);
			}
			
			if(!short_activo && !compra_activa){

				final double STOCH_MAIN = listaCalculoMetrica_STOCH.get(j).getValor_main();
				final double STOCH_SIGNAL = listaCalculoMetrica_STOCH.get(j).getValor_signal();
				final double STOCH = STOCH_MAIN- STOCH_SIGNAL;

				double bandaSuperior = listaCalculoMetrica_BAND.get(j).getValor_banda_superior();
				double bandaMedia = listaCalculoMetrica_BAND.get(j).getValor_banda_media();
				final double ATR_MAIN = listaCalculoMetrica_ATR.get(j).getIndicador_valor();  
				
				final String trazas = 
					 "STOCH:" + df.format(STOCH) +
					 "- B.Sup:" + df.format(bandaSuperior) +
					 "- B.Med:" + df.format(bandaMedia) +
					 "- P.Max:" + df.format(listaCotizaciones.get(j).getPrecio_maximo()) +
					 "- P.Min:" + df.format(listaCotizaciones.get(j).getPrecio_minimo()) +
					 "- ATR:" + df.format(ATR_MAIN) +
					 "- VOL:" + df_enteros.format(listaCotizaciones.get(j).getVolumen());
					 
				final String trazasMMT15 = "MMT:" + listaCalculoMetrica_MMT_15.get(j).getIndicador_valor_formateado() + 
					   "- MMT_03:" + listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor_formateado() +
					   "- MMT_05:" + listaCalculoMetrica_MMT_15.get(j-5).getIndicador_valor_formateado() +
					   "- OBV:" + df_enteros.format(listaCalculoMetrica_OBV.get(j).getIndicador_valor()) +
				       "- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre());
										         
		        if(MetodosDeLogica.aplica_TrazeoFull()){
		        	System.out.println(
	        			MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	        			"- XXX->>" + trazas);
		        	
		        	System.out.println(
		        			MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		        			"- XXX->>" + trazasMMT15);
		        }
			}
	
		}//end form
		
	/* =============================================================== */
		
		//TOTAL->> ganancia_acumulada_SHORT 
		double ultimoPrecioValido = 0;
		if (short_activo || compra_activa){
			double resultado = resultadoMoney+resultadoJuego;
			
			for(Cotizacion vCotizacion :listaCotizacionesDentroRangoFechas){
				if (vCotizacion.getFecha().equals(ultimaOperacionAbierta.getFecha())){
					ultimoPrecioValido = listaCotizacionesDentroRangoFechas.get(listaCotizacionesDentroRangoFechas.size()-1).getPrecio_cierre();
					ultimaOperacionAbierta.setPrecio_cierre_operacion(ultimoPrecioValido);
					ultimaOperacionAbierta.setResultado(resultado);
					ultimaOperacionAbierta.setShort(short_activo);
					break;
				}
			}
						
			String operacion = "";
			if(compra_activa){
				operacion = "Compra:";
			}else{
				operacion = "Short:";
			}
									
			String desgloseResultado = "(RMoney:"+ df.format(resultadoMoney) + "- RJuego:" + df.format(resultadoJuego) + ")";
			System.out.println(
					listaCotizaciones.get(0).getNombrePapel() + "->>" + operacion +
					//df.format(resultado) +
					df.format(resultadoJuego) +
					desgloseResultado +
					"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
					"- DiasCarga:" + df_enteros.format(diasCargando) +
					"- Prec:" + df.format(ultimoPrecioValido)
			);
			
			if(vElliottData != null)
				vElliottData.mostrarValoresElliott(null, null);
			
			resumen = new Resumen();
			resumen.setSimbolo_papel(listaCotizaciones.get(0).getNombrePapel());
			resumen.setOperacion(operacion);
			//resumen.setResultado(df.format(resultado));
			resumen.setResultado(df.format(resultadoJuego));
			resumen.setDesglose_resultado(desgloseResultado);
			resumen.setUnidades_operadas(MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas));
			resumen.setUltimo_precio_papel(ultimoPrecioValido); 
			resumen.setDiasCargando(diasCargando);
			if(compra_activa){
				resumen.setSegnalIngreso(true);
			}else{
				resumen.setSegnalIngreso(false);
			}
			
		    //Estadisticas Final
		    lista_resultados_operaciones.add(ultimaOperacionAbierta);	
		    
	    }else{
	    	resumen = new Resumen();
	    	resumen.setSimbolo_papel(listaCotizaciones.get(0).getNombrePapel());
			resumen.setOperacion("N/A:");
			resumen.setResultado(df.format(0));
			resumen.setDesglose_resultado("0");
			resumen.setUnidades_operadas(0);
			resumen.setSegnalIngreso(false);
			
			ultimoPrecioValido = listaCotizacionesDentroRangoFechas.get(listaCotizacionesDentroRangoFechas.size()-1).getPrecio_cierre();
			resumen.setUltimo_precio_papel(ultimoPrecioValido); 
			
	    }			     
		
//Tendencias del dia
		cargamosSegunTendenciaTimeFrames(true, true,listaCotizaciones.size()-1,listaCotizaciones);
			
		calculos_cierre_previo(0,hs_48,listaCotizaciones, listaCalculoMetrica_OBV, listaCalculoMetrica_STOCH, listaCalculoMetrica_BAND,listaCalculoMetrica_ATR,listaCalculoMetrica_MMT_20,true);
		calculos_cierre_previo(0,hs_24,listaCotizaciones, listaCalculoMetrica_OBV,listaCalculoMetrica_STOCH, listaCalculoMetrica_BAND,listaCalculoMetrica_ATR,listaCalculoMetrica_MMT_20,true);
		calculos_cierre_previo(0,hs_0,listaCotizaciones, listaCalculoMetrica_OBV, listaCalculoMetrica_STOCH, listaCalculoMetrica_BAND,listaCalculoMetrica_ATR,listaCalculoMetrica_MMT_20,true);
		
		mostrarResultadosEstadisticos(resumen);
		
		if(MetodosDeLogica.aplica_TrazeoFull())
			mostrarRendimientos();
		
//		if(MetodosDeLogica.aplica_TrazeoFull())
//			mostrarResultadoPiñones(ultimoPrecioValido);
		
		return resumen;
	}
	
	/**
	 * 
	 * @param j
	 * @param listaCotizaciones
	 * @param listaCalculoMetrica_STOCH
	 * @param listaCalculoMetrica_BAND
	 * @return
	 */
	private Boolean shorteamos_SASC(
			final int j,
			final List<Cotizacion> listaCotizaciones, 
			final List<CalculoMetricaSTOCH> listaCalculoMetrica_STOCH, 
			final List<CalculoMetricaBand> listaCalculoMetrica_BAND,
			final List<CalculoMetrica> listaCalculoMetrica_ATR,
			final List<CalculoMetrica> listaCalculoMetrica_MMT_15,
			final List<CalculoMetrica> listaCalculoMetrica_OBV,
			boolean esVisible)
	{
		
		  //fechaStringCorteDebug = "2013-03-20";
		  if(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()).equals(fechaStringCorteDebug)){
				System.out.println("");
		  }
		
		  ATR_SEMILLA = listaCalculoMetrica_ATR.get(j).getIndicador_valor();
		  final double STOCH_MAIN = listaCalculoMetrica_STOCH.get(j).getValor_main();
		  final double STOCH_SIGNAL = listaCalculoMetrica_STOCH.get(j).getValor_signal();
		  final double DELTA_STOCH = STOCH_MAIN * 0.05;
	      boolean REGLA_NO_HAY_ELEFANTE_ROJO = STOCH_MAIN > STOCH_SIGNAL && (STOCH_MAIN- STOCH_SIGNAL) > DELTA_STOCH ;
	
	      if(REGLA_NO_HAY_ELEFANTE_ROJO){
	    	  return false;
	      }
	      
	      double bandaSuperior = listaCalculoMetrica_BAND.get(j).getValor_banda_superior();
		  double bandaMedia = listaCalculoMetrica_BAND.get(j).getValor_banda_media();
			
	      boolean shorteamos = false;
	      boolean REGLA_NACE_ELEFANTE_ROJO = STOCH_SIGNAL > STOCH_MAIN && STOCH_SIGNAL < 85 &&  (STOCH_SIGNAL- STOCH_MAIN) > DELTA_STOCH;
	      if(REGLA_NACE_ELEFANTE_ROJO){
	    	  shorteamos = true;
	    	 
			  if(esVisible)
			  System.out.println(
					MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
					"- SHORTEAR->REGLA_ELEFANTE_ROJO" +
					"- Stoch:" + df.format(STOCH_SIGNAL-STOCH_MAIN) +
					"- B.Sup:" + df.format(bandaSuperior) +
				    "- B.Med:" + df.format(bandaMedia) +
				    "- P.Max:" + df.format(listaCotizaciones.get(j).getPrecio_maximo()) +
				    "- P.Min:" + df.format(listaCotizaciones.get(j).getPrecio_minimo()) +
					"- ATR_Semilla:" +  df.format(ATR_SEMILLA) +
					"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()) 
	    	  );
	      }
	      
	      boolean REGLA_PRECIO_MENOR_BANDA_INFERIOR = listaCotizaciones.get(j).getPrecio_cierre() < listaCalculoMetrica_BAND.get(j).getValor_banda_inferior();
	      if(!shorteamos && REGLA_PRECIO_MENOR_BANDA_INFERIOR){
	    	  shorteamos = true;
	    	  
			  if(esVisible)
	    	  System.out.println(
					MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
					"- SHORTEAR->PREC_MENOR_BAND_INF:" + df.format(listaCalculoMetrica_BAND.get(j).getValor_banda_inferior()) +
					"- Stoch:" + df.format(STOCH_SIGNAL-STOCH_MAIN) +
					"- B.Sup:" + df.format(bandaSuperior) +
				    "- B.Med:" + df.format(bandaMedia) +
				    "- P.Max:" + df.format(listaCotizaciones.get(j).getPrecio_maximo()) +
				    "- P.Min:" + df.format(listaCotizaciones.get(j).getPrecio_minimo()) +
					"- ATR_Semilla:" +  df.format(ATR_SEMILLA) +
					"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()) 
	    	  );
	      }
	      
	      if(shorteamos){
	    	  precio_operacion_original_DESC = listaCotizaciones.get(j).getPrecio_cierre();
	    	  REGLA_COMENZAR_CIERRE_1 = false;
	    	  FUERTE_AGOTAMIENTO = false;
	    	  PRE_EVENTO = false;
	    	  REGLA_TARGET_PREVENTIVO = false;
	    	  REGLA_DESCARGA_BSUPERIOR = false;
	    	  PELIGRO_INVISIBLE = false;
	    	  DESCARGA_POR_SALIDA = false;
	    	  
	    	  resultadoMoney = 0;
			  resultadoJuego = 0;
			  listaUnidadesOperadas = new ArrayList<UnidadOperada>();
			  ultimaOperacionAbierta = new Operacion(listaCotizaciones.get(j).getFecha(), 0, 0,true);
			  			  
			  UnidadOperada vUnidadOperada = new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j));
			  listaUnidadesOperadas.add(vUnidadOperada);
			  
			  listaFichas = new ArrayList<Ficha>();
			  listaFichas.add(new Ficha(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),true));
			   
	    	  STOCH_PRE_ANTERIOR =-1000;
			  STOCH_ANTERIOR =-1000;
			  diasCargando = 0;

			  if(esVisible)
					System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- DESC->>" +
							"MMT:" + df.format(listaCalculoMetrica_MMT_15.get(j).getIndicador_valor()) +
							"- MMT_03:" + df.format(listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor()) +
						    "- MMT_05:" + df.format(listaCalculoMetrica_MMT_15.get(j-5).getIndicador_valor()) +
						    "- OBV:" + df.format(listaCalculoMetrica_OBV.get(j).getIndicador_valor()) +
						    "- VOL:" + df_enteros.format(listaCotizaciones.get(j).getVolumen())
					);
			  
			  return true;
	      }
	      
		return false;
	}
	
	/**
	 * Usado para consulta puntual
	 * @param j
	 * @param listaCotizaciones
	 * @param listaCalculoMetrica_STOCH
	 * @param listaCalculoMetrica_BAND
	 * @param listaCalculoMetrica_ATR
	 * @param isVisible
	 * @return
	 */
	private Boolean shorteamos_SASC_simula(
			final int j,
			final List<Cotizacion> listaCotizaciones, 
			final List<CalculoMetricaSTOCH> listaCalculoMetrica_STOCH, 
			final List<CalculoMetricaBand> listaCalculoMetrica_BAND,
			final List<CalculoMetrica> listaCalculoMetrica_ATR,
			boolean isVisible)
	{
		  double ATR_SEMILLA_SM = listaCalculoMetrica_ATR.get(j).getIndicador_valor();
		  final double STOCH_MAIN = listaCalculoMetrica_STOCH.get(j).getValor_main();
		  final double STOCH_SIGNAL = listaCalculoMetrica_STOCH.get(j).getValor_signal();
		  final double DELTA_STOCH = STOCH_MAIN * 0.05;
	      boolean REGLA_NO_HAY_ELEFANTE_ROJO = STOCH_MAIN > STOCH_SIGNAL && (STOCH_MAIN- STOCH_SIGNAL) > DELTA_STOCH ;
	
	      if(REGLA_NO_HAY_ELEFANTE_ROJO){
	    	  return false;
	      }    
	      
	      boolean shorteamos = false;
	      boolean REGLA_NACE_ELEFANTE_ROJO = STOCH_SIGNAL > STOCH_MAIN && STOCH_SIGNAL < 85 &&  (STOCH_SIGNAL- STOCH_MAIN) > DELTA_STOCH;
	      if(REGLA_NACE_ELEFANTE_ROJO){
	    	  shorteamos = true;
	    	 
			  if(isVisible)
			  System.out.println(
					MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
					"- SHORTEAR->REGLA_NACE_ELEFANTE_ROJO-" +
					"- Stoch:" + df.format(STOCH_SIGNAL-STOCH_MAIN) +
					"- ATR_Semilla:" +  df.format(ATR_SEMILLA_SM) +
					"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()) 
	    	  );
	      }
	      
	      boolean REGLA_PRECIO_MENOR_BANDA_INFERIOR = listaCotizaciones.get(j).getPrecio_cierre() < listaCalculoMetrica_BAND.get(j).getValor_banda_inferior();
	      if(!shorteamos && REGLA_PRECIO_MENOR_BANDA_INFERIOR){
	    	  shorteamos = true;
	    	  
			  if(isVisible)
	    	  System.out.println(
					MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
					"- SHORTEAR->REGLA_PRECIO_MENOR_BANDA_INFERIOR- BandaInferior:" + df.format(listaCalculoMetrica_BAND.get(j).getValor_banda_inferior()) +
					"- Stoch:" + df.format(STOCH_SIGNAL-STOCH_MAIN) +
					"- ATR_Semilla:" +  df.format(ATR_SEMILLA_SM) +
					"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()) 
	    	  );
	      }
	      
	      if(shorteamos){
			  return true;
	      }
	      
		return false;
	}

	/**
	 * 
	 * @param j
	 * @param m_1
	 * @param m_2
	 * @param listaCotizaciones
	 * @param listaCalculoMetrica_STOCH
	 * @return
	 */
	private Boolean cerramosShort_SASC(
			int j, int m_1, int m_2, 
			List<Cotizacion> listaCotizaciones, 
			List<CalculoMetricaSTOCH> listaCalculoMetrica_STOCH,
			List<CalculoMetricaBand> listaCalculoMetrica_BAND,
			List<CalculoMetrica> listaCalculoMetrica_ATR,
			List <CalculoMetrica> listaCalculoMetrica_MMT_20,
			List <CalculoMetrica> listaCalculoMetrica_MMT_15,
			List <CalculoMetrica> listaCalculoMetrica_OBV) 
	{
		//fechaStringCorteDebug = "2015-06-04";
		if(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()).equals(fechaStringCorteDebug)){
			System.out.println("");
		}
		
		if(MetodosDeLogica.esFechaVencimiento(listaCotizaciones.get(j))){
			if(MetodosDeLogica.aplica_TrazeoFull())
				System.out.println(
					MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
					"- DESC->>MES VENCIMIENTO - SOLO CARAMELOS!!");
		}
		
		//Traza de Avance Precio
		String porcentaje_real = null;
		double precioActual_ = listaCotizaciones.get(j).getPrecio_cierre();
		double porcentaje = -1*(precioActual_- precio_operacion_original_DESC)*100/precio_operacion_original_DESC; 
		porcentaje_real = "(" + df.format(porcentaje) + "%)";
		
		int m=0;
		if(j>1) m=j-1;
		double balance_short_actual = calcularBalance(false,false,null,j,listaCotizaciones);
		final boolean bajoPrecio  = listaCotizaciones.get(j).getPrecio_cierre() <= listaCotizaciones.get(m).getPrecio_cierre();

		final double ATR_MAIN = listaCalculoMetrica_ATR.get(j).getIndicador_valor();
		
		final double STOCH_MAIN = listaCalculoMetrica_STOCH.get(j).getValor_main();
		final double STOCH_SIGNAL = listaCalculoMetrica_STOCH.get(j).getValor_signal();
		final double STOCH = STOCH_SIGNAL-STOCH_MAIN;
		
		double bandaSuperior = listaCalculoMetrica_BAND.get(j).getValor_banda_superior();
		double bandaMedia = listaCalculoMetrica_BAND.get(j).getValor_banda_media();
		final String desgloseResultado = "(RMoney:"+ df.format(resultadoMoney) + "- RJuego:" + df.format(resultadoJuego) + ")";	
		              
		//Cierre de operacion forzado			
		if(FECHA_CIERRE_FORZADO != null && cierreForzado(listaCotizaciones, j, balance_short_actual)){ 
			listaFichas = new ArrayList<Ficha>();
						
			System.out.println(
	        		MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	        		"- DESC->>CIERRE FORZADO VIA PROPERTIES" +
	        		desgloseResultado +
					"- B.Sup:" + df.format(bandaSuperior) +
					"- B.Med:" + df.format(bandaMedia) +
					"- P.Max:" + df.format(listaCotizaciones.get(j).getPrecio_maximo()) +
					"- ATR:" + df.format(ATR_MAIN) +
					"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
					"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre())); 
			return true;
		}

		String estado_ATR = "(-):";
		boolean ATR_BAJO = false;
		if(ATR_MAIN < ATR_SEMILLA){
			ATR_BAJO = true;
			estado_ATR = "(B):";
		} else{
			estado_ATR = "(A):";
		}
		
		if(!REGLA_COMENZAR_CIERRE_1 && STOCH > valor_fuerte_stoch_DESC){
	    	REGLA_COMENZAR_CIERRE_1 = true;	
	    	
	    	System.out.println(
				 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
        		 "- DESC->>FUERTE STOCH!!!- Stoch:" + df.format(STOCH));
	    }
		
		boolean STOCH_REDUCIENDO = false;
		if(!ATR_BAJO && STOCH <= STOCH_ANTERIOR && STOCH_ANTERIOR <= STOCH_PRE_ANTERIOR){
			STOCH_REDUCIENDO = true;
			System.out.println(
				MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
				"- DESC->>ALERTA- POSIBLE DESCARGA- STOCH MENOR a:" + df.format(STOCH_ANTERIOR));
		}
		
		if(!ATR_BAJO && STOCH <= STOCH_ANTERIOR && !(STOCH_ANTERIOR <= STOCH_PRE_ANTERIOR)){
			if(MetodosDeLogica.aplica_TrazeoFull())
				System.out.println(
					MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
					"- DESC->>WARNING- POSIBLE DESCARGA- STOCH MENOR a:" + df.format(STOCH_ANTERIOR));
		}
		STOCH_PRE_ANTERIOR = STOCH_ANTERIOR;
		STOCH_ANTERIOR = STOCH;
	
//Bloqueos en Descargas 
			boolean descarga_bloqueada = false;
			boolean esATR_DESC = false;
			boolean esOBV_DESC = false;
			boolean esVOL_DESC = false;
			
			if(MetodosDeLogica.aplica_RutaATR(nombrePapel)){
				
				 //Bloqueo Descarga - ATR DESC
			     boolean REGLA_ATR_DESC_01 = listaCalculoMetrica_ATR.get(j).getIndicador_valor() < listaCalculoMetrica_ATR.get(j-1).getIndicador_valor();
			     boolean REGLA_ATR_DESC_02 = listaCalculoMetrica_ATR.get(j).getIndicador_valor() < listaCalculoMetrica_ATR.get(j-2).getIndicador_valor();
			     boolean REGLA_ATR_DESC_03 = listaCalculoMetrica_ATR.get(j).getIndicador_valor() < listaCalculoMetrica_ATR.get(j-3).getIndicador_valor();
			     
			     int count = 0;
			     if(REGLA_ATR_DESC_01) count++;
			     if(REGLA_ATR_DESC_02) count++;
			     if(REGLA_ATR_DESC_03) count++;
			     
			     if(count>1){
			    	 descarga_bloqueada = esATR_DESC = true;
		    	     
			    	 if(!bajoPrecio) //cartel solo cuando sube precio
				    	 System.out.println(
							 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							 "- DESC->>DESCARGA BLOQUEADA - ATR_DESC:" + 
							 df.format(listaCalculoMetrica_ATR.get(j).getIndicador_valor())+
							 "- ATR_1:" + df.format(listaCalculoMetrica_ATR.get(j-1).getIndicador_valor())+
							 "- ATR_2:" + df.format(listaCalculoMetrica_ATR.get(j-2).getIndicador_valor())+
							 "- ATR_3:" + df.format(listaCalculoMetrica_ATR.get(j-3).getIndicador_valor()));
			     }
			     
			     //Bloqueo Descarga - OBV DESC
			     boolean REGLA_OBV_DESC_01 = listaCalculoMetrica_OBV.get(j).getIndicador_valor() < listaCalculoMetrica_OBV.get(j-1).getIndicador_valor();
			     boolean REGLA_OBV_DESC_02 = listaCalculoMetrica_OBV.get(j).getIndicador_valor() < listaCalculoMetrica_OBV.get(j-2).getIndicador_valor();
			     
			     if(!descarga_bloqueada && REGLA_OBV_DESC_01 && REGLA_OBV_DESC_02){
			    	 descarga_bloqueada = esOBV_DESC = true;
			    	 
			    	 if(!bajoPrecio) //cartel solo cuando sube precio
						 System.out.println(
							 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							 "- DESC->>DESCARGA BLOQUEADA -OBV_DESC:" + 
							 df_enteros.format(listaCalculoMetrica_OBV.get(j).getIndicador_valor())+
							 "- OBV_1:" + df_enteros.format(listaCalculoMetrica_OBV.get(j-1).getIndicador_valor())+
							 "- OBV_2:" + df_enteros.format(listaCalculoMetrica_OBV.get(j-2).getIndicador_valor()));
			     }
			     
			     //Bloqueo Descarga - VOL DESC
			     boolean REGLA_VOL_DESC_01 = listaCotizaciones.get(j).getVolumen() < listaCotizaciones.get(j-1).getVolumen();
			     		     
			     if(!descarga_bloqueada && REGLA_VOL_DESC_01 && bajoPrecio){
			    	 descarga_bloqueada = esVOL_DESC = true;
					 System.out.println(
						 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						 "- DESC->>DESCARGA BLOQUEADA -VOL_DESC:" + 
						 df_enteros.format(listaCotizaciones.get(j).getVolumen())+
						 "- VOL_1:" + df_enteros.format(listaCotizaciones.get(j-1).getVolumen()));
			     }
			}	
			
//Descarga unid.
	     	 boolean yaDescargamos = false;
	     	 long sizeAntesDescarga = MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas);
 
		     //Descarga MMT_REDUCIENDO	     	 
	     	 boolean REGLA_CONT_REDUCIENDOSE = false;
	     	 if(contador_salida_elefante_rojo >0 && contador_salida_elefante_rojo < sleep_salida_elefante_rojo){
	     		REGLA_CONT_REDUCIENDOSE = true;
	     	 }
	     	 
			 if(!descarga_bloqueada && STOCH_REDUCIENDO && REGLA_CONT_REDUCIENDOSE){
	    	 	 System.out.println(
						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						"- DESC->>DESCARGA_STOCH_REDUCIENDO" +
						"- Stoch:" + STOCH +
						"- Cont:" + contador_salida_elefante_rojo);
	    	 	 
		     	 yaDescargamos = true;
	        	 balance_short_actual = descargaUnidades(j,listaCotizaciones,balance_short_actual,false);
	        	 balanceAcumulado += balance_short_actual;
	        	 descargaPrevia = true;
		    	 
		    	 if(balance_short_actual > 0){
	    	    	 final Rendimiento vRendimiento = new Rendimiento(listaCotizaciones.get(j).getFecha(), df.format(balance_short_actual),porcentaje_real,false,sizeAntesDescarga,diasCargando);
	    	    	 listaRendimientos.add(vRendimiento);
	    	     }
		    	 
	        	 //desacople de resultado de descargas
		    	 if (sizeAntesDescarga >0)
		    		 cerrarOperacion(j, listaCotizaciones, balance_short_actual,STOCH,false);
		    	 
		     }
			 
			 //Descarga MMT_REDUCIENDO
			 double MMT__0 = listaCalculoMetrica_MMT_15.get(j).getIndicador_valor(); 
		 	 double MMT__3 = listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor();
		 	 double MMT__5 = listaCalculoMetrica_MMT_15.get(j-5).getIndicador_valor();
				
			 if(STOCH_REDUCIENDO && MMT__0<MMT__3 && MMT__0<MMT__5){
				 System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- DESC->>DESCARGA_MMT_REDUCIENDO" +
							"- Stoch:" + STOCH +
							"- MMT:" + listaCalculoMetrica_MMT_15.get(j).getIndicador_valor_formateado() + 
							"- MMT_03:" + listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor_formateado() +
							"- MMT_05:" + listaCalculoMetrica_MMT_15.get(j-5).getIndicador_valor_formateado()
				 );
		    	 	 
			     yaDescargamos = true;
		         balance_short_actual = descargaUnidades(j,listaCotizaciones,balance_short_actual,false);
		         balanceAcumulado += balance_short_actual;
		         descargaPrevia = true;
			    	 
			     if(balance_short_actual > 0){
		    	   	 final Rendimiento vRendimiento = new Rendimiento(listaCotizaciones.get(j).getFecha(), df.format(balance_short_actual),porcentaje_real,false,sizeAntesDescarga,diasCargando);
		    	   	 listaRendimientos.add(vRendimiento);
		    	 }
			    	 
		         //desacople de resultado de descargas
			     if (sizeAntesDescarga >0)
			    	 cerrarOperacion(j, listaCotizaciones, balance_short_actual,STOCH,false);
					
			 }
						
//Carga unid.	
			boolean esCargaProgresiva = false;

			if(!yaDescargamos && !MetodosDeLogica.aplica_RutaATR(nombrePapel)){
				boolean REGLA_PREC_MIN_DESC_1 = listaCotizaciones.get(j).getPrecio_minimo() < listaCotizaciones.get(j-1).getPrecio_minimo();
		    	boolean REGLA_PREC_MIN_DESC_2 = listaCotizaciones.get(j).getPrecio_minimo() < listaCotizaciones.get(j-2).getPrecio_minimo();
		    	boolean REGLA_PREC_MIN_DESC_3 = listaCotizaciones.get(j).getPrecio_minimo() < listaCotizaciones.get(j-3).getPrecio_minimo();
			    	 
		    	int count = 0;
			    if(REGLA_PREC_MIN_DESC_1) count++;
			    if(REGLA_PREC_MIN_DESC_2) count++;
			    if(REGLA_PREC_MIN_DESC_3) count++;
		    	 
				if(count > 1){
					esCargaProgresiva = true;
					UnidadOperada vUnidadOperada = null;
					diasCargando++;
	   			 	vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j));
					listaUnidadesOperadas.add(vUnidadOperada);
					 
					if(MetodosDeLogica.aplica_TrazeoFull()){
						System.out.println(
							 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			    	  		 "- DESC->>UNIDAD CARGADA-DESC_PREGRESIVO " +
					         vUnidadOperada.toString() +
			    	  		 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)); 
					 }	 
				}
			}
			
	    	if(!yaDescargamos && esATR_DESC || esOBV_DESC || esVOL_DESC || esCargaProgresiva){
	    		UnidadOperada vUnidadOperada = null;
	    		diasCargando++;
   			 	vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j));
				listaUnidadesOperadas.add(vUnidadOperada);
				 
				if(MetodosDeLogica.aplica_TrazeoFull()){
					String tipoCarga = "ATR_DESC:";
		    		if(esOBV_DESC)tipoCarga = "OBV_DESC:";
		    		if(esVOL_DESC)tipoCarga = "VOL_DESC:";		
				 
					System.out.println(
						 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    	  		 "- DESC->>UNIDAD CARGADA- " + tipoCarga +
				         vUnidadOperada.toString() +
		    	  		 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)); 
				 }
	    	}
			
	 		String trazas = "STOCH:" + df.format(STOCH) +
		     		 "- B.Sup:" + df.format(bandaSuperior) +
					 "- B.Med:" + df.format(bandaMedia) +
					 "- P.Max:" + df.format(listaCotizaciones.get(j).getPrecio_maximo()) +
					 "- P.Min:" + df.format(listaCotizaciones.get(j).getPrecio_minimo()) +
					 "- ATR" + estado_ATR + df.format(ATR_MAIN) + 
					 "- VOL:" + df_enteros.format(listaCotizaciones.get(j).getVolumen());
			
			String trazasMMT15 = "";
			if(MetodosDeLogica.aplica_TrazeoFull())
				trazasMMT15 = "-MMT:" + listaCalculoMetrica_MMT_15.get(j).getIndicador_valor_formateado() + 
				   "- MMT_03:" + listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor_formateado() +
				   "- MMT_05:" + listaCalculoMetrica_MMT_15.get(j-5).getIndicador_valor_formateado() +
				   "- OBV:" + df_enteros.format(listaCalculoMetrica_OBV.get(j).getIndicador_valor());
			 
	    	if(MetodosDeLogica.aplica_TrazeoFull())
	    		System.out.println(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) + "- DESC->>" + trazas );

	    	 
	 	    if (bajoPrecio){ //Ganancias
		    	 contador_salida_elefante_rojo = 0;    	 
		    	 
		    	 if(!yaDescargamos && !FUERTE_AGOTAMIENTO && MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) == 0){
			    		UnidadOperada vUnidadOperada = new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j));
						listaUnidadesOperadas.add(vUnidadOperada);
						
			    	  	System.out.println(
			    	  		MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			    	  		"- DESC->>UNIDAD MINIMA CARGADA:" + vUnidadOperada.toString() +
			    	  		"- Unidades:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas));
				 }	
	    	 
		         System.out.println(
		        		MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		        		"- DESC->>MANTENER:" + 
		        		df.format(resultadoJuego) +
		        		porcentaje_real +
		        		desgloseResultado +
		        		trazasMMT15 +
						"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
						"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre())); 
		         return false;
	     }
	    
	     if (!bajoPrecio){ //Perdidas >> Prec deberia bajar en shorteo
	    	 
				double MMT_0 = listaCalculoMetrica_MMT_15.get(j).getIndicador_valor(); 
		 		double MMT_3 = listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor();
		 		double MMT_5 = listaCalculoMetrica_MMT_15.get(j-5).getIndicador_valor();
				
				if(MMT_0 < MMT_3 && MMT_0 < MMT_5){
					 contador_salida_elefante_rojo = 0;
					 System.out.println(
			          		   	MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			          		   	"- DESC->>MMT_ROJO:" + 
			          		    df.format(resultadoJuego) +
			          		   	porcentaje_real +
			  	         		desgloseResultado +
			  	         		trazasMMT15 +
			             		"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
								"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()));
			         return false;
				}
				
//Control de Fichas
	    	 String mensajeFicha = null;
	    	 	if(esFichaNueva(listaCotizaciones,j,balance_short_actual)){
	    	 		
	    	 		mensajeFicha =
		    				 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			        		 "- DESC->>FICHA n°"  + (listaFichas.size()-1)  +
		    				 "- Ficha:" + df.format(listaFichas.get(listaFichas.size()-1).getPrecio_cierre_operacion());
	    	 	}
	    	 
	    	 	String mensajeFichaSalir = null;
	    	 	if(
	    	 		//contador_salida_elefante_rojo > sleep_salida_elefante_rojo &&
	    	 		!esCargaProgresiva && 
	    	 		MetodosDeLogica.validarGiros_papel(nombrePapel) &&
	    	 		!esFichaFalsa(listaCotizaciones,j, listaCalculoMetrica_ATR ))
    	 		{	 
					//Validacion MMT(20) 
					boolean REGLA_MMT_0_1 = listaCalculoMetrica_MMT_20.get(j).getIndicador_valor() > listaCalculoMetrica_MMT_20.get(j-1).getIndicador_valor();
					boolean REGLA_MMT_0_2 = listaCalculoMetrica_MMT_20.get(j).getIndicador_valor() > listaCalculoMetrica_MMT_20.get(j-2).getIndicador_valor();
					
					if(MetodosDeLogica.aplica_TrazeoFull())
						 System.out.println(
								MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
								"- DESC->>Validacion MMT(20)" +
								" -MMT_0:" + df.format(listaCalculoMetrica_MMT_20.get(j).getIndicador_valor()) +
								" -MMT_1:" + df.format(listaCalculoMetrica_MMT_20.get(j-1).getIndicador_valor()) +
								" -MMT_2:" + df.format(listaCalculoMetrica_MMT_20.get(j-2).getIndicador_valor()));
					
					if(REGLA_MMT_0_1 && REGLA_MMT_0_2){

						//Rearmando fichas
					     listaFichas = new ArrayList<Ficha>();
						 listaFichas.add(new Ficha(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),true));
						 mensajeFicha =
			    				 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
				        		 "- FICHA n°"  + (listaFichas.size()-1)  +
			    				 "- Ficha:" + df.format(listaFichas.get(listaFichas.size()-1).getPrecio_cierre_operacion());
		    	 		
						 mensajeFichaSalir =
			          		   	MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			          		   	"- DESC->>SALIR POR FICHA VALIDA:" + 
			          		    df.format(resultadoJuego) +
			          		   	porcentaje_real +
			          		   	desgloseResultado +
			          		   	trazasMMT15 +
								"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
								"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()); 
					     
						 System.out.println(mensajeFicha);
						 System.out.println(mensajeFichaSalir);
						 
		    	 	     //Estadisticas
						 final Operacion vOperacion = new Operacion(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(), balance_short_actual,true);
						 lista_resultados_operaciones.add(vOperacion);
						 
						 if(balance_short_actual > 0){
			    	    	 final Rendimiento vRendimiento = new Rendimiento(listaCotizaciones.get(j).getFecha(), df.format(balance_short_actual),porcentaje_real,false,sizeAntesDescarga,diasCargando);
			    	    	 listaRendimientos.add(vRendimiento);
						 }
						 
				    	 return true;
					}	    	 	
    	 		}
	    	 	if(mensajeFicha!= null) System.out.println(mensajeFicha);
	    	 	
	    	 	if(REGLA_COMENZAR_CIERRE_1 && STOCH < valor_fuerte_stoch_DESC){
			     	if(balance_short_actual >0){
		     			FUERTE_AGOTAMIENTO = true;
			     		
		     			if(MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) > 0)
		     				System.out.println(
			    				 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
				        		 "- DESC->>FUERTE AGOTAMIENTO- Se validara descargar!- Stoch:" + df.format(STOCH));	
		     			
			     	}
		    	 }
	    	 
				 final double VARIACION_STOCH = STOCH_MAIN * 0.03;
				 boolean REGLA_CIERRA_ELEFANTE_ROJO = STOCH_MAIN > STOCH_SIGNAL && (STOCH_MAIN- STOCH_SIGNAL) > VARIACION_STOCH ;
			     if(REGLA_CIERRA_ELEFANTE_ROJO)
			     { 
			    	 contador_salida_elefante_rojo++;
			          if(contador_salida_elefante_rojo > sleep_salida_elefante_rojo){
			             //return true; //Para realizar operatoria del final
			          }else{
			        	 
			        	 double precioMax = listaCotizaciones.get(j).getPrecio_maximo();
			        	 double precioActual = listaCotizaciones.get(j).getPrecio_cierre();
			        	 
			        	 if(!(precioActual > bandaSuperior || precioMax > bandaSuperior)){
				        	 			    	    	 
				        	 System.out.println(
				          		   	MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
				          		   	"- DESC->>REING_ELEF_ROJO(" + contador_salida_elefante_rojo + "):" + 
				          		    df.format(resultadoJuego) +
				          		   	porcentaje_real +
				  	         		desgloseResultado +
				  	         		trazasMMT15 +
									"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
									"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()));
				             return false;
			        	 } 
			          }
			     }else{
	
			    	 System.out.println(
			          		   	MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			          		   	"- DESC->>ELEF_ROJO:" + 
			          		    df.format(resultadoJuego) +
			          		   	porcentaje_real +
			  	         		desgloseResultado +
			  	         		trazasMMT15 +
			             		"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
								"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()));
			         return false;
			     }	    		 
			     
			     //Evitar cierre con ATR DESC
		    	 if(esATR_DESC){
		    		 			      	 
			        System.out.println(
		        		 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		        		 "- DESC->>REGLA_ATR_DESC:" + 
		        		 df.format(resultadoJuego) +
		        		 porcentaje_real +
		        		 desgloseResultado +
		        		 trazasMMT15 +
						 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
						 "- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()));
			        
			        return false; 
		    	 }
		    	 		    	 
		    	 if(contador_salida_elefante_rojo > sleep_salida_elefante_rojo){
		    		 //return true; //Para realizar operatoria del final
		    	 }else{
		    		 
		    		  System.out.println(
				        		 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
				        		 "- DESC->>CONT_ROJO(" + contador_salida_elefante_rojo + ")" +
				        		 df.format(resultadoJuego) +
				        		 porcentaje_real +
				        		 desgloseResultado +
				        		 trazasMMT15 +
								 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
								 "- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()));
		    		   
		    		   contador_salida_elefante_rojo++;     
					   return false;  
		    	 }
	    	 
			     listaFichas = new ArrayList<Ficha>();
				 listaFichas.add(new Ficha(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),true));
				 ULTIMO_TOP_PRICE = 0;
				
		         balance_short_actual = descargaUnidades(j,listaCotizaciones,balance_short_actual,false);
		         balanceAcumulado += balance_short_actual;
					    	 
			     if(balance_short_actual > 0){
		    	   	 final Rendimiento vRendimiento = new Rendimiento(listaCotizaciones.get(j).getFecha(), df.format(balance_short_actual),porcentaje_real,false,sizeAntesDescarga,diasCargando);
		    	   	 listaRendimientos.add(vRendimiento);
		    	 }
				 
			     //Estadisticas
				 final Operacion vOperacion = new Operacion(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(), balance_short_actual,true);
				 lista_resultados_operaciones.add(vOperacion);
				 
				 final String desgloseResultadoCerrado = "(RMoney:"+ df.format(balance_short_actual) + "- RJuego:0,00)";
				 
			     System.out.println(
		          		   	MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		          		   	"- DESC->>SALIR:" + 
		          		    df.format(balance_short_actual) +
		          		   	porcentaje_real +
		          		   	desgloseResultadoCerrado +
		          		   	trazasMMT15 +							
							"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre())
				     );
		    	 return true; 
	     }
	     return false;
	}

	/**
	  * 
	  * @param j
	  * @param listaCotizaciones
	  * @param listaCalculoMetrica_STOCH
	  * @return
	  */
	private boolean compramos_SASC(
			int j,  
			List<Cotizacion> listaCotizaciones, 
			List<CalculoMetricaSTOCH> listaCalculoMetrica_STOCH,
			List<CalculoMetrica> listaCalculoMetrica_ATR,
			List<CalculoMetricaBand> listaCalculoMetrica_BAND,
			List <CalculoMetrica> listaCalculoMetrica_MMT_20,
			List <CalculoMetrica> listaCalculoMetrica_MMT_15,
			List <CalculoMetrica> listaCalculoMetrica_OBV,
			boolean esVisible)
	{
		//fechaStringCorteDebug = "2016-05-05";
		if(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()).equals(fechaStringCorteDebug)){
			System.out.println("");
    	}
		
		boolean compramos=false;
		ATR_SEMILLA = listaCalculoMetrica_ATR.get(j).getIndicador_valor()+listaCalculoMetrica_ATR.get(j).getIndicador_valor()*delta_ATR;
				
		double bandaSuperior = listaCalculoMetrica_BAND.get(j).getValor_banda_superior();
		double bandaMedia = listaCalculoMetrica_BAND.get(j).getValor_banda_media();
			
		final double STOCH_MAIN = listaCalculoMetrica_STOCH.get(j).getValor_main();
		final double STOCH_SIGNAL = listaCalculoMetrica_STOCH.get(j).getValor_signal();
		
		boolean esFichaCargada = false;
		if(MetodosDeLogica.validarGiros_papel(nombrePapel) && listaFichas != null && listaFichas.size()>0){

			boolean fichaValida = listaCotizaciones.get(j).getPrecio_cierre() >= listaFichas.get(0).getPrecio_cierre_operacion();
			if(fichaValida){
				
				//Validacion MMT(20) 
				boolean REGLA_MMT_0_1 = listaCalculoMetrica_MMT_20.get(j).getIndicador_valor() > listaCalculoMetrica_MMT_20.get(j-1).getIndicador_valor();
				boolean REGLA_MMT_0_2 = listaCalculoMetrica_MMT_20.get(j).getIndicador_valor() > listaCalculoMetrica_MMT_20.get(j-2).getIndicador_valor();
				
				if(REGLA_MMT_0_1 && REGLA_MMT_0_2){
					compramos = true;
					esFichaCargada = true;
					if(esVisible)
						System.out.println(
								MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
								"- COMPRAR->>REGLA_FICHA" +
								"- Ficha:" + listaFichas.get(0).getPrecio_cierre_operacion() +
								"- Stoch:" + df.format(STOCH_MAIN-STOCH_SIGNAL) +
								"- B.Sup:" + df.format(bandaSuperior) +
							    "- B.Med:" + df.format(bandaMedia) +
							    "- P.Max:" + df.format(listaCotizaciones.get(j).getPrecio_maximo()) +
							    "- P.Min:" + df.format(listaCotizaciones.get(j).getPrecio_minimo()) +
							    "- ATR_Semilla:" +  df.format(ATR_SEMILLA) +
								"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()) 
						);
				}
			}
		}
		
		if(!compramos){
			final double DIFERENCIA_SOPORTADA_STOCH = STOCH_MAIN * 0.03;
			boolean REGLA_ABRE_ELEFANTE_VERDE = STOCH_MAIN > STOCH_SIGNAL && STOCH_MAIN > valor_fuerte_stoch_ASC && (STOCH_MAIN- STOCH_SIGNAL) > DIFERENCIA_SOPORTADA_STOCH ;
			
			if(REGLA_ABRE_ELEFANTE_VERDE){
				compramos = true;
				
				if(esVisible)
					System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- COMPRAR->>REGLA_ELEFANTE_VERDE" +
							"- Stoch:" + df.format(STOCH_MAIN-STOCH_SIGNAL) +
							"- B.Sup:" + df.format(bandaSuperior) +
						    "- B.Med:" + df.format(bandaMedia) +
						    "- P.Max:" + df.format(listaCotizaciones.get(j).getPrecio_maximo()) +
						    "- P.Min:" + df.format(listaCotizaciones.get(j).getPrecio_minimo()) +
						    "- ATR_Semilla:" +  df.format(ATR_SEMILLA) +
							"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre())
					);
			}
		}
		
		if(!compramos){
			 boolean REGLA_ATR_ASC_01 = listaCalculoMetrica_ATR.get(j).getIndicador_valor() > listaCalculoMetrica_ATR.get(j-1).getIndicador_valor();
		     boolean REGLA_ATR_ASC_02 = listaCalculoMetrica_ATR.get(j).getIndicador_valor() > listaCalculoMetrica_ATR.get(j-2).getIndicador_valor();

		     if(REGLA_ATR_ASC_01 && REGLA_ATR_ASC_02){
				 
		    	 compramos = true;
				 if(esVisible)		    	
		    	    System.out.println(
						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						"- COMPRAR->>REGLA_ATR_ASC" +
						"- Stoch:" + df.format(STOCH_MAIN-STOCH_SIGNAL) +
						"- B.Sup:" + df.format(bandaSuperior) +
					    "- B.Med:" + df.format(bandaMedia) +
					    "- P.Max:" + df.format(listaCotizaciones.get(j).getPrecio_maximo()) +
					    "- P.Min:" + df.format(listaCotizaciones.get(j).getPrecio_minimo()) +
					    "- ATR_Semilla:" +  df.format(ATR_SEMILLA) +
						"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre())
					);
		     }
		}
		
		if(compramos){
			precio_operacion_original_ASC = listaCotizaciones.get(j).getPrecio_cierre();
			ultimaOperacionAbierta = new Operacion(listaCotizaciones.get(j).getFecha(), 0, 0,false);
			REGLA_COMENZAR_CIERRE_1 = false;
			FUERTE_AGOTAMIENTO = false;
			PRE_EVENTO = false;
			REGLA_TARGET_PREVENTIVO = false;
			REGLA_DESCARGA_BSUPERIOR = false;
			PELIGRO_INVISIBLE = false;
			DESCARGA_POR_SALIDA = false;
			resultadoMoney = 0;
			resultadoJuego = 0;
			listaUnidadesOperadas = new ArrayList<UnidadOperada>();
			
			STOCH_PRE_ANTERIOR =-1000;
			STOCH_ANTERIOR =-1000;
			TOCAMOS_BSUPERIOR = false;
						
			if(ULTIMO_TOP_PRICE > 0) ULTIMO_TOP_PRICE = ACTUAL_TOP_PRICE;
			ACTUAL_TOP_PRICE = 0;

			if(!esFichaCargada){
				UnidadOperada vUnidadOperada = new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j));
				listaUnidadesOperadas.add(vUnidadOperada);
			}
			
			listaFichasPiñon = new ArrayList<FichaPiñon>();
			FichaPiñon vFichaPiñon = new FichaPiñon(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),1);
			listaFichasPiñon.add(vFichaPiñon);
			precioDescarga = 0;
			descargaPrevia = false;

			//Cargando Elliott
			if(MetodosDeLogica.aplica_Elliott(nombrePapel)){
				vElliottData = null;
				vElliottData = new ElliottData();
				buscando_E0_E1_E2(listaCotizaciones,j);
			}
			
			maxRSI_compra = null;
			minRSI_compra = null;
			DESCARGA_VENCIMIENTO = false;
			DESCARGA_VENCIMIENTO_PRECIO = 0;
			balanceAcumulado = 0;
			ultimoMejorOBV_ASC = 0;
			YA_CARGAMOS_BY_ELLIOTT = false;
			CONT_ELEF_MINI_ROJO_ASC = 0;
			CARGA_BLOQUE_APAGADA = false;
			balanceAnterior = 0;
			diasCargando = 0;
			diasTranscurridos = 0;
			DESCARGA_GRUESA_ASC = false;
			semillaSTOCH = 0;
			fechaDesdeBuscarSemillaSTOCH = MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha());
			pasosGiroStoch = 0;
			contDivergenciaBajista = 0;
			
			if(esVisible)
				if(MetodosDeLogica.aplica_TrazeoFull())
					System.out.println(
						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						"- ASC->>" +
						"MMT:" + df.format(listaCalculoMetrica_MMT_15.get(j).getIndicador_valor()) +
						"- MMT_03:" + df.format(listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor()) +
					    "- MMT_05:" + df.format(listaCalculoMetrica_MMT_15.get(j-5).getIndicador_valor()) +
					    "- OBV:" + df.format(listaCalculoMetrica_OBV.get(j).getIndicador_valor()) +
					    "- VOL:" + df_enteros.format(listaCotizaciones.get(j).getVolumen())
				);
			
			return true;
		}		
	return false;
	}
	
	/**
	  * Usado para consulta puntual
	  * @param j
	  * @param listaCotizaciones
	  * @param listaCalculoMetrica_STOCH
	  * @return
	  */
	private boolean compramos_SASC_simula(
			int j,  
			List<Cotizacion> listaCotizaciones, 
			List<CalculoMetricaSTOCH> listaCalculoMetrica_STOCH,
			List<Ficha> listaFichas_SM,
			List <CalculoMetrica> listaCalculoMetrica_MMT_20,
			List <CalculoMetrica> listaCalculoMetrica_ATR,
			boolean esVisible)
	{
		
		boolean compramos_SM =false;
						
		if(MetodosDeLogica.validarGiros_papel(nombrePapel) && listaFichas_SM != null && listaFichas_SM.size()>0){

			boolean fichaValida = listaCotizaciones.get(j).getPrecio_cierre() >= listaFichas_SM.get(0).getPrecio_cierre_operacion();
			if(fichaValida){
				
				//Validacion MMT(20) 
				boolean REGLA_MMT_0_1 = listaCalculoMetrica_MMT_20.get(j).getIndicador_valor() > listaCalculoMetrica_MMT_20.get(j-1).getIndicador_valor();
				boolean REGLA_MMT_0_2 = listaCalculoMetrica_MMT_20.get(j).getIndicador_valor() > listaCalculoMetrica_MMT_20.get(j-2).getIndicador_valor();
				
				if(REGLA_MMT_0_1 && REGLA_MMT_0_2){
					compramos_SM = true;	
				}
			}
		}
		
		if(!compramos_SM){
			final double STOCH_MAIN = listaCalculoMetrica_STOCH.get(j).getValor_main();
			final double STOCH_SIGNAL = listaCalculoMetrica_STOCH.get(j).getValor_signal();
			final double DIFERENCIA_SOPORTADA_STOCH = STOCH_MAIN * 0.03;
			boolean REGLA_ABRE_ELEFANTE_VERDE = STOCH_MAIN > STOCH_SIGNAL && STOCH_MAIN > valor_fuerte_stoch_ASC && (STOCH_MAIN- STOCH_SIGNAL) > DIFERENCIA_SOPORTADA_STOCH ;
			
			if(REGLA_ABRE_ELEFANTE_VERDE){
				compramos_SM = true;
			}
		}
		
		if(!compramos_SM){
			 boolean REGLA_ATR_ASC_01 = listaCalculoMetrica_ATR.get(j).getIndicador_valor() > listaCalculoMetrica_ATR.get(j-1).getIndicador_valor();
		     boolean REGLA_ATR_ASC_02 = listaCalculoMetrica_ATR.get(j).getIndicador_valor() > listaCalculoMetrica_ATR.get(j-2).getIndicador_valor();

		     if(REGLA_ATR_ASC_01 && REGLA_ATR_ASC_02){
		    	 compramos_SM = true;
		     }
		}
		
		if(compramos_SM){
			return true;
		}
		
	return false;
	}

	/**
	 * @param j
	 * @param listaCotizaciones
	 * @param listaCalculoMetrica_OBV
	 * @param listaCalculoMetrica_STOCH
	 * @param listaCalculoMetrica_BAND
	 * @param listaCalculoMetrica_ATR
	 * @param listaCalculoMetrica_MMT_15
	 * @param listaCalculoMetrica_MMT_20
	 * @param vMetricasPapelSector
	 * @return
	 */
	private boolean cerramosCompra_SASC( 
			int j, 
			List<Cotizacion> listaCotizaciones, 
			List<CalculoMetrica> listaCalculoMetrica_OBV,
			List<CalculoMetricaSTOCH> listaCalculoMetrica_STOCH,
			List<CalculoMetricaBand> listaCalculoMetrica_BAND,
			List<CalculoMetrica> listaCalculoMetrica_ATR,
			List<CalculoMetrica> listaCalculoMetrica_MMT_15,
			List <CalculoMetrica> listaCalculoMetrica_MMT_20,
			MetricasPapel vMetricasPapelSector
			)  
	{
		    //fechaStringCorteDebug = "2016-09-01";
			if(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()).equals(fechaStringCorteDebug)){
				System.out.println("");
	    	}
		
			diasTranscurridos++;
			List<Cotizacion> listaCotizacionesPapelSector = null;
			List<CalculoMetrica> listaPapelSectorCalculoMetrica_MMT_15 = null;
			int indiceListaPapelSector = 0;
			if(vMetricasPapelSector != null){
				listaCotizacionesPapelSector = vMetricasPapelSector.getListaCotizaciones();
				listaPapelSectorCalculoMetrica_MMT_15 = vMetricasPapelSector.getListaCalculoMetrica_MMT_15();
				indiceListaPapelSector = calculaUbicacion(listaCotizacionesPapelSector,listaCotizaciones.get(j).getFecha());
			}		
			
			//Descarga por DESCARGA_ATR_PMAX
			double ATR_pMax0 =listaCalculoMetrica_ATR.get(j).getIndicador_valor()*listaCotizaciones.get(j).getPrecio_maximo();
			double ATR_pMax1 =listaCalculoMetrica_ATR.get(j-1).getIndicador_valor()*listaCotizaciones.get(j-1).getPrecio_maximo();
			double ATR_pMax2 =listaCalculoMetrica_ATR.get(j-2).getIndicador_valor()*listaCotizaciones.get(j-2).getPrecio_maximo();
			double ATR_pMax3 =listaCalculoMetrica_ATR.get(j-3).getIndicador_valor()*listaCotizaciones.get(j-3).getPrecio_maximo();
			
			int count_ATR_pMax = 0;
			if(ATR_pMax0 < ATR_pMax1) count_ATR_pMax++;
			if(ATR_pMax0 < ATR_pMax2) count_ATR_pMax++;
			
			boolean ATR_PMAX_BAJO = false;
			String mensaje_ATR_pMax = "ATR_pMax(A)";
			if(count_ATR_pMax == 2){
				mensaje_ATR_pMax = "ATR_pMax(B)";
				ATR_PMAX_BAJO = true;
			}

			if(MetodosDeLogica.aplica_TrazeoFull())
				System.out.println(
					MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) + 
					"- ASC->>" + mensaje_ATR_pMax +
                    "- ATR_pMax0:" + df.format(ATR_pMax0) + 
                    "- ATR_pMax1:" + df.format(ATR_pMax1) + 
                    "- ATR_pMax2:" + df.format(ATR_pMax2)  
			);
			
			double promedioPreciosMax = 
					(listaCotizaciones.get(j-1).getPrecio_maximo() +
					 listaCotizaciones.get(j-2).getPrecio_maximo() +
					 listaCotizaciones.get(j-3).getPrecio_maximo() +
					 listaCotizaciones.get(j-4).getPrecio_maximo() +
					 listaCotizaciones.get(j-5).getPrecio_maximo())/5;
			
			boolean REGLA_ES_GATO_MUERTO = promedioPreciosMax > listaCotizaciones.get(j).getPrecio_maximo(); 
			
			if(REGLA_ES_GATO_MUERTO)
				if(MetodosDeLogica.aplica_TrazeoFull())
					System.out.println(
						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						"- ASC->>GATO_MUERTO:" + REGLA_ES_GATO_MUERTO +
						"- promedioPreciosMax:" + df.format(promedioPreciosMax) +
						"- precioMax:" + df.format(listaCotizaciones.get(j).getPrecio_maximo()));

			int REGLA_ES_GATO_MUERTO_PAPEL_SECTOR = 0;
			double promedioPreciosMaxsPapelSector = 0;
			if(indiceListaPapelSector != 0){
				promedioPreciosMaxsPapelSector = 
						(listaCotizacionesPapelSector.get(indiceListaPapelSector-1).getPrecio_maximo() +
						 listaCotizacionesPapelSector.get(indiceListaPapelSector-2).getPrecio_maximo() +
						 listaCotizacionesPapelSector.get(indiceListaPapelSector-3).getPrecio_maximo())/3;

				if(promedioPreciosMaxsPapelSector > listaCotizacionesPapelSector.get(indiceListaPapelSector).getPrecio_maximo())
					REGLA_ES_GATO_MUERTO_PAPEL_SECTOR = 1;
				else
					REGLA_ES_GATO_MUERTO_PAPEL_SECTOR = -1;
							
			}
						
			//REGLAS usadas en logica
	    	boolean REGLA_MMT_POSITIVO = listaCalculoMetrica_MMT_15.get(j).getIndicador_valor()>=0;
	    	boolean REGLA_MMT_ASC_3DIAS = listaCalculoMetrica_MMT_15.get(j).getIndicador_valor() >= listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor();
			boolean REGLA_MMT_ASC_5DIAS = listaCalculoMetrica_MMT_15.get(j).getIndicador_valor() >= listaCalculoMetrica_MMT_15.get(j-5).getIndicador_valor();
	    	
			boolean REGLA_MMT_DESC_3DIAS = listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor() > listaCalculoMetrica_MMT_15.get(j).getIndicador_valor();
			boolean REGLA_MMT_DESC_5DIAS = listaCalculoMetrica_MMT_15.get(j-5).getIndicador_valor() > listaCalculoMetrica_MMT_15.get(j).getIndicador_valor();
			
			boolean esVencimiento = false;
			if(MetodosDeLogica.esFechaVencimiento(listaCotizaciones.get(j))){
				esVencimiento = true;
				
				if(MetodosDeLogica.aplica_TrazeoFull())
					System.out.println(
						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						"- ASC->>MES VENCIMIENTO- SOLO CARAMELOS!!");
			}
		
			//Promedio volumenes
			//Promedio corto volumenes
			boolean esPromedioVolCreciente = false;
			boolean esPromedioVolDecreciente = false;
			int nro_dias_vol_promedio = 3;
			double volPromedio = 0;
			int contador = 0;
			for(int k=j; nro_dias_vol_promedio>0; k--){
				
				if(listaCotizaciones.get(k).getPrecio_maximo() > listaCotizaciones.get(k-1).getPrecio_maximo()){
					volPromedio += listaCotizaciones.get(k).getVolumen();
					contador++;
				}
				nro_dias_vol_promedio--;
			}
			
			if(contador>0)
				volPromedio = volPromedio/contador;
			
			//Promedio largo volumenes
			if(volPromedio > 0){
				nro_dias_vol_promedio = 10;
				int nro_dias_vol_promedio_PRE = 10;
				contador = 0;
				double volPromedio_PRE = 0;
				for(int k=(j-nro_dias_vol_promedio_PRE); nro_dias_vol_promedio>0; k--){
					if(listaCotizaciones.get(k).getPrecio_maximo() > listaCotizaciones.get(k-1).getPrecio_maximo()){
						volPromedio_PRE += listaCotizaciones.get(k).getVolumen();
						contador++;
					}
					nro_dias_vol_promedio--;
				}
				if(contador>0)
					volPromedio_PRE = volPromedio_PRE/contador;
								
				//delta xx%
				if(volPromedio_PRE > 0 && volPromedio > (volPromedio_PRE*0.85)){
					esPromedioVolCreciente = true;
					
					if(MetodosDeLogica.aplica_TrazeoFull())
						System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- ASC->>VOLUMENES PROMEDIOS CRECIENTES(85-100%)" +
							"- VolPromedio:" + df_enteros.format(volPromedio) +
							"- VolPromedioPrev:" + df_enteros.format(volPromedio_PRE));
				} else {
					
					if(volPromedio_PRE > 0){
					
						if(volPromedio < (volPromedio_PRE/3)){
							esPromedioVolDecreciente = true;
							
							if(listaCotizaciones.get(j).getVolumen() < volPromedio)
							{
								if(MetodosDeLogica.aplica_TrazeoFull() && MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) <= 50)
									System.out.println(
										MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
										"- ASC->>VOLUMENES PROMEDIOS DECRECIENTES 0-30% - DANGER!!" +
										"- Vol:" + df_enteros.format(listaCotizaciones.get(j).getVolumen()) +
										"- VolPromedio:" + df_enteros.format(volPromedio) +
										"- VolPromedioPrev:" + df_enteros.format(volPromedio_PRE));
							}else{
								if(MetodosDeLogica.aplica_TrazeoFull() && MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) <= 50)
									System.out.println(
										MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
										"- ASC->>VOLUMENES PROMEDIOS DECRECIENTES 0-30% - WARNING!!" +
										"- VolPromedio:" + df_enteros.format(volPromedio) +
										"- VolPromedioPrev:" + df_enteros.format(volPromedio_PRE));
							}
					
						} else { 
							if(MetodosDeLogica.aplica_TrazeoFull())
								System.out.println(
									MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
									"- ASC->>VOLUMENES PROMEDIOS LATERAL" +
									"- VolPromedio:" + df_enteros.format(volPromedio) +
									"- VolPromedioPrev:" + df_enteros.format(volPromedio_PRE));
						}
				    }
				}
			}
			
			//Trazeo Velones
			boolean esVelon = false;
			double deltaVelon = 100;
			int nro_dias_velon_promedio = 10;
			double velonPromedio = 0;
			double dif = 0;
			
			for(int k=j; nro_dias_velon_promedio>0; k--){
				dif = listaCotizaciones.get(k).getPrecio_cierre() - listaCotizaciones.get(k-1).getPrecio_cierre();
				if(dif<0) 
					dif = dif*-1;
				
				velonPromedio += dif;					
				nro_dias_velon_promedio--;
			}
			nro_dias_velon_promedio = 10;
			velonPromedio = velonPromedio /nro_dias_velon_promedio;
			velonPromedio = velonPromedio + (velonPromedio* (deltaVelon/100));
						
			double diferencia_0 = listaCotizaciones.get(j).getPrecio_cierre() - listaCotizaciones.get(j-1).getPrecio_cierre();
			double diferencia_1 = listaCotizaciones.get(j-1).getPrecio_cierre() - listaCotizaciones.get(j-2).getPrecio_cierre();
			if(diferencia_0 > velonPromedio || diferencia_1 > velonPromedio && diferencia_0 > 0){
				esVelon = true;
				
				if(MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) <= 50)
				  System.out.println(
						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						"- ASC->>VELON- OPORTUNIDAD DESCARGA!!" +
						"- Diferencia_0:" + df.format(diferencia_0) +
						"- Diferencia_1:" + df.format(diferencia_1) +
						"- VelonPromedio(+"+ deltaVelon +"%):" + df.format(velonPromedio) 
				);
		    }

			//Calculos stochs
			final double STOCH_MAIN = listaCalculoMetrica_STOCH.get(j).getValor_main();
			final double STOCH_SIGNAL = listaCalculoMetrica_STOCH.get(j).getValor_signal();
			final double STOCH = STOCH_MAIN- STOCH_SIGNAL;
			
			final double STOCH_MAIN_01 = listaCalculoMetrica_STOCH.get(j-1).getValor_main();
			final double STOCH_SIGNAL_01 = listaCalculoMetrica_STOCH.get(j-1).getValor_signal();
			final double STOCH_01 = STOCH_MAIN_01- STOCH_SIGNAL_01;
	    	boolean STOCH_REDUCIENDOSE_0 = STOCH < STOCH_01;
	    	
	    	final double STOCH_MAIN_02 = listaCalculoMetrica_STOCH.get(j-2).getValor_main();
			final double STOCH_SIGNAL_02 = listaCalculoMetrica_STOCH.get(j-2).getValor_signal();
			final double STOCH_02 = STOCH_MAIN_02- STOCH_SIGNAL_02;
	    	boolean STOCH_REDUCIENDOSE_2 = STOCH < STOCH_02;

	    	final double STOCH_MAIN_03 = listaCalculoMetrica_STOCH.get(j-3).getValor_main();
			final double STOCH_SIGNAL_03 = listaCalculoMetrica_STOCH.get(j-3).getValor_signal();
			final double STOCH_03 = STOCH_MAIN_03- STOCH_SIGNAL_03;
	    	
			final double STOCH_PROM_1_3 = (STOCH_01 + STOCH_02 + STOCH_03)/3; 
						
			//Trazeo Giro Stoch
			Date fechaDesde = MetodosFechas.crearFechaString(fechaDesdeBuscarSemillaSTOCH.split(GUION_MEDIO)).getTime();
			boolean esPeriodoValido = fechaDesde.before(listaCotizaciones.get(j-2).getFecha());

	    	if(esPeriodoValido){
	    		//Buscando fondo
	    		if(STOCH < semillaSTOCH || pasosGiroStoch == 0){
	    			
	    			if(STOCH < semillaSTOCH)
	    				semillaSTOCH = STOCH;
	    			
	    			pasosGiroStoch = 1;//#1- nuevo minimo
	    			if(MetodosDeLogica.aplica_TrazeoFull())
	    				System.out.println(	
			    		  MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			    		  "- ASC->>***#1-NUEVO MINIMO- SemillaStoch:" + df.format(semillaSTOCH));
	    		}
	    		
		    	if(pasosGiroStoch == 1 && STOCH_01 < STOCH_02 && !STOCH_REDUCIENDOSE_0){
		    		pasosGiroStoch = 2; //#2- posible nuevo minimo - falta confirmar
		    		if(MetodosDeLogica.aplica_TrazeoFull())
		    			System.out.println(
		    				MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    				"- ASC->>***#2-POSIBLE FONDO- Falta confirmacion!! - SemillaStoch:" + df.format(semillaSTOCH));
		    	} else
		    		if(pasosGiroStoch == 2 && !STOCH_REDUCIENDOSE_0 && !STOCH_REDUCIENDOSE_2){
		    		pasosGiroStoch = 3; //#3- Fondo confirmado
		    		if(MetodosDeLogica.aplica_TrazeoFull())
		    			System.out.println(
		    				MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    				"- ASC->>***#3-FONDO CONFIRMADO- SemillaStoch:" + df.format(semillaSTOCH));
		    	} else
			    	if(pasosGiroStoch == 3 && STOCH >= semillaSTOCH){
			    		pasosGiroStoch = 4; //#4- Cargando un.
			    		if(MetodosDeLogica.aplica_TrazeoFull())
			    			System.out.println(
			    				MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			    				"- ASC->>***#4-CARGAR UNIDAD- SemillaStoch:" + df.format(semillaSTOCH));
			    	}else{
			    		if(pasosGiroStoch == 4 && STOCH < semillaSTOCH){
			    			semillaSTOCH = STOCH;
			    			pasosGiroStoch = 1;//#1- nuevo minimo
			    			if(MetodosDeLogica.aplica_TrazeoFull())
			    				System.out.println(
			    					MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			    					"- ASC->>***#0-ANULADO- NUEVO MINIMO- SemillaStoch:" + df.format(semillaSTOCH));
			    			
		    		}
		    	}
	    	}else{
	    		pasosGiroStoch = 0;
	    		semillaSTOCH = 0;
	    		if(MetodosDeLogica.aplica_TrazeoFull())
	    			System.out.println(
	    				MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	    				"- ASC->>***#PERIODO INVALIDO");
	    	}

	    	boolean esCRECIENTE_STOCH = false;
	    	if(pasosGiroStoch>=3){
		   		if(STOCH >= STOCH_PROM_1_3){
		   			esCRECIENTE_STOCH = true;
		   		}
	    	}
	    	
	    	if(STOCH >= STOCH_PROM_1_3){
	    		if(MetodosDeLogica.aplica_TrazeoFull())
	    			System.out.println(
	    				MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	    				"- ASC->>STOCH CRECIENTE- SemillaStoch:" + df.format(semillaSTOCH) +
	    				"- Stoch:" + df.format(STOCH) +
	    				"- StochProm:" + df.format(STOCH_PROM_1_3));
    		} else {
    			if(MetodosDeLogica.aplica_TrazeoFull())
    				System.out.println(
    					MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
    					"- ASC->>STOCH DECRECIENTE- SemillaStoch:" + df.format(semillaSTOCH) +
    					"- Stoch:" + df.format(STOCH) +
    					"- StochProm:" + df.format(STOCH_PROM_1_3));
    		}	    	
	    	
	    	boolean esCRECIENTE_MMT = false;
	    	double MMT_PROM_1_3 = 
	    			(listaCalculoMetrica_MMT_15.get(j-1).getIndicador_valor() +
	    			 listaCalculoMetrica_MMT_15.get(j-2).getIndicador_valor() +
	    			 listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor())/3;
	    	
	    	if(listaCalculoMetrica_MMT_15.get(j).getIndicador_valor() >= MMT_PROM_1_3){
	    		esCRECIENTE_MMT = true;
	    		if(MetodosDeLogica.aplica_TrazeoFull())
	    			System.out.println(
	    				MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	    				"- ASC->>MMT CRECIENTE- MMT:" + df.format(listaCalculoMetrica_MMT_15.get(j).getIndicador_valor()) +
	    				"- MMT_Prom:" + df.format(MMT_PROM_1_3));
	    	}else{
	    		if(MetodosDeLogica.aplica_TrazeoFull())
	    			System.out.println(
	    				MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	    				"- ASC->>MMT DECRECIENTE- MMT:" + df.format(listaCalculoMetrica_MMT_15.get(j).getIndicador_valor()) +
	    				"- MMT_Prom:" + df.format(MMT_PROM_1_3));
	    	}
		    	
			//Traza de Avance Precio
			String porcentaje_real = null;
			double precioActual = listaCotizaciones.get(j).getPrecio_cierre();
			double porcentaje = (precioActual-precio_operacion_original_ASC)*100/precio_operacion_original_ASC; 
			porcentaje_real = "(" + df.format(porcentaje) + "%)";
			
			double precioConDelta = precioActual + (precioActual*delta_porcentaje_precio/100);
			
			int m=0;
			if(j>1) m=j-1;
			double balance_compra_actual = calcularBalance(false,true,null,j,listaCotizaciones);
			boolean subioPrecio = precioConDelta >= listaCotizaciones.get(m).getPrecio_cierre();
			
			double balanceAnteriorCalculo = balanceAnterior;
			balanceAnterior = balance_compra_actual;

			//Validando Elliott timeout
			if(MetodosDeLogica.estaElliottTimeout(listaCotizaciones.get(j), vElliottData))
				vElliottData = null;
			
			
			//Validando Elliott regenacion
			int cantUnidadesRegeneracionElliot = 2;
			if(balance_compra_actual >1 || MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)>=cantUnidadesRegeneracionElliot)
				regenerarElliott(listaCotizaciones, j);
			
			//Cargando Elliott timeout
			hayNuevoMaximo = false;
			cargar_valores_Elliott(listaCotizaciones,j,listaCalculoMetrica_ATR,listaCalculoMetrica_BAND);

			
			//Actualizando precios Target
			if (ACTUAL_TOP_PRICE < precioConDelta)
				ACTUAL_TOP_PRICE = precioConDelta;
				
			if(ULTIMO_TOP_PRICE < ACTUAL_TOP_PRICE)
				ULTIMO_TOP_PRICE = ACTUAL_TOP_PRICE;

			//Trazas RATIO CRECIMIENTO
			double ratioCrecimiento = 0;
			if(MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)>0){
				ratioCrecimiento = balance_compra_actual/MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas);
				String mensaje = "- Balance:" + df.format(balance_compra_actual) +
						         "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas);	

				
				if(ratioCrecimiento <1 && ratioCrecimiento >0.5){
					if(MetodosDeLogica.aplica_TrazeoFull())
					  System.out.println(
						 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						 "- ASC->>RATIO CRECIMIENTO DEBIL:" + df.format(ratioCrecimiento) + mensaje );

				}else if(ratioCrecimiento <= 0.5){
					if(MetodosDeLogica.aplica_TrazeoFull())
					   System.out.println(
						  MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						  "- ASC->>RATIO CRECIMIENTO ROJO:" + df.format(ratioCrecimiento) + mensaje );	
					
				}else if(ratioCrecimiento >=2){
					if(MetodosDeLogica.aplica_TrazeoFull())
					  System.out.println(
						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						"- ASC->>RATIO CRECIMIENTO FUERTE:" + df.format(ratioCrecimiento) + mensaje );
				
				}else{
					if(MetodosDeLogica.aplica_TrazeoFull())
						System.out.println(
						   MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						   "- ASC->>RATIO CRECIMIENTO VERDE:" + df.format(ratioCrecimiento) + mensaje );
				}
			}
			
			if(ratioCrecimiento >0 && ratioCrecimiento >=1 &&
			   !esCRECIENTE_MMT && (!esCRECIENTE_STOCH && pasosGiroStoch>=3))
			{
				contDivergenciaBajista++;

				if(contDivergenciaBajista == 1){
					if(MetodosDeLogica.aplica_TrazeoFull())
						System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- ASC->>DIVERGENCIA BAJISTA(Stoch y MMT Desc- RatioCrecim Verde)- Se validara descargar!!");
				}else{
					if(MetodosDeLogica.aplica_TrazeoFull())
						System.out.println(
						  MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						  "- ASC->>CONFIRMADA DIVERGENCIA BAJISTA(Stoch y MMT Desc- RatioCrecim Verde)");
				}

			}else{
				contDivergenciaBajista = 0;
			}
			
			//RatioDiasCarga
			double ratioDiasCarga = 0;
			if(diasCargando >0){
				ratioDiasCarga = new Double(diasTranscurridos).doubleValue()/diasCargando;
				if(ratioDiasCarga >= 0.4){
					if(MetodosDeLogica.aplica_TrazeoFull())
						System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- ASC->>RATIO DIAS CARGA VERDE:" + df.format(ratioDiasCarga) +
							"- diasTranscurridos:" + diasTranscurridos +
							"- diasCargando:" + diasCargando);
				}else{
					if(MetodosDeLogica.aplica_TrazeoFull())
						System.out.println(
						    MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						    "- ASC->>RATIO DIAS CARGA ROJO:" + df.format(ratioDiasCarga)+
							"- diasTranscurridos:" + diasTranscurridos +
							"- diasCargando:" + diasCargando);
				}
			}
			
			//Calculos TrabajoMecanico  JFM
			double MMT_PROM_0_4= 
	    			(listaCalculoMetrica_MMT_15.get(j).getIndicador_valor() +
	    			 listaCalculoMetrica_MMT_15.get(j-1).getIndicador_valor() +
	    			 listaCalculoMetrica_MMT_15.get(j-2).getIndicador_valor() +
	    			 listaCalculoMetrica_MMT_15.get(j-2).getIndicador_valor()
	    			)/4;
			
			
			double STOCH_PROM_0_4 = (STOCH + STOCH_01 + STOCH_02 + STOCH_03)/4;
						
			double EC = MMT_PROM_0_4;
			double EP = STOCH_PROM_0_4;
			double W1 = EC + EP;
			final String W1_traza = "W1(EC+EP):" + df.format(W1) + "- (EC:" + df.format(EC)+ "- EP:" + df.format(EP)+ ")";

			final double FUERZA_0_4 = (ATR_pMax0 + ATR_pMax1 + ATR_pMax2 + ATR_pMax3)/4;
			final double DIST_0_4= 
				(
				(listaCalculoMetrica_MMT_15.get(j).getIndicador_valor()-listaCalculoMetrica_MMT_15.get(j-1).getIndicador_valor())+
				(listaCalculoMetrica_MMT_15.get(j-1).getIndicador_valor()-listaCalculoMetrica_MMT_15.get(j-2).getIndicador_valor())+
				(listaCalculoMetrica_MMT_15.get(j-2).getIndicador_valor()-listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor())+
				(listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor()-listaCalculoMetrica_MMT_15.get(j-4).getIndicador_valor())
				)/4;
					
			final double W2 = FUERZA_0_4 * DIST_0_4;			
			final String W2_traza = "W2(F*d):" + df.format(W2) + "-(F:" + df.format(FUERZA_0_4)+ "- d:" + df.format(DIST_0_4)+ ")";
	
			boolean es_W_cargaGrosa = false;
			boolean es_NO_RescateFichas = false;
			double W1_70_porciento = W1 - (W1*0.3);
			if(W1_70_porciento>=W2){
				if(MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)>50){
					
					if(!MetodosDeLogica.aplica_RutaATR(nombrePapel))
						es_W_cargaGrosa = true;
					
					if(W2>0)
						es_NO_RescateFichas=true;
					
					if(MetodosDeLogica.aplica_TrazeoFull())
						System.out.println(
						    MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						    "- ASC->>W1_70%>=W2 (W Fuerte)" +
						    "- " + W1_traza + "- " + W2_traza);
				}
				
			}else{
				double W2_70_porciento = W2 - (W2*0.3);
				if(W1>=W2_70_porciento){
					System.out.println(
						    MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						    "- ASC->>W1>W2_70% (W Normal)" +
						    "- " + W1_traza + "- " + W2_traza);
				}else{
					if(MetodosDeLogica.aplica_TrazeoFull())
						System.out.println(
						    MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						    "- ASC->>W1<W2_70% (W Debil)" +
						    "- " + W1_traza + "- " + W2_traza);
				}
			}			
			
			//Cierre de operacion forzado			
			if(FECHA_CIERRE_FORZADO != null && cierreForzado(listaCotizaciones, j, balance_compra_actual)){ 
				listaFichas = new ArrayList<Ficha>();
				return true;
			}
			
			//Cierre por Fecha Ejercicio			
			if(MetodosDeLogica.validar_fecha_ejercicio_papel(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha())) &&
			   cierreForzado(listaCotizaciones, j, balance_compra_actual)){ 
					listaFichas = new ArrayList<Ficha>();
					return true;
			}
			
			final double ATR_MAIN = listaCalculoMetrica_ATR.get(j).getIndicador_valor();
			boolean ATR_BAJO = false;
			if(MetodosDeLogica.validarGiros_papel(nombrePapel) && ATR_MAIN < ATR_SEMILLA) ATR_BAJO = true;
			
			if(!REGLA_COMENZAR_CIERRE_1 && STOCH > valor_fuerte_stoch_ASC){
		    	REGLA_COMENZAR_CIERRE_1 = true;	
		    			    	
		    	System.out.println(
    				 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	        		 "- ASC->>FUERTE STOCH!!!- Stoch:" + df.format(STOCH));
		    }
			
			double bandaSuperior = listaCalculoMetrica_BAND.get(j).getValor_banda_superior();
			double bandaMedia = listaCalculoMetrica_BAND.get(j).getValor_banda_media();
			
			boolean VELA_MAX_ABANDONA_BANDA_SUPERIOR = listaCotizaciones.get(j).getPrecio_maximo() < bandaSuperior;
			boolean PRECIO_ABANDONA_BANDA_SUPERIOR = precioConDelta < bandaSuperior;
			boolean PRECIO_ABANDONA_BANDA_MEDIA = precioConDelta < bandaMedia;
			
			boolean PRECIO_ESTA_BANDA_SUPERIOR = precioConDelta > bandaSuperior;
			if(PRECIO_ESTA_BANDA_SUPERIOR){
				TOCAMOS_BSUPERIOR = true;
			}
			
			if(!ATR_BAJO && STOCH_ANTERIOR < STOCH_PRE_ANTERIOR){
				
				if(MetodosDeLogica.validarGiros_papel(nombrePapel) && TOCAMOS_BSUPERIOR && VELA_MAX_ABANDONA_BANDA_SUPERIOR){
					REGLA_DESCARGA_BSUPERIOR = true;
				}
								
				if(!REGLA_DESCARGA_BSUPERIOR && PRECIO_ABANDONA_BANDA_SUPERIOR && !PRECIO_ABANDONA_BANDA_MEDIA){
					if(MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)>0){
						
						if(MetodosDeLogica.aplica_TrazeoFull())
							System.out.println(
									MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
									"- ASC->>WARNING- STOCH MENOR a:" + df.format(STOCH_ANTERIOR) 
							);
					}
				}
			}
			STOCH_PRE_ANTERIOR = STOCH_ANTERIOR;
			STOCH_ANTERIOR = STOCH;

//Bloqueos Descarga
			long sizeAntesDescarga = MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas);
			boolean descarga_bloqueada = false;
			
			//Bloqueo por Sector
			if(REGLA_ES_GATO_MUERTO_PAPEL_SECTOR == -1){
				descarga_bloqueada = true;

				if(!subioPrecio)
					System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- ASC->>DESCARGA BLOQUEADA- PAPEL_SECTOR(" + vMetricasPapelSector.getNombrePapel() + ")" +
							"- promedioPreciosMaxsPapelSector:" + df.format(promedioPreciosMaxsPapelSector) +
							"- precioMax:" + df.format(listaCotizacionesPapelSector.get(indiceListaPapelSector).getPrecio_maximo()));
				
			}
			
			//Bloqueo Descarga - ATR ASC
			boolean esATR_ASC = false;
			boolean esOBV_ASC = false;
			boolean esVOL_ASC = false;
			
			if(!descarga_bloqueada && MetodosDeLogica.aplica_RutaATR(nombrePapel)){
			     boolean REGLA_ATR_ASC_01 = listaCalculoMetrica_ATR.get(j).getIndicador_valor() > listaCalculoMetrica_ATR.get(j-1).getIndicador_valor();
			     boolean REGLA_ATR_ASC_02 = listaCalculoMetrica_ATR.get(j).getIndicador_valor() > listaCalculoMetrica_ATR.get(j-2).getIndicador_valor();
			     boolean REGLA_ATR_ASC_03 = listaCalculoMetrica_ATR.get(j).getIndicador_valor() > listaCalculoMetrica_ATR.get(j-3).getIndicador_valor();
			     
			     int count = 0;
			     if(REGLA_ATR_ASC_01) count++;
			     if(REGLA_ATR_ASC_02) count++;
			     if(REGLA_ATR_ASC_03) count++;
			     						     
			     if(!descarga_bloqueada && count>1){
			    	 descarga_bloqueada = esATR_ASC = true;
		    	     
			    	 if(!subioPrecio)
			    	   if(MetodosDeLogica.aplica_TrazeoFull())
				    	 System.out.println(
							 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							 "- ASC->>DESCARGA BLOQUEADA- ATR_ASC:" + 
							 df.format(listaCalculoMetrica_ATR.get(j).getIndicador_valor())+
							 "- ATR_1:" + df.format(listaCalculoMetrica_ATR.get(j-1).getIndicador_valor())+
							 "- ATR_2:" + df.format(listaCalculoMetrica_ATR.get(j-2).getIndicador_valor())+
							 "- ATR_3:" + df.format(listaCalculoMetrica_ATR.get(j-3).getIndicador_valor()));
			     }
			     
			     //Bloqueo Descarga - OBV ASC
			     boolean REGLA_OBV_ASC_01 = listaCalculoMetrica_OBV.get(j).getIndicador_valor() > listaCalculoMetrica_OBV.get(j-1).getIndicador_valor();
			     boolean REGLA_OBV_ASC_02 = listaCalculoMetrica_OBV.get(j).getIndicador_valor() > listaCalculoMetrica_OBV.get(j-2).getIndicador_valor();
			     
			     if(!descarga_bloqueada && REGLA_OBV_ASC_01 && REGLA_OBV_ASC_02){
			    	 descarga_bloqueada = esOBV_ASC = true;
			    	 
			    	 if(!subioPrecio)
			    	  if(MetodosDeLogica.aplica_TrazeoFull())
						 System.out.println(
							 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							 "- ASC->>DESCARGA BLOQUEADA- OBV_ASC:" + 
							 df_enteros.format(listaCalculoMetrica_OBV.get(j).getIndicador_valor())+
							 "- OBV_1:" + df_enteros.format(listaCalculoMetrica_OBV.get(j-1).getIndicador_valor())+
							 "- OBV_2:" + df_enteros.format(listaCalculoMetrica_OBV.get(j-2).getIndicador_valor()));
			     }
			     
			     //Bloqueo Descarga - VOL ASC
			     boolean REGLA_VOL_ASC_01 = listaCotizaciones.get(j).getVolumen() > listaCotizaciones.get(j-1).getVolumen();
			     		     
			     if(!descarga_bloqueada && REGLA_VOL_ASC_01 && subioPrecio){
			    	 descarga_bloqueada = esVOL_ASC = true;
			    	 
			    	 if(!subioPrecio)
			    		 if(MetodosDeLogica.aplica_TrazeoFull())
			    			 System.out.println(
							 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							 "- ASC->>DESCARGA BLOQUEADA- VOL_ASC:" + 
							 df_enteros.format(listaCotizaciones.get(j).getVolumen())+
							 "- VOL_1:" + df_enteros.format(listaCotizaciones.get(j-1).getVolumen()));
			     }
			}	
			
			//DesBloqueo Descarga
			boolean descargaBloquedaGeneral = false;
			if(esCRECIENTE_STOCH){
				descarga_bloqueada = true;
				descargaBloquedaGeneral = true;
				
				if(!subioPrecio)
					if(MetodosDeLogica.aplica_TrazeoFull())
						System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    	  			"- ASC->>DESCARGA BLOQUEADA- GIROS STOCH- SemillaStoch:" + df.format(semillaSTOCH));

			}else 
				if(esCRECIENTE_MMT){
					descarga_bloqueada = true;
					descargaBloquedaGeneral = true;
					
					if(!subioPrecio)
						if(MetodosDeLogica.aplica_TrazeoFull())
							System.out.println(
								MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			    	  			"- ASC->>DESCARGA BLOQUEADA- GIROS STOCH(MMT ASC)" +
			    	  			"- Stoch:" + df.format(STOCH) +
								"- SemillaStoch:" + df.format(semillaSTOCH));
			    }
			
			//Bloqueo Descarga GiroStoch
			if(!esCRECIENTE_STOCH && !esCRECIENTE_MMT &&
			   pasosGiroStoch >= 4 && STOCH > semillaSTOCH &&
			   !REGLA_ES_GATO_MUERTO)
			{
				descarga_bloqueada = true;
				descargaBloquedaGeneral = true;
				
				if(!subioPrecio)
					if(MetodosDeLogica.aplica_TrazeoFull())
						System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    	  			"- ASC->>DESCARGA BLOQUEADA- STOCH MAYOR SEMILLA (No Gato Muerto)" +
							"- Stoch:" + df.format(STOCH) +
							"- SemillaStoch:" + df.format(semillaSTOCH));
		    }			

			boolean esAltaVolatilidad = MetodosDeLogica.aplica_RutaATR(nombrePapel);
			if(!esAltaVolatilidad && pasosGiroStoch >= 4 && STOCH > semillaSTOCH)
			{
				descarga_bloqueada = true;
				descargaBloquedaGeneral = true;
				
				if(!subioPrecio)
					if(MetodosDeLogica.aplica_TrazeoFull())
						System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- ASC->>DESCARGA BLOQUEADA- STOCH MAYOR SEMILLA (No quebro semilla)" +
							"- Stoch:" + df.format(STOCH) +
							"- SemillaStoch:" + df.format(semillaSTOCH));
			}	
			
//Descarga Unidades
			//Descarga Rescate full
			boolean yaDescargamos = false;
			
			boolean REGLA_MMT_DESC_1DIA = listaCalculoMetrica_MMT_15.get(j).getIndicador_valor() < listaCalculoMetrica_MMT_15.get(j-1).getIndicador_valor();
			boolean REGLA_MMT_DESC_2DIA = listaCalculoMetrica_MMT_15.get(j).getIndicador_valor() < listaCalculoMetrica_MMT_15.get(j-2).getIndicador_valor();
			boolean REGLA_MMT_DESC_3DIA = listaCalculoMetrica_MMT_15.get(j).getIndicador_valor() < listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor();
						
			boolean STOCH_DESC = STOCH_REDUCIENDOSE_0 & STOCH_REDUCIENDOSE_2;
			boolean MMT_DESC = REGLA_MMT_DESC_1DIA & REGLA_MMT_DESC_2DIA & REGLA_MMT_DESC_3DIA;

			boolean haySemillasRescatadas = ScannerSemillas.cantSemillasRescatadas(listaUnidadesOperadas)>0;
			boolean haySemillasNoRescatadas = ScannerSemillas.cantSemillasNoRescatadas(listaUnidadesOperadas)>0;

			if(STOCH_DESC && MMT_DESC && haySemillasRescatadas && haySemillasNoRescatadas && ATR_PMAX_BAJO){
			     ScannerSemillas.rescatarSemillas(true,listaCotizaciones,j,listaUnidadesOperadas,es_NO_RescateFichas);
			}
			
			//Descarga por EVENTOS
			String mensajeSemaforoDiaPreEvento = MetodosDeLogica.semaforoDiaPreEvento(listaUnidadesOperadas,listaCotizaciones.get(j).getFecha(), mensaje_ATR_pMax, STOCH, STOCH_ANTERIOR);
			
			if(MetodosDeLogica.validar48hsPreEvento(listaUnidadesOperadas,listaCotizaciones.get(j).getFecha()))
			{
				if(descarga_bloqueada && descargaBloquedaGeneral){
					if(!ATR_PMAX_BAJO && STOCH_REDUCIENDOSE_0 && STOCH_REDUCIENDOSE_0 && STOCH_REDUCIENDOSE_2){
			    	 	 System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- ASC->>DESCARGA_PRE_EVENTO(48hs- ATR_pMax(A)- STOCH_DESC)" +
							"- ATR:" + df.format(ATR_MAIN) +
							"- Stoch:" + df.format(STOCH) +
							"- STOCH_01:" + df.format(STOCH_01)
						 );
			    	 
			    		 PRE_EVENTO = true;
				     	 yaDescargamos = true;
			        	 balance_compra_actual = descargaUnidades(j,listaCotizaciones,balance_compra_actual,true);
			        	 balanceAcumulado += balance_compra_actual;
			        	 precioDescarga = precioConDelta;
			        	 descargaPrevia = true;
			        	 YA_CARGAMOS_BY_ELLIOTT = false;
					    	 
				    	 if(balance_compra_actual > 0){
			    	    	 final Rendimiento vRendimiento = new Rendimiento(listaCotizaciones.get(j).getFecha(), df.format(balance_compra_actual),porcentaje_real,true,sizeAntesDescarga,diasCargando);
			    	    	 listaRendimientos.add(vRendimiento);
			    	     }

				    	 //desacople de resultado de descargas
				    	 if (sizeAntesDescarga >0)
				    		 cerrarOperacion(j, listaCotizaciones, balance_compra_actual,STOCH,true);
			    	}
				}
		     }
			
			 //Rescate Forzado de Semillas por EVENTO DANGER 
			 if(!yaDescargamos){
				 boolean haySemillasNoRescatadas_ = ScannerSemillas.cantSemillasNoRescatadas(listaUnidadesOperadas)>0;
					
				 if(mensajeSemaforoDiaPreEvento.toUpperCase().contains("DANGER") && haySemillasRescatadas && haySemillasNoRescatadas_){
				     ScannerSemillas.rescatarSemillas(true,listaCotizaciones,j,listaUnidadesOperadas,es_NO_RescateFichas);
				 }
			 }
		    
			 String estado_ATR = "(-):";
			 if(ATR_MAIN < ATR_SEMILLA)
				estado_ATR = "(B):";
			 else
				estado_ATR = "(A):";
			
		     //Descarga Unidades - PELIGRO INVISIBLE
			 String mensajeDescargaPeligroInvisible = 
					 MetodosDeLogica.semaforoPeligroInvisible(
							listaCalculoMetrica_MMT_15, 
							listaCotizaciones, 
							j,
							balanceAcumulado,
							resultadoJuego,
							porcentaje,
							estado_ATR,
							nombrePapel
			   );

			 //Validando MMT Sector
			 boolean REGLA_MMT_SECTOR_ASC_3DIAS = false;
			 boolean REGLA_MMT_SECTOR_ASC_5DIAS = false;
			 if(indiceListaPapelSector != 0){
				 REGLA_MMT_SECTOR_ASC_3DIAS = listaPapelSectorCalculoMetrica_MMT_15.get(indiceListaPapelSector).getIndicador_valor()>=listaPapelSectorCalculoMetrica_MMT_15.get(indiceListaPapelSector-3).getIndicador_valor();
				 REGLA_MMT_SECTOR_ASC_5DIAS = listaPapelSectorCalculoMetrica_MMT_15.get(indiceListaPapelSector).getIndicador_valor()>=listaPapelSectorCalculoMetrica_MMT_15.get(indiceListaPapelSector-5).getIndicador_valor();
			 }
					 
			 if(!(descarga_bloqueada || descargaBloquedaGeneral) && mensajeDescargaPeligroInvisible != null && !(REGLA_MMT_SECTOR_ASC_3DIAS && REGLA_MMT_SECTOR_ASC_5DIAS)){
	    	 	 System.out.println(
						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						"- ASC->>DESCARGA_PELIGRO_INVISIBLE_MMT(15)- " +
						mensajeDescargaPeligroInvisible);
	    	 	 
		     	 yaDescargamos = true;
		     	 PELIGRO_INVISIBLE = true;
	        	 balance_compra_actual = descargaUnidades(j,listaCotizaciones,balance_compra_actual,true);
	        	 balanceAcumulado += balance_compra_actual;
	        	 precioDescarga = precioConDelta;
	        	 descargaPrevia = true;
	        	 YA_CARGAMOS_BY_ELLIOTT = false;
		    	 
		    	 if(balance_compra_actual > 0){
	    	    	 final Rendimiento vRendimiento = new Rendimiento(listaCotizaciones.get(j).getFecha(), df.format(balance_compra_actual),porcentaje_real,true,sizeAntesDescarga,diasCargando);
	    	    	 listaRendimientos.add(vRendimiento);
	    	     }
		    	 
	        	 //desacople de resultado de descargas
		    	 if (sizeAntesDescarga >0)
		    		 cerrarOperacion(j, listaCotizaciones, balance_compra_actual,STOCH,true);
		    	 
		     }
		    
			 //Descarga Unidades - FUERTE_AGOTAMIENTO
			 boolean BLOQUEO_CARGA_FUERTE_AGOT_MMT_OK = false;
			 
			 if(!descarga_bloqueada && !ATR_BAJO && FUERTE_AGOTAMIENTO && !PRECIO_ESTA_BANDA_SUPERIOR){
				 
				 if(REGLA_MMT_POSITIVO && REGLA_MMT_DESC_3DIAS || REGLA_MMT_DESC_5DIAS){
					 System.out.println(
								MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
								"- ASC->>" + "FUERTE_AGOTAMIENTO (MMT NOK)" +
								"- ATR:" + df.format(ATR_MAIN) +
								"- Stoch:" + df.format(STOCH)
						 );
					 yaDescargamos = true;
		        	 balance_compra_actual = descargaUnidades(j,listaCotizaciones,balance_compra_actual,true);
		        	 balanceAcumulado += balance_compra_actual;
		        	 precioDescarga = precioConDelta;
		        	 descargaPrevia = true;
		        	 YA_CARGAMOS_BY_ELLIOTT = false;
		        	 	     		
		    		 if(balance_compra_actual > 0){
		    	    	 final Rendimiento vRendimiento = new Rendimiento(listaCotizaciones.get(j).getFecha(), df.format(balance_compra_actual),porcentaje_real,true,sizeAntesDescarga,diasCargando);
		    	    	 listaRendimientos.add(vRendimiento);
		    	     }
		    		 
		        	 //desacople de resultado de descargas
			    	 if (sizeAntesDescarga >0)
			    		 cerrarOperacion(j, listaCotizaciones, balance_compra_actual,STOCH,true);
		    		 
				 } else {
					 if(MetodosDeLogica.aplica_TrazeoFull())
						 System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- ASC->>INVALIDO FUERTE_AGOTAMIENTO (MMT OK)");
					 	BLOQUEO_CARGA_FUERTE_AGOT_MMT_OK = true;
				 	}
	         }

			 //Descarga Unidades - REGLA_DESCARGA_BSUPERIOR
	         if(!descarga_bloqueada && !yaDescargamos && REGLA_DESCARGA_BSUPERIOR &&
	        	MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)>0)
	         {
	        	 System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- ASC->>DESCARGA_BSUPERIOR- STOCH MENOR a:" + df.format(STOCH_ANTERIOR) +
							"- B.Sup:" + df.format(bandaSuperior) +
							"- B.Med:" + df.format(bandaMedia) +
							"- P.Max:" + df.format(listaCotizaciones.get(j).getPrecio_maximo()) +
							"- ATR:" + df.format(ATR_MAIN) 
				 );
	        	 
	        	 yaDescargamos = true;
	        	 balance_compra_actual = descargaUnidades(j,listaCotizaciones,balance_compra_actual,true);
	        	 balanceAcumulado += balance_compra_actual;
	        	 precioDescarga = precioConDelta;
	        	 descargaPrevia = true;
	        	 YA_CARGAMOS_BY_ELLIOTT = false;
	        	 	     		
	    		 if(balance_compra_actual > 0){
	    	    	 final Rendimiento vRendimiento = new Rendimiento(listaCotizaciones.get(j).getFecha(), df.format(balance_compra_actual),porcentaje_real,true,sizeAntesDescarga,diasCargando);
	    	    	 listaRendimientos.add(vRendimiento);
	    	     }
	    		 
	        	 //desacople de resultado de descargas
		    	 if (sizeAntesDescarga >0)
		    		 cerrarOperacion(j, listaCotizaciones, balance_compra_actual,STOCH,true);	        	

	         }

	         //Descarga Unidades - REGLA_TARGET_PREVENTIVO
	         REGLA_TARGET_PREVENTIVO = false;
	         double target = 0;
	         if(!yaDescargamos && !subioPrecio && MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)>0){

	        	 target = ULTIMO_TOP_PRICE - (ULTIMO_TOP_PRICE * MetodosDeLogica.getDesviosMaximosAlcanzados(nombrePapel));
	        	 boolean descargamos = precioConDelta > target;
	        	 boolean pMax_mayor_BSuperior = listaCotizaciones.get(j).getPrecio_maximo() > bandaSuperior;
	        	 boolean pMin_mayor_BMedia = listaCotizaciones.get(j).getPrecio_minimo() > bandaMedia;
	        	 boolean VELAS_MAX_MIN_UP = pMax_mayor_BSuperior && pMin_mayor_BMedia;
	        	 
	        	 if(descargamos && !VELAS_MAX_MIN_UP){

	        		 //Validacion REGLA_ES_GATO_MUERTO_PAPEL_SECTOR
	        		 if(REGLA_ES_GATO_MUERTO_PAPEL_SECTOR == 1){
	        			 REGLA_TARGET_PREVENTIVO = true;
		        		 System.out.println(
		 	        	 		MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		 	        	 		"- ASC->>DESCARGA_TARGET:" + df.format(target) +  "- ULTIMO_TOP_PRICE:" + df.format(ULTIMO_TOP_PRICE) + "- VELAS_MAX_MIN_UP:" +  VELAS_MAX_MIN_UP);	        	 
	        		 }
	        	 }else{
	        		 System.out.println(
	 	        	 		MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	 	        	 		"- ASC->>TARGET:" + df.format(target) +  "- ULTIMO_TOP_PRICE:" + df.format(ULTIMO_TOP_PRICE) + "- VELAS_MAX_MIN_UP:" +  VELAS_MAX_MIN_UP);	        	 
	        	 }
	         }
	         
	    	 if(!descarga_bloqueada && !yaDescargamos && MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)>0){
	    		 if(REGLA_TARGET_PREVENTIVO){
		        	 yaDescargamos = true;
		        	 balance_compra_actual = descargaUnidades(j,listaCotizaciones,balance_compra_actual,true);
		        	 balanceAcumulado += balance_compra_actual;
		        	 precioDescarga = precioConDelta;
		        	 descargaPrevia = true;
		        	 YA_CARGAMOS_BY_ELLIOTT = false;
		        	 		     		
		    		 if(balance_compra_actual > 0){
		    	    	 final Rendimiento vRendimiento = new Rendimiento(listaCotizaciones.get(j).getFecha(), df.format(balance_compra_actual),porcentaje_real,true,sizeAntesDescarga,diasCargando);
		    	    	 listaRendimientos.add(vRendimiento);
		    	     }
		    		 
		        	 //desacople de resultado de descargas
			    	 if (sizeAntesDescarga >0)
			    		 cerrarOperacion(j, listaCotizaciones, balance_compra_actual,STOCH,true);
		    		 
 	    		}	 	 
	         }
	         
			 //Descarga Unidades por OPEX
	         if(!yaDescargamos && DESCARGA_VENCIMIENTO && MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)>0 &&
	        	listaCotizaciones.get(j).getPrecio_maximo() > DESCARGA_VENCIMIENTO_PRECIO)
	         {
	        	 System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- ASC->>DESCARGA_VENCIMIENTO- STOCH MENOR a:" + df.format(STOCH_ANTERIOR) +
							"- B.Sup:" + df.format(bandaSuperior) +
							"- B.Med:" + df.format(bandaMedia) +
							"- P.Max:" + df.format(listaCotizaciones.get(j).getPrecio_maximo()) +
							"- ATR:" + df.format(ATR_MAIN) 
				 );
	        	 yaDescargamos = true;
	        	 balance_compra_actual = descargaUnidades(j,listaCotizaciones,balance_compra_actual,true);
	        	 balanceAcumulado += balance_compra_actual;
	        	 precioDescarga = precioConDelta;
	        	 descargaPrevia = true;
	        	 YA_CARGAMOS_BY_ELLIOTT = false;
	        	 	     		
	    		 if(balance_compra_actual > 0){
	    	    	 final Rendimiento vRendimiento = new Rendimiento(listaCotizaciones.get(j).getFecha(), df.format(balance_compra_actual),porcentaje_real,true,sizeAntesDescarga,diasCargando);
	    	    	 listaRendimientos.add(vRendimiento);
	    	     }
	    		 
	        	 DESCARGA_VENCIMIENTO = false;
	        	 DESCARGA_VENCIMIENTO_PRECIO =0;
	        	 
	        	 //desacople de resultado de descargas
		    	 if (sizeAntesDescarga >0)
		    		 cerrarOperacion(j, listaCotizaciones, balance_compra_actual,STOCH,true);
	         }
	         
	         if(contDivergenciaBajista>1 && semillaSTOCH != 0){
	        	 System.out.println(
					MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
					"- ASC->>DESCARGA DIVERGENCIA BAJISTA");
	        	 
	        	 yaDescargamos = true;
	        	 balance_compra_actual = descargaUnidades(j,listaCotizaciones,balance_compra_actual,true);
	        	 balanceAcumulado += balance_compra_actual;
	        	 precioDescarga = precioConDelta;
	        	 descargaPrevia = true;
	        	 YA_CARGAMOS_BY_ELLIOTT = false;
	        	 	     		
	    		 if(balance_compra_actual > 0){
	    	    	 final Rendimiento vRendimiento = new Rendimiento(listaCotizaciones.get(j).getFecha(), df.format(balance_compra_actual),porcentaje_real,true,sizeAntesDescarga,diasCargando);
	    	    	 listaRendimientos.add(vRendimiento);
	    	     }
	    		 
	        	 //desacople de resultado de descargas
		    	 if (sizeAntesDescarga >0)
		    		 cerrarOperacion(j, listaCotizaciones, balance_compra_actual,STOCH,true);	
	        	 
	         }else if(contDivergenciaBajista>1){
	        	 if(MetodosDeLogica.aplica_TrazeoFull())
		        	 System.out.println(
		 				MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		 				"- ASC->>BLOQUEO DESCARGA DIVERGENCIA BAJISTA" +
		 				"- SemillaStoch:" + df.format(semillaSTOCH));
	         }

//Bloqueo cargas
	         boolean yaCargamosUnidad = false;
	         
	         //Bloqueo esVelon
	         boolean velonReconfirmado = false;
	         boolean velonLivianoConfirmado = false;
	         if(esVelon && !hayNuevoMaximo && diasCargando <=5)          
	         {
	        	 yaCargamosUnidad = true;
	        	 velonReconfirmado = true;
	        	 
    			 if(MetodosDeLogica.aplica_TrazeoFull())
    				 System.out.println(
						 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	    	  			  "- ASC->>BLOQUEO CARGA VELON");
	         }
	         
	         if(!yaCargamosUnidad && esVelon && diasCargando <=5 && STOCH<10)
	 	     {
	 	        	 yaCargamosUnidad = true;
	 	        	 velonReconfirmado = true;
	 	        	 velonLivianoConfirmado = true;
	 	     }

	         if(!yaCargamosUnidad && esVelon && diasCargando <=5 && (STOCH<10 ||STOCH_01<10 ))
	 	     {
	 	        	 yaCargamosUnidad = true;
	 	        	 velonReconfirmado = true;
	 	     }
	         
	         //Bloqueo Carga Resistencia No Vencida
	         if(!yaCargamosUnidad && DESCARGA_GRUESA_ASC && vElliottData != null && vElliottData.getE1() != null){
	        	 double ElliotPrecioMax = vElliottData.getE1().getPrecio_maximo();
		        
		         if (vElliottData.getE3() != null)
		        	 ElliotPrecioMax =  vElliottData.getE3().getPrecio_maximo();
		         
		         if(listaCotizaciones.get(j).getPrecio_maximo() < ElliotPrecioMax)
		         {
		        	 yaCargamosUnidad = true;
		        	 
	    			 if(MetodosDeLogica.aplica_TrazeoFull())
	    				 System.out.println(
							 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    	  			  "- ASC->>BLOQUEO CARGA RESISTENCIA NO VENCIDA (Max>" + df.format(ElliotPrecioMax)  + ")");
		         }else{
		        	 DESCARGA_GRUESA_ASC = false; //desbloqueo
		         }
	         }
	         
	        //Bloqueo esPromedioVolDecreciente
	         if(esPromedioVolDecreciente && !hayNuevoMaximo &&
	 	        MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) <= 50 )
	         {
	        	 if(esValidoBloqueo(j, listaCotizaciones,listaUnidadesOperadas)){
		        	 yaCargamosUnidad = true;
	    			 if(MetodosDeLogica.aplica_TrazeoFull())
	    				 System.out.println(
							 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    	  			  "- ASC->>BLOQUEO CARGA PROMEDIOS VOLUMENES DECRECIENTES");
	        	 }
	         }
	         
	         //Bloqueo Mes Vencimiento
	         double precio_PMax_Delta = 0;
	         double precio_PMax_calculado = listaCotizaciones.get(j).getPrecio_maximo() + (listaCotizaciones.get(j).getPrecio_maximo() * precio_PMax_Delta/100);
	         boolean REGLA_PMAX_ASC = precio_PMax_calculado >= listaCotizaciones.get(j-1).getPrecio_maximo();
	         if(esVencimiento && !REGLA_PMAX_ASC && !hayNuevoMaximo && balanceAnteriorCalculo<1){
	        	 yaCargamosUnidad = true;
	        	 
    			 if(MetodosDeLogica.aplica_TrazeoFull())
    				 System.out.println(
						 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	    	  			  "- ASC->>BLOQUEO CARGA MES VENCIMIENTO");
	         }
	         
	         //Bloqueo carga por resultado negativo
	         if(balance_compra_actual < -1 && !hayNuevoMaximo && !ATR_PMAX_BAJO){
	        	 if(esValidoBloqueo(j, listaCotizaciones,listaUnidadesOperadas)){
	        		 yaCargamosUnidad = true;
		        	 
	    			 if(MetodosDeLogica.aplica_TrazeoFull())
	    				 System.out.println(
							 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    	  			  "- ASC->>BLOQUEO CARGA BALANCE NEGATIVO");
	        	 }
	         }
	         
	         //Bloqueo carga por ATR_PMAX Bajo 
			 boolean REGLA_MMT_ASC_1DIA = listaCalculoMetrica_MMT_15.get(j).getIndicador_valor() > listaCalculoMetrica_MMT_15.get(j-1).getIndicador_valor();
			 boolean REGLA_MMT_ASC_2DIA = listaCalculoMetrica_MMT_15.get(j).getIndicador_valor() > listaCalculoMetrica_MMT_15.get(j-2).getIndicador_valor();
			 boolean REGLA_MMT_ASC_3DIA = listaCalculoMetrica_MMT_15.get(j).getIndicador_valor() > listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor();
			 boolean REGLA_MMT_ASC_5DIA = listaCalculoMetrica_MMT_15.get(j).getIndicador_valor() > listaCalculoMetrica_MMT_15.get(j-5).getIndicador_valor();
			 boolean MMT_ELE_ASC = REGLA_MMT_ASC_1DIA & REGLA_MMT_ASC_2DIA && REGLA_MMT_ASC_3DIA && REGLA_MMT_ASC_5DIA;
	         
	         if(ATR_PMAX_BAJO && !hayNuevoMaximo && !subioPrecio && !MMT_ELE_ASC){
	        	 if(esValidoBloqueo(j, listaCotizaciones,listaUnidadesOperadas)){
	        		 yaCargamosUnidad = true;
		        	 
	    			 if(MetodosDeLogica.aplica_TrazeoFull())
	    				 System.out.println(
							 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    	  			  "- ASC->>BLOQUEO CARGA ATR_PMAX");
	        	 }
	         }
	         
	         if(BLOQUEO_CARGA_FUERTE_AGOT_MMT_OK)
	        	 yaCargamosUnidad = true;
	         
			 //BLOQUEO_CARGA_NUEVOS_MINIMOS
	         boolean REGLA_PREC_MIN_DESC_1 = listaCotizaciones.get(j).getPrecio_minimo() < listaCotizaciones.get(j-1).getPrecio_minimo();
	    	 boolean REGLA_PREC_MIN_DESC_2 = listaCotizaciones.get(j).getPrecio_minimo() < listaCotizaciones.get(j-2).getPrecio_minimo();
	    	 boolean REGLA_PREC_MIN_DESC_3 = listaCotizaciones.get(j).getPrecio_minimo() < listaCotizaciones.get(j-3).getPrecio_minimo();
   	 
	    	 int count = 0;
		     if(REGLA_PREC_MIN_DESC_1) count++;
		     if(REGLA_PREC_MIN_DESC_2) count++;
		     if(REGLA_PREC_MIN_DESC_3) count++;
		     
			 final double STOCH_LOCAL_ANTERIOR = listaCalculoMetrica_STOCH.get(j-1).getValor_main()-listaCalculoMetrica_STOCH.get(j-1).getValor_signal();
			 final double STOCH_LOCAL_PRE_ANTERIOR = listaCalculoMetrica_STOCH.get(j-2).getValor_main()-listaCalculoMetrica_STOCH.get(j-2).getValor_signal();		     
		     
			 boolean STOCH_DESC_0 = STOCH < STOCH_LOCAL_ANTERIOR;
	    	 boolean STOCH_DESC_1 = STOCH < STOCH_LOCAL_PRE_ANTERIOR;
	    	 boolean REGLA_STOCH_DESC = STOCH_DESC_0 || STOCH_DESC_1;
	    	 boolean REGLA_STOCH_NEGATIVO = STOCH < 0 || STOCH_LOCAL_ANTERIOR < 0;
	    	 
	    	 if(!yaCargamosUnidad && ! yaDescargamos && count > 1 && (REGLA_STOCH_DESC || REGLA_STOCH_NEGATIVO)){
	    		 if(!REGLA_MMT_ASC_3DIAS && !REGLA_MMT_ASC_5DIAS){
		    		 if(esValidoBloqueo(j, listaCotizaciones,listaUnidadesOperadas)){
		    			 yaCargamosUnidad = true;
			    		 CONT_BLOQUEO_CARGA_NUEVOS_MINIMOS++;
			    		 
		    			 if(MetodosDeLogica.aplica_TrazeoFull())
		    				System.out.println(
		    					MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			    	  			"- ASC->>BLOQUEO CARGA NUEVOS MINIMOS");
		    		 }
	    		 }
    		 }
	    	 
	    	 //Bloqueo GATO MUERTO
	    	 if(!yaCargamosUnidad && !yaDescargamos && REGLA_ES_GATO_MUERTO){
	    		 if(!REGLA_MMT_ASC_3DIAS && !REGLA_MMT_ASC_5DIAS){
	    			 if(esValidoBloqueo(j, listaCotizaciones,listaUnidadesOperadas)){
			    		 yaCargamosUnidad = true;

			    		 if(MetodosDeLogica.aplica_TrazeoFull())
		    				 System.out.println(
						        MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			    	  			  "- ASC->>BLOQUEO CARGA GATO MUERTO");
	    			 }
	    		 }
	    	 }
	    	 
	    	 //Cargas Giros STOCH 
	    	 if(esCRECIENTE_STOCH){
	    		 yaCargamosUnidad = false;
				 if(MetodosDeLogica.aplica_TrazeoFull())
					 System.out.println(
						 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	    	  			  "- ASC->>CARGA GIROS STOCH- SemillaStoch:" + df.format(semillaSTOCH));
	    		 
	    	 } else 
	    		  if(esCRECIENTE_MMT){
	 				   yaCargamosUnidad = false;
	 				   if(MetodosDeLogica.aplica_TrazeoFull())
	 					   System.out.println(
	 						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	 		    	  	    "- ASC->>CARGA GIROS STOCH(MMT ASC)- SemillaStoch:" + df.format(semillaSTOCH));
	 			    }
				
	    	 //Bloqueo post descarga -> validar descarga anterior de positivo
	    	 if(esGiroStochBloqueado(balanceAcumulado) && balance_compra_actual<0){
	    		 yaCargamosUnidad = true;
    			 if(MetodosDeLogica.aplica_TrazeoFull())
    				 System.out.println(
						 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	    	  			  "- ASC->>BLOQUEO CARGA GIRO (#1,#2)");
	    	 }
	    	 
	    	 
// Carga Unidades	    	 
	    	 
	    	 //Carga W_cargaGrosa
	    	 if(es_W_cargaGrosa && MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) >= 50 &&
	    	    !yaDescargamos &&MetodosDeLogica.aplica_cargaMultiBloque() &&  balance_compra_actual>1)
	 		 {  
 				 if( (!velonReconfirmado && !yaCargamosUnidad && !velonLivianoConfirmado) ||
 					 (yaCargamosUnidad && velonLivianoConfirmado)) 
 				 {
 					 yaCargamosUnidad = true;

 		    		 UnidadOperada vUnidadOperada = null;
 		    		 diasCargando++;
 		    		 int CARGA_BARCO_CONTAINER = 100;
 		    		 
 		    		 if(MetodosDeLogica.aplica_cargaBloque()){ 
 		    			 vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j),CARGA_BARCO_CONTAINER);
 						 listaUnidadesOperadas.add(vUnidadOperada);
 		    		 }
 					 
 					 if(MetodosDeLogica.aplica_TrazeoFull())
 				    	 System.out.println(
 			    	  		MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
 			    	  		"- ASC->>CARGADO BUQUE CONTAINER-REGLA W:" + vUnidadOperada.toString() +
 			    	  		"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)); 
 				 }
	 		 }
	    	 
	    	 
	    	 //Carga PROMEDIO VOLUMEN CRECIENTE 
			 if(esPromedioVolCreciente && hayNuevoMaximo && !ATR_PMAX_BAJO &&
			    balance_compra_actual>1&& !yaDescargamos && 
				MetodosDeLogica.aplica_cargaMultiBloque())
			 {
				 
				 if( (!velonReconfirmado && !yaCargamosUnidad && !velonLivianoConfirmado) ||
					 (yaCargamosUnidad && velonLivianoConfirmado)) 
				 {
					 yaCargamosUnidad = true;

		    		 UnidadOperada vUnidadOperada = null;
		    		 diasCargando++;
		    		 
		    		 if(MetodosDeLogica.aplica_cargaBloque()){ 
		    			 vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j),CARGA_CONTAINER);
						 listaUnidadesOperadas.add(vUnidadOperada);
		    		 }else{
		    			 vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j),1);
						 listaUnidadesOperadas.add(vUnidadOperada);
		    		 }
					 
					 if(MetodosDeLogica.aplica_TrazeoFull())
				    	 System.out.println(
			    	  		MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			    	  		"- ASC->>CARGADO CONTAINER-PROMEDIO VOLUMEN CRECIENTE:" + vUnidadOperada.toString() +
			    	  		"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)); 
				 }
			 }
             
			 if((esCRECIENTE_STOCH || esCRECIENTE_MMT) && ratioCrecimiento >=1 && ratioDiasCarga >= 0.4 &&
			    balance_compra_actual>1&& !yaDescargamos && MetodosDeLogica.aplica_cargaMultiBloque())
			 {
				 
				 if( (!velonReconfirmado && !yaCargamosUnidad && !velonLivianoConfirmado) ||
					 (yaCargamosUnidad && velonLivianoConfirmado)) 
				 {
					 yaCargamosUnidad = true;

		    		 UnidadOperada vUnidadOperada = null;
		    		 diasCargando++;
		    		 
		    		 if(MetodosDeLogica.aplica_cargaBloque()){ 
		    			 vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j),CARGA_CONTAINER);
						 listaUnidadesOperadas.add(vUnidadOperada);
		    		 }
					 
					 if(MetodosDeLogica.aplica_TrazeoFull())
				    	 System.out.println(
			    	  		MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			    	  		"- ASC->>CARGADO CONTAINER-GIROS Y RATIOS CRECIENTES:" + vUnidadOperada.toString() +
			    	  		"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)); 
				 }
			 }			 
			 
	    	 //Carga Elliott Simple
	    	 boolean yaCargamosElliott = false;
	    	 if(!CARGA_BLOQUE_APAGADA && !yaCargamosUnidad && !yaDescargamos && hayNuevoMaximo){
	    		 YA_CARGAMOS_BY_ELLIOTT = true;
	    		 yaCargamosElliott = true;
	    		 UnidadOperada vUnidadOperada = null;
	    		 diasCargando++;
	    		 
	    		 if(MetodosDeLogica.aplica_cargaBloque()){
	    			 vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j),CARGA_BLOQUE);
					 listaUnidadesOperadas.add(vUnidadOperada);
	    		 }else{
	    			 vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j),1);
					 listaUnidadesOperadas.add(vUnidadOperada);
	    		 }
				 
				 if(MetodosDeLogica.aplica_TrazeoFull()){
			    	 System.out.println(
		    	  			MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    	  			"- ASC->>CARGADO BLOQUE-ELLIOTT:" + vUnidadOperada.toString() +
		    	  			"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas));
				 }
	    	 }	    	    	 	   	 
	    	 	    
	    	 //Carga Elliott Inercial
	    	 if(!CARGA_BLOQUE_APAGADA && !yaCargamosUnidad && YA_CARGAMOS_BY_ELLIOTT && !REGLA_ES_GATO_MUERTO && !yaCargamosElliott && !yaDescargamos && MetodosDeLogica.aplica_cargaBloque()){
	    		 yaCargamosElliott = true;
	    		 UnidadOperada vUnidadOperada = null;
	    		 diasCargando++;
	    		 
	    		 if(MetodosDeLogica.aplica_cargaBloque()){
	    			 vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j),CARGA_BLOQUE);
					 listaUnidadesOperadas.add(vUnidadOperada);
	    		 }else{
	    			 vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j),1);
					 listaUnidadesOperadas.add(vUnidadOperada);
	    		 }
				 
				 if(MetodosDeLogica.aplica_TrazeoFull())
			    	 System.out.println(
		    	  			MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    	  			"- ASC->>CARGADO BLOQUE-ELLIOTT INERCIAL:" + vUnidadOperada.toString() +
		    	  			"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas));
				 
	    	 }else{
	    		 if(esValidoBloqueo(j, listaCotizaciones,listaUnidadesOperadas)){
		    		 if(YA_CARGAMOS_BY_ELLIOTT && !yaCargamosElliott && REGLA_ES_GATO_MUERTO){
		    			 if(MetodosDeLogica.aplica_TrazeoFull())
					    	 System.out.println(
			    	  			MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			    	  			"- ASC->>BLOQUEADO ELLIOTT INERCIAL POR GATO MUERTO");
		    		 }
	    		 }
	    	 }		    	 

			 //Carga BLOQUE CARGADO-PAPEL_SECTOR // GIRO_STOCH
 			if((REGLA_ES_GATO_MUERTO_PAPEL_SECTOR == -1 || ( pasosGiroStoch>=3 && (esCRECIENTE_STOCH || esCRECIENTE_MMT) ))&&
	 		    !CARGA_BLOQUE_APAGADA && !yaCargamosUnidad &&
	 		    !yaCargamosElliott && !yaDescargamos && MetodosDeLogica.aplica_cargaBloque())
			 {
				yaCargamosUnidad = true;

	    		 UnidadOperada vUnidadOperada = null;
	    		 diasCargando++;
	    		 
	    		 if(MetodosDeLogica.aplica_cargaBloque()){
	    			 vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j),CARGA_BLOQUE);
					 listaUnidadesOperadas.add(vUnidadOperada);
	    		 }else{
	    			 vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j),1);
					 listaUnidadesOperadas.add(vUnidadOperada);
	    		 }
				 
	    		 if(pasosGiroStoch>=3)
					 if(MetodosDeLogica.aplica_TrazeoFull())
				    	 System.out.println(
		    	  			MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    	  			"- ASC->>CARGADO BLOQUE-PASOS GIRO STOCH:" + vUnidadOperada.toString() +
		    	  			"- s:" + df.format(semillaSTOCH) +
		    	  			"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas));
	    			 
	    		 else	    			 
		    		 
					 if(MetodosDeLogica.aplica_TrazeoFull())
				    	 System.out.println(
		    	  			MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    	  			"- ASC->>CARGADO BLOQUE-PAPEL_SECTOR:" + vUnidadOperada.toString() +
		    	  			"- promedioPreciosMaxsPapelSector:" + df.format(promedioPreciosMaxsPapelSector) +
							"- precioMax:" + df.format(listaCotizacionesPapelSector.get(indiceListaPapelSector).getPrecio_maximo()) +
		    	  			"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas));
				 
			 }
					 	    	 
	    	 //Carga Unidades General
	    	 if(!yaCargamosElliott && !yaCargamosUnidad && !yaDescargamos){
	    		 boolean REGLA_OBV = listaCalculoMetrica_OBV.get(j).getIndicador_valor() > listaCalculoMetrica_OBV.get(j-1).getIndicador_valor();

	    		 if(REGLA_OBV){
	    			 yaCargamosUnidad = true;

	    			 String mensaje = "- ASC->>CARGADA UNIDAD-CARGA(OBV):";
  			 
	    			 UnidadOperada vUnidadOperada = null;
	    			 diasCargando++;
	    			 vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j));
					 listaUnidadesOperadas.add(vUnidadOperada);
					 
					 if(MetodosDeLogica.aplica_TrazeoFull())
						 System.out.println(
								 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			    	  			 mensaje + vUnidadOperada.toString() +
			    	  			 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)); 
	    		 }
	    	 }
	    	 
	         //Carga Unidades - TIME FRAMES(AV-G)
	    	 if(resultadoJuego>1){
	    	  	 if(!yaCargamosElliott && !yaCargamosUnidad && descargaPrevia && !yaDescargamos && !ATR_BAJO && cargamosSegunTendenciaTimeFrames(false,true,j,listaCotizaciones)){

	 	    		UnidadOperada vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j));
	 				listaUnidadesOperadas.add(vUnidadOperada);
	 				diasCargando++;
	 	    	
	 				 if(MetodosDeLogica.aplica_TrazeoFull()){
	 		    	  	System.out.println(
	 		    	  			MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
	 		    	  			"- ASC->>CARGADA UNIDAD-TIME FRAMES(AV-G):" + vUnidadOperada.toString() +
	 		    	  			"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas));
	 				 }
	 	    	 }	 
	    	 }

	    	 //Carga ATR 
	    	 if(!yaCargamosElliott && !yaCargamosUnidad && !yaDescargamos && (esATR_ASC || esOBV_ASC || esVOL_ASC)){
	    		 yaCargamosUnidad = true;
	    		 UnidadOperada vUnidadOperada = null;
	    		 diasCargando++;
    			 vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j));
				 listaUnidadesOperadas.add(vUnidadOperada);
				 
				 if(MetodosDeLogica.aplica_TrazeoFull()){
					String tipoCarga = "ATR_ASC:";
		    		if(esOBV_ASC)tipoCarga = "OBV_ASC:";
		    		if(esVOL_ASC)tipoCarga = "VOL_ASC:";		
				 
					System.out.println(
						 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		    	  		 "- ASC->>CARGADA UNIDAD- " + tipoCarga +
				         vUnidadOperada.toString() +
		    	  		 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas)); 
				 }
	    	 }

     	     //Carga cargaGiroSTOCH
    	  	 if(!yaCargamosElliott && !yaCargamosUnidad && descargaPrevia &&
    	  		!yaDescargamos && pasosGiroStoch >= 3)
    	  	 {
 	    		UnidadOperada vUnidadOperada= new UnidadOperada(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),listaCotizaciones.get(j));
 				listaUnidadesOperadas.add(vUnidadOperada);
 				diasCargando++;
 	    	
 				 if(MetodosDeLogica.aplica_TrazeoFull()){
 		    	  	System.out.println(
 		    	  		MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
 		    	  		"- ASC->>CARGADA UNIDAD- CARGA GIRO STOCH:" + vUnidadOperada.toString() +
 		    	  		"- SemillaStoch:" + df.format(semillaSTOCH) +
 		    	  		"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas));
 				 }
 	    	 }	 
  	 
             if(MetodosDeLogica.aplica_TrazeoFull())
	        	 System.out.println(
        			 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) + 
        			 "- ASC->>FICHAS RESCATADAS:" + ScannerSemillas.cantSemillasRescatadas(listaUnidadesOperadas) +
        			 "- SIN RESCATE:" + ScannerSemillas.cantSemillasNoRescatadas(listaUnidadesOperadas));
    	  	 
	    	 final String desgloseResultado = "(RMoney:"+ df.format(resultadoMoney) + "- RJuego:" + df.format(resultadoJuego) + ")";
			
			 final String trazas = "STOCH:" + df.format(STOCH) +
		     		 "- B.Sup:" + df.format(bandaSuperior) +
					 "- B.Med:" + df.format(bandaMedia) +
					 "- P.Max:" + df.format(listaCotizaciones.get(j).getPrecio_maximo()) +
					 "- P.Min:" + df.format(listaCotizaciones.get(j).getPrecio_minimo()) +
					 "- ATR:" + df.format(ATR_MAIN) +
					 "- VOL:" + df_enteros.format(listaCotizaciones.get(j).getVolumen()) +
					 "- P.Delta:" + df.format(precioConDelta);
			 
			 String trazasMMT15 = "";
			 if(MetodosDeLogica.aplica_TrazeoFull())
				trazasMMT15 = "-MMT:" + listaCalculoMetrica_MMT_15.get(j).getIndicador_valor_formateado() + 
				   "- MMT_03:" + listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor_formateado() +
				   "- MMT_05:" + listaCalculoMetrica_MMT_15.get(j-5).getIndicador_valor_formateado() +
				   "- OBV:" + df_enteros.format(listaCalculoMetrica_OBV.get(j).getIndicador_valor());
						
//Precio ha subido
		     if (subioPrecio){
		    		ScannerSemillas.rescatarSemillas(false,listaCotizaciones,j,listaUnidadesOperadas,es_NO_RescateFichas);		
			     		    	 
		         ultimoMejorOBV_ASC = listaCalculoMetrica_OBV.get(j).getIndicador_valor();

		         if(MetodosDeLogica.aplica_TrazeoFull())
		        	 System.out.println(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) + "- ASC->>" + trazas );
		         
		         System.out.println(
		        		 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		        		 "- ASC->>MANTENER:" + 
		        		 df.format(resultadoJuego) +
		        		 porcentaje_real +
		        		 desgloseResultado +
		        		 trazasMMT15 +
						 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
						 "- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()));
		         
		         return false;
		     }
		     
//Precio ha bajado		    
		     if (!subioPrecio){
		    	 
		    	 if(REGLA_COMENZAR_CIERRE_1 && STOCH < valor_fuerte_stoch_ASC){
			    	if((balanceAcumulado + resultadoJuego) >0){
			    		FUERTE_AGOTAMIENTO = true;
			    		listaFichas = new ArrayList<Ficha>();
						listaFichas.add(new Ficha(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(),false));
			    		
			    		if(MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) > 0){
				    		System.out.println(
			    				 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
				        		 "- ASC->>FUERTE AGOTAMIENTO- Se validara descargar!- Stoch:" + df.format(STOCH) +
				        		 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas));
			    		}
			    	}
		    	 }
		    	 
		    	 boolean REGLA_MANTENER_ELEFANTE_VERDE = STOCH_MAIN > STOCH_SIGNAL;
				 if(REGLA_MANTENER_ELEFANTE_VERDE){
			 
			         if(MetodosDeLogica.aplica_TrazeoFull())
			        	 System.out.println(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) + "- ASC->>" + trazas );
					 
			         System.out.println(
			        		MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			        		"- ASC->>ELEF_VERDE:" + 
			        		df.format(resultadoJuego) +
			        		porcentaje_real +
			        		desgloseResultado + 
			        		trazasMMT15 +
			        		"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
						    "- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()));
			         
			         return false;
			     } 
		    	 
				 double DELTA_STOC = STOCH_MAIN * 0.05;
				 boolean REGLA_MANTENER_ELEFANTE_MINI_ROJO = STOCH_SIGNAL > STOCH_MAIN && (STOCH_SIGNAL- STOCH_MAIN ) < DELTA_STOC;
				 if(REGLA_MANTENER_ELEFANTE_MINI_ROJO){

					 if(CONT_ELEF_MINI_ROJO_ASC < 1){
						 CONT_ELEF_MINI_ROJO_ASC++;
						 
				         if(MetodosDeLogica.aplica_TrazeoFull())
				        	 System.out.println(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) + "- ASC->>" + trazas );
						 
						 System.out.println(
				        		 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
				        		 "- ASC->>ELEF_MINI_ROJO:" +
				        		 df.format(resultadoJuego) +
				        		 porcentaje_real +
				         		 desgloseResultado +
				         		 trazasMMT15 +
				        		 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
							     "- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre())); 
				         
				         return false;
				         
					 } else{
						 
						 boolean ya_descargo_ELEF_MINI_ROJO_ASC = false;
						 if(balance_compra_actual < -1){
							 ya_descargo_ELEF_MINI_ROJO_ASC = true;
						     yaDescargamos = true;
					         balance_compra_actual = descargaUnidades(j,listaCotizaciones,balance_compra_actual,true);
					         balanceAcumulado += balance_compra_actual;
					         CONT_ELEF_MINI_ROJO_ASC++;
					         					  					    	 
						     if(balance_compra_actual > 0){
					    	   	 final Rendimiento vRendimiento = new Rendimiento(listaCotizaciones.get(j).getFecha(), df.format(balance_compra_actual),porcentaje_real,true,sizeAntesDescarga,diasCargando);
					    	   	 listaRendimientos.add(vRendimiento);
					    	 }
						    	 
					         //desacople de resultado de descargas
						     if (sizeAntesDescarga >0)
						    	 cerrarOperacion(j, listaCotizaciones, balance_compra_actual,STOCH,true);
						     						     
						     CARGA_BLOQUE_APAGADA = true;
						     
						     System.out.println(
									MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
									"- ASC->>CARGA_BLOQUE_APAGADA!!");
						     
						     System.out.println(
					        		 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
					        		 "- ASC->>DESCARGA_ELEF_MINI_ROJO_ASC:" +
					        		 df.format(resultadoJuego) +
					        		 porcentaje_real +
					         		 desgloseResultado +
					         		 trazasMMT15 +
					        		 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
								     "- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre())); 
						     
						 }
						 
						 if(!ya_descargo_ELEF_MINI_ROJO_ASC){
							 if(MetodosDeLogica.aplica_TrazeoFull())
					        	 System.out.println(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) + "- ASC->>" + trazas );
							 
							 System.out.println(
					        		 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
					        		 "- ASC->>ELEF_MINI_ROJO_ASC:" +
					        		 df.format(resultadoJuego) +
					        		 porcentaje_real +
					         		 desgloseResultado +
					         		 trazasMMT15 +
					        		 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
								     "- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre())); 
						 }
				    	 return false;
					 }
			     }  
					 
			     boolean REGLA_PRECIO_MAYOR_BANDA_CENTRAL = precioConDelta > listaCalculoMetrica_BAND.get(j).getValor_banda_media();
			     if(REGLA_PRECIO_MAYOR_BANDA_CENTRAL){

			         if(MetodosDeLogica.aplica_TrazeoFull())
			        	 System.out.println(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) + "- ASC->>" + trazas );
			    	 
			    	 System.out.println(
		        		 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		        		 "- ASC->>PREC_MAYOR_BCENTRAL:" + 
		        		 df.format(resultadoJuego) +
		        		 porcentaje_real +
		        		 desgloseResultado +
		        		 trazasMMT15 +
		        		 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
						 "- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()));
			        
			        return false;
     		     }
			    
				 if(REGLA_MMT_POSITIVO && REGLA_MMT_DESC_3DIAS && REGLA_MMT_DESC_5DIAS){

			         if(MetodosDeLogica.aplica_TrazeoFull())
			        	 System.out.println(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) + "- ASC->>" + trazas );
					 
			        System.out.println(
		        		 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		        		 "- ASC->>MANT_MMT:" + 
		        		 df.format(resultadoJuego) +
		        		 porcentaje_real +
		        		 desgloseResultado +
		        		 trazasMMT15 +
						 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
						 "- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()));
			        
			        return false;
				 }
			     
		    	 if(MetodosDeLogica.validarGiros_papel(nombrePapel) && listaFichas != null && listaFichas.size()>0){
		    		double precio_ficha = listaFichas.get(listaFichas.size()-1).getPrecio_cierre_operacion();
					double resultadoFicha = precioConDelta- precio_ficha;
										
		    		if(resultadoFicha > 0){

				         if(MetodosDeLogica.aplica_TrazeoFull())
				        	 System.out.println(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) + "- ASC->>" + trazas );

		    		     System.out.println(
					        		MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
					        		"- ASC->>MANT_FICHA:" + 
					        		df.format(resultadoJuego) +
					        		porcentaje_real +
					        		desgloseResultado +
					        		trazasMMT15 +
					        		"- Ficha:" + df.format(precio_ficha) +					        		
					        		"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
					        		"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()));
		    		     
				         return false;		 
		    		}
		    	 }
		    	 		    	 
		    	 //Evitar cierre con ATR ASC
		    	 if(esATR_ASC || esOBV_ASC || esVOL_ASC){
		    		 
		    		String tipoBloqueo = "MANT_ATR_ASC:";
		    		if(esOBV_ASC)tipoBloqueo = "MANT_OBV_ASC:";
		    		if(esVOL_ASC)tipoBloqueo = "MANT_VOL_ASC:";		    		 
		    		 
			        if(MetodosDeLogica.aplica_TrazeoFull())
			        	 System.out.println(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) + "- ASC->>" + trazas );
					 
			        System.out.println(
		        		 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		        		 "- ASC->>" + tipoBloqueo +
		        		 df.format(resultadoJuego) +
		        		 porcentaje_real +
		        		 desgloseResultado +
		        		 trazasMMT15 +
						 "- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
						 "- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()));
			        
			        return false; 
		    	 }
		    	
		    	 //Evitar cierre por Elliott
		    	 if(vElliottData != null && resultadoJuego>-1)	
		    	 {
		    		 vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),null);

			         if(MetodosDeLogica.aplica_TrazeoFull())
			        	 System.out.println(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) + "- ASC->>" + trazas );
		    		 
		    		 System.out.println(
		        		MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
		        		"- ASC->>MANT_ELLIOTT:" + 
		        		df.format(resultadoJuego) +
		        		porcentaje_real +
		        		desgloseResultado +
		        		trazasMMT15 +
		        		"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
		        		"- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()));
		    		 return false;
		    	 }

		    	 //CERRAR 
		    	 vElliottData = null;
			     ultimoMejorOBV_ASC = 0;
			     listaFichas = new ArrayList<Ficha>();
			     DESCARGA_POR_SALIDA = true;
			    
			     if(precioDescarga != 0) limpiarPiñon(precioDescarga);
			     if(precioDescarga == 0) limpiarPiñon(listaCotizaciones.get(j).getPrecio_cierre());			      
	        	 
	        	 balance_compra_actual = descargaUnidades(j,listaCotizaciones,balance_compra_actual,true);
	        	 balanceAcumulado += balance_compra_actual;
	        	 	        	 	     		
	    		 if(balance_compra_actual > 0){
	    	    	 final Rendimiento vRendimiento = new Rendimiento(listaCotizaciones.get(j).getFecha(), df.format(balance_compra_actual),porcentaje_real,true,sizeAntesDescarga,diasCargando);
	    	    	 listaRendimientos.add(vRendimiento);
	    	     }
			    
	    		 //Estadisticas
			     final Operacion vOperacion = new Operacion(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(), balance_compra_actual, false);
			     lista_resultados_operaciones.add(vOperacion);
			     	    		 
		         if(MetodosDeLogica.aplica_TrazeoFull())
		        	 System.out.println(MetodosFechas.Date_to_String(
		        			 listaCotizaciones.get(j).getFecha()) + 
		        			 "- ASC->>" + trazas );
			     
		         final String desgloseResultadoCerrado = "(RMoney:"+ df.format(balance_compra_actual) + "- RJuego:0,00)";
		         
			     System.out.println(
			    		 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			    		 "- ASC->>SALIR:" + df.format(balance_compra_actual) + porcentaje_real +
			    		 desgloseResultadoCerrado +	
			    		 trazasMMT15 +						  
			    		 "- Prec:" + df.format(listaCotizaciones.get(j).getPrecio_cierre()) 
		    	 );
			     
			     return true;
		     }
		return false;
	}

	/**
	 * 
	 */
	private boolean esValidoBloqueo(int j, List<Cotizacion> listaCotizaciones, List<UnidadOperada> listaUnidadesOperadas) {
		
		if(listaCotizaciones.get(j).getPrecio_minimo()<listaCotizaciones.get(j-1).getPrecio_minimo())
			return true;
		
		if(MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) <= 30)
			return true;
	
		return false;
	}

	/**
	 * Aplica a casos de post descarga en positivo
	 * @return
	 */
	private boolean esGiroStochBloqueado(double balanceAcumulado){
		if((pasosGiroStoch == 1 || pasosGiroStoch == 2) && balanceAcumulado >0)
			return true;
		
		return false;
	}
	

	private int calculaUbicacion(List<Cotizacion> listaCotizacionesPapelSector,Date fecha) {

		if(listaCotizacionesPapelSector == null) return 0; 
		
		int m = 0;
		for(Cotizacion vCotizacion: listaCotizacionesPapelSector){
			if(MetodosFechas.Date_to_String(fecha).equalsIgnoreCase(MetodosFechas.Date_to_String(vCotizacion.getFecha())))
				return m;

			m++;			
		}
		return 0;
	}

	/**
	 * @param j
	 * @param listaCotizaciones
	 * @param balance_compra_actual
	 */
	private void cerrarOperacion(
			int j,
			List<Cotizacion> listaCotizaciones, 
			double balance_compra_actual,
			double STOCH,
			boolean esCompra) 
	{
		
		String cartel = "- ASC";
		if(!esCompra) cartel = "- DESC";
			
		System.out.println(
				 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
				 cartel + "->>OPERACION CERRADA:" + df.format(balance_compra_actual));
		 
		 //Estadisticas
		 final Operacion vOperacion = new Operacion(listaCotizaciones.get(j).getFecha(), listaCotizaciones.get(j).getPrecio_cierre(), balance_compra_actual, false);
		 lista_resultados_operaciones.add(vOperacion);
		 
		 //Reseteo de acumuladores
		 resultadoMoney = 0;
		 resultadoJuego = 0;
		 diasCargando = 0;
		 diasTranscurridos = 0;
		 DESCARGA_GRUESA_ASC = true;
		 
		 pasosGiroStoch = 0; //JFM
		 semillaSTOCH = STOCH;
		 contDivergenciaBajista = 0;
		 
		 System.out.println(
			MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
			cartel + "->>***CERRAR OPERACION- PasosGiroStoch:0- SemillaStoch:" + df.format(semillaSTOCH));
	}

	/**
	 * 
	 * @param listaCotizaciones
	 * @param j
	 * @param listaCalculoMetrica_ATR
	 * @param listaCalculoMetrica_BAND
	 */
	private void cargar_valores_Elliott(
				List<Cotizacion> listaCotizaciones, 
				int j, 
				List <CalculoMetrica> listaCalculoMetrica_ATR,
				List <CalculoMetricaBand> listaCalculoMetrica_BAND)
	{
		
			//fechaStringCorteDebug = "2016-02-10";
			if(MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()).equals(fechaStringCorteDebug)){
				System.out.println("");
	    	}
		
			if(vElliottData == null) return;
			
			//Anulacion de semilla
			if(vElliottData.es_valido_carga_E1_E2() && vElliottData.getE0().getPrecio_minimo()>listaCotizaciones.get(j).getPrecio_minimo()){
				
				if(MetodosDeLogica.aplica_TrazeoFull()){
					System.out.println(
						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						"- ASC->>ELLIOTT ANULADO POR ROMPER E0");
				}
				//vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),null);
				vElliottData = null;
				return;
			}
	
			//Anulacion E1
			if(!vElliottData.es_valido_carga_E1_E2() &&
				vElliottData.es_valido_carga_E3_E4() &&
				//vElliottData.getE1().getPrecio_maximo()>listaCotizaciones.get(j).getPrecio_minimo() &&
				//vElliottData.getE1().getPrecio_maximo()>listaCotizaciones.get(j-1).getPrecio_minimo() &&
				vElliottData.getE2().getPrecio_maximo()>listaCotizaciones.get(j).getPrecio_minimo())
			{
				if(MetodosDeLogica.aplica_TrazeoFull()){
					System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- ASC->>ELLIOTT ANULADO POR ROMPER E1_E2");
				}
				vElliottData = null;
				return;
			}
			
			//Anulacion E3
			if(!vElliottData.es_valido_carga_E1_E2() &&
			   !vElliottData.es_valido_carga_E3_E4() &&
			   vElliottData.es_valido_carga_E5() &&
			   //vElliottData.getE3().getPrecio_maximo()>listaCotizaciones.get(j).getPrecio_minimo() &&
			   //vElliottData.getE3().getPrecio_maximo()>listaCotizaciones.get(j-1).getPrecio_minimo() &&
			   vElliottData.getE4().getPrecio_maximo()>listaCotizaciones.get(j).getPrecio_minimo())
			{
				if(MetodosDeLogica.aplica_TrazeoFull()){
					System.out.println(
						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						"- ASC->>ELLIOTT ANULADO POR ROMPER E3_E4");
				}
				vElliottData = null;
				return;
			}
			
			//Anulacion E5
			if(!vElliottData.es_valido_carga_E1_E2() &&
			   !vElliottData.es_valido_carga_E3_E4() &&
			   vElliottData.es_valido_carga_E5() &&
			   listaCalculoMetrica_BAND.get(j).getValor_banda_media() > listaCotizaciones.get(j).getPrecio_cierre())
			{
				if(MetodosDeLogica.aplica_TrazeoFull()){
					System.out.println(
						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						"- ASC->>ELLIOTT ANULADO POR ROMPER E5");
				}
				vElliottData = null;
				return;
			}
			
			//CARGANDO onda1/onda2
			if(vElliottData.es_valido_carga_E1_E2()){
				
				if(vElliottData.getE1() == null) {
					vElliottData.setE1(listaCotizaciones.get(j));
					vElliottData.setE2(listaCotizaciones.get(j));
					vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"CARGANDO E1_E2");
					return;
				}
							
				if(listaCotizaciones.get(j).getPrecio_maximo() > vElliottData.getE1().getPrecio_maximo()){
					
					if(vElliottData.isHabilitaCarga_E1()){
						vElliottData.setE1(listaCotizaciones.get(j));
						vElliottData.setE2(listaCotizaciones.get(j));//blanquear NUEVO MIN
						vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"MAX E1");
						hayNuevoMaximo = true;
						return;
					}
					
				} else{
					if(listaCotizaciones.get(j).getPrecio_minimo() < vElliottData.getE2().getPrecio_minimo()){
						vElliottData.setE2(listaCotizaciones.get(j));
						vElliottData.countE2++;
						vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"MIN E2");
						
						if(vElliottData.isHabilitaCarga_E1() &&
						   vElliottData.countE2 >= 3){
								vElliottData.setHabilitaCarga_E1(false);//CIERRA E1		
							    System.out.println(
									MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
									"- ASC->>CIERRA E1: " +  vElliottData.getE1().getPrecio_maximo());
						}
						return;
					}
				}
			}
			
			//Filtrando olas E1-E2
			if(vElliottData.es_valido_carga_E1_E2() &&
			   listaCotizaciones.get(j).getPrecio_maximo() <= vElliottData.getE1().getPrecio_maximo() &&	
			   listaCotizaciones.get(j).getPrecio_minimo() >= vElliottData.getE2().getPrecio_minimo()		
			){
				if(vElliottData.countE2 >0) vElliottData.countE2--;
				vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"FILTRO CONTRA OLAS E1_E2");
				return;
			}
			
			//Buscando cerrar E1 y E2
			if( vElliottData.es_valido_carga_E1_E2() &&
				!vElliottData.es_valido_carga_E3_E4() &&
			    listaCotizaciones.get(j).getPrecio_maximo() > vElliottData.getE1().getPrecio_maximo()
			  ){
				    vElliottData.setHabilitaCarga_E1(false);//Bloquear E1 y E3
					vElliottData.setHabilitaCarga_E2(false);
					
					vElliottData.setHabilitaCarga_E3(true);//Habilitar E3 y E4
					vElliottData.setHabilitaCarga_E4(true);
					
					Cotizacion cotizBuscada = buscando_Minimos(listaCotizaciones,j,10);
					
					if(vElliottData.getE3() == null){
						vElliottData.setE3(cotizBuscada);
					}else{
						if(cotizBuscada.getPrecio_minimo() < vElliottData.getE3().getPrecio_minimo()){
							vElliottData.setE3(cotizBuscada);
						}	
					}
					vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"CONFIRMADA ONDA 3");
			}
			  
			//CARGANDO E3/E4
			if(vElliottData.es_valido_carga_E3_E4()){
				
				if(vElliottData.getE3() == null) {
					vElliottData.setE3(listaCotizaciones.get(j));
					vElliottData.setE4(listaCotizaciones.get(j));
					vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"E3==NULL");
					return;
				}
							
				if(listaCotizaciones.get(j).getPrecio_maximo() >= vElliottData.getE3().getPrecio_maximo()){
					//carga E3
					if(vElliottData.isHabilitaCarga_E3()){
						vElliottData.setE3(listaCotizaciones.get(j));
						vElliottData.setE4(listaCotizaciones.get(j));//blanqueando NUEVO MIN
						vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"MAX E3");
						hayNuevoMaximo = true;
						return;
					}
				} else {
					//carga E4
					if(listaCotizaciones.get(j).getPrecio_minimo() < vElliottData.getE4().getPrecio_minimo()){
						vElliottData.setE4(listaCotizaciones.get(j));
						vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"MIN E4");
						
						if(vElliottData.isHabilitaCarga_E3() &&
						   vElliottData.getE4().getPrecio_minimo() < listaCotizaciones.get(j-1).getPrecio_minimo() &&
						   vElliottData.getE4().getPrecio_minimo() < listaCotizaciones.get(j-2).getPrecio_minimo() &&
						   vElliottData.getE4().getPrecio_minimo() < listaCotizaciones.get(j-3).getPrecio_minimo() ){
								vElliottData.setHabilitaCarga_E3(false);						
								System.out.println(
										MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
										"- ASC->>CIERRA E3: " +  vElliottData.getE3().getPrecio_maximo());
								vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"CIERRA E3");
						}
						return;
					}
				}
			}
			
			//Filtrando olas E3-E4
			if(vElliottData.es_valido_carga_E3_E4() &&
			   listaCotizaciones.get(j).getPrecio_maximo() <= vElliottData.getE3().getPrecio_maximo() &&	
			   listaCotizaciones.get(j).getPrecio_minimo() >= vElliottData.getE4().getPrecio_minimo()		
			){
				if(vElliottData.countE4 >0) vElliottData.countE4--;
				vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"FILTRO CONTRA OLAS E3_E4");
				return;
			}
			
			//Buscando E5
			if( !vElliottData.es_valido_carga_E1_E2() &&
				vElliottData.isHabilitaCarga_E4() &&
			    listaCotizaciones.get(j).getPrecio_maximo() > vElliottData.getE3().getPrecio_maximo()
			  ){
					vElliottData.setHabilitaCarga_E3(false);
					vElliottData.setHabilitaCarga_E4(false);
					vElliottData.setHabilitaCarga_E5(true);
					
					Cotizacion cotizBuscada = buscando_Minimos(listaCotizaciones,j,5);
					
					if(vElliottData.getE5() == null){
						vElliottData.setE5(cotizBuscada);
					}else{
						if(cotizBuscada.getPrecio_minimo() < vElliottData.getE3().getPrecio_minimo()){
							vElliottData.setE3(cotizBuscada);
						}	
					}
					vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"CONFIRMADA ONDA 5");
			}
	
			//carga E5
			if(vElliottData.isHabilitaCarga_E5() &&
			   listaCotizaciones.get(j).getPrecio_maximo() >= vElliottData.getE5().getPrecio_maximo())
			{
				vElliottData.setE5(listaCotizaciones.get(j));
				vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"MAX E5");
				hayNuevoMaximo = true;
			}
		}

	/**
	 * 
	 * @param listaCotizaciones
	 * @param j
	 * @param cantBusquedaAtras
	 * @return
	 */
	private Cotizacion buscando_Minimos(List<Cotizacion> listaCotizaciones, int j, int cantBusquedaAtras) {
		Cotizacion vCotizacionActual = listaCotizaciones.get(j);
		for(int r=0; r<cantBusquedaAtras; r++){
			int count = j--;
			if (vCotizacionActual.getPrecio_minimo() > listaCotizaciones.get(count).getPrecio_minimo()){
				vCotizacionActual = listaCotizaciones.get(count);
			}
		}
		return vCotizacionActual;
	}
	
	/**
	 * 
	 * @param listaCotizaciones
	 * @param j
	 */
	private void buscando_E0_E1_E2(
			List<Cotizacion> listaCotizaciones, 
			int j) 
	{
		int cantBusquedaAtras = 50;
		int countCalculado = 0;
		int countBuscado = j;
		
		//Cargando E0
		Cotizacion vCotizacionBuscado = listaCotizaciones.get(j);
		for(int r=0; r<cantBusquedaAtras; r++){
			countCalculado = j-1;
			if (listaCotizaciones.get(countCalculado).getPrecio_minimo() < vCotizacionBuscado.getPrecio_minimo()){
				vCotizacionBuscado = listaCotizaciones.get(countCalculado);
				countBuscado = countCalculado;
			}
		}
		vElliottData.setE0(vCotizacionBuscado);
		
		//Cargando E1_E2
		vElliottData.setE1(listaCotizaciones.get(countBuscado));
		vElliottData.setE2(listaCotizaciones.get(countBuscado));
						
		for(int r=countBuscado; r<j; r++){
						
			if(listaCotizaciones.get(r).getPrecio_maximo() >= vElliottData.getE1().getPrecio_maximo()){
					vElliottData.setE1(listaCotizaciones.get(r));
					vElliottData.setE2(listaCotizaciones.get(r));
					//vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"Calculando MAX E1");
					continue;
			}else{
				if(listaCotizaciones.get(r).getPrecio_minimo() < vElliottData.getE2().getPrecio_minimo()){
					vElliottData.setE2(listaCotizaciones.get(r));
					//vElliottData.countE2++;
					//vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"Calculando MIN E2");
					continue;
				}
			}
			
			//Filtrando olas E1-E2
			if(vElliottData.es_valido_carga_E1_E2() &&
			   listaCotizaciones.get(j).getPrecio_maximo() <= vElliottData.getE1().getPrecio_maximo() &&	
			   listaCotizaciones.get(j).getPrecio_minimo() >= vElliottData.getE2().getPrecio_minimo()		
			){
				//if(vElliottData.countE2 >0) vElliottData.countE2--;
				//vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"Calculando FILTRO CONTRA OLAS E1_E2");
				continue;
			}
		}
				
		vElliottData.mostrarValoresElliott(listaCotizaciones.get(j),"Calculando E1 E2");
		vElliottData.setHabilitaCarga_E1(true);
		vElliottData.setHabilitaCarga_E2(true);
	}
	
	/**
	 * 
	 * @param precioSalida
	 */
	private void limpiarPiñon(double precioSalida) {
		FichaPiñon vFichaPiñon = listaFichasPiñon.get(listaFichasPiñon.size()-1);
		vFichaPiñon.setPrecioSalida(precioSalida);

		if(precioSalida > vFichaPiñon.getPrecioOriginal()){
			listaFichasPiñonHistorico.add(vFichaPiñon);
		}else{
			listaFichasPiñon.remove(vFichaPiñon);
		}
	}

	/**
	 * 
	 * @param listaCotizaciones
	 * @param j
	 * @param balance_short_actual
	 * @return
	 */
	private boolean esFichaNueva(
			 List<Cotizacion> listaCotizaciones,
			 int j, 
			 double balance_short_actual ) 
	 {
	    
		 if(listaFichas != null ){
			 final Ficha ultimaFicha = listaFichas.get(listaFichas.size()-1);
			 
			 if(balance_short_actual > 1){
				 if (listaCotizaciones.get(j).getPrecio_cierre() < ultimaFicha.getPrecio_cierre_operacion()){
					 
					 double baja = ultimaFicha.getPrecio_cierre_operacion()-listaCotizaciones.get(j).getPrecio_cierre(); 
					 double minimaBaja = listaCotizaciones.get(j).getPrecio_cierre()*0.03;
					 
					 if (baja > minimaBaja){
						 if(listaFichas.get(listaFichas.size()-1).isEsSemilla()){
							 listaFichas = new ArrayList<Ficha>(); //limpiando la semilla 
						 }
						 listaFichas.add(new Ficha(listaCotizaciones.get(j-1).getFecha(), listaCotizaciones.get(j-1).getPrecio_cierre(),false));
						 return true;
					 }
				 } 
			 }
		 }
		return false;
	 }

	/**
	 * 
	 * @param listaCotizaciones
	 * @param j
	 * @return
	 */
	private boolean esFichaFalsa(List<Cotizacion> listaCotizaciones, int j, List<CalculoMetrica> listaCalculoMetrica_ATR ) {
		 
		final Ficha ultimaFicha = listaFichas.get(listaFichas.size()-1);
		double precioFicha = ultimaFicha.getPrecio_cierre_operacion();
		double precioActual = listaCotizaciones.get(j).getPrecio_cierre();
		double precioPrevio = listaCotizaciones.get(j-1).getPrecio_cierre();
		
		if(!ultimaFicha.isEsSemilla()){
			if(precioFicha <= precioActual && precioFicha <= precioPrevio){
				String cartel = "**FICHA Prec VALIDA" +
								"- ATR:" +df.format(listaCalculoMetrica_ATR.get(j).getIndicador_valor()) + 
								"- Ficha:" + df.format(precioFicha) + "- Prec Actual:" + df.format(precioActual);
				
				 System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- DESC->>" + cartel);
				
				return false;
			}
		}
		
		boolean REGLA_ATR_SEMILLA_VALIDA00 = listaCalculoMetrica_ATR.get(j).getIndicador_valor() <= listaCalculoMetrica_ATR.get(j-1).getIndicador_valor();
		boolean REGLA_ATR_SEMILLA_VALIDA01 = listaCalculoMetrica_ATR.get(j-1).getIndicador_valor() <= listaCalculoMetrica_ATR.get(j-2).getIndicador_valor();
		if(REGLA_ATR_SEMILLA_VALIDA00 && REGLA_ATR_SEMILLA_VALIDA01){
			
			if(MetodosDeLogica.aplica_TrazeoFull()){
				String cartel = "**FICHA ATR VALIDA" +
						"- ATR:" + df.format(listaCalculoMetrica_ATR.get(j).getIndicador_valor()) + 
						"- Ficha:" + df.format(precioFicha) + "- Prec Actual:" + df.format(precioActual);
				
				 System.out.println(
							MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
							"- DESC->>" + cartel);

			}
			return false;
		}
		
		if(MetodosDeLogica.aplica_TrazeoFull()){
			String cartel = "**FICHA FALSA" +
					"- ATR:" + df.format(listaCalculoMetrica_ATR.get(j).getIndicador_valor()) + 
					"- ATR_PREV:" + df.format(listaCalculoMetrica_ATR.get(j-1).getIndicador_valor()) +
					"- Ficha:" + df.format(precioFicha) + "- Prec Actual:" + df.format(precioActual);

			System.out.println(
						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						"- DESC->>" + cartel);

		}
		return true;
	}

	/**
	 * True(Compra) - False(Short)
	 * @param cantidadDiasPrevio
	 * @param listaCotizaciones
	 * @param listaCalculoMetrica_OBV
	 * @param listaCalculoMetrica_STOCH
	 * @param listaCalculoMetrica_BAND
	 * @param listaCalculoMetrica_ATR
	 * @param esVisible
	 * @return
	 */
	
	private boolean calculos_cierre_previo(
			int indice,
			int cantidadDiasPrevio,
			List<Cotizacion> listaCotizaciones,
			List<CalculoMetrica> listaCalculoMetrica_OBV,
			List<CalculoMetricaSTOCH> listaCalculoMetrica_STOCH,
			List<CalculoMetricaBand> listaCalculoMetrica_BAND,
			List<CalculoMetrica> listaCalculoMetrica_ATR,
			List <CalculoMetrica> listaCalculoMetrica_MMT_20,
			boolean esVisible) 
	{
		  String tipoCierre = "- CIERRE " + cantidadDiasPrevio*24 + "HS(Ficha Simulada)->> ";
		 
		  boolean short_activo = false;
		  boolean compra_activa = false;
		  int r = indice;
		  
		  if(indice == 0){
			  r = (listaCotizaciones.size()-1)-cantidadDiasPrevio;  
		  }
		 
		  //Simulacion ficha creada
		  List<Ficha> listaFichas_SM = new ArrayList<Ficha>();
		  listaFichas_SM.add(new Ficha(listaCotizaciones.get(r-1).getFecha(), listaCotizaciones.get(r-1).getPrecio_cierre(),true));
		  		  
	      //Compramos?
		  compra_activa = compramos_SASC_simula(
							   r,
							   listaCotizaciones,
							   listaCalculoMetrica_STOCH,
							   listaFichas_SM,
							   listaCalculoMetrica_MMT_20,
							   listaCalculoMetrica_ATR,
							   false
						   );

		  
		  if(!compra_activa){
			  short_activo = shorteamos_SASC_simula(
								  r,
								  listaCotizaciones,
								  listaCalculoMetrica_STOCH,
								  listaCalculoMetrica_BAND,
								  listaCalculoMetrica_ATR,
								  false
						 	);
		  }	
		  		  
		  String operacion = "";
		  if (short_activo || compra_activa){
				if(compra_activa)
					operacion = "COMPRAR";
				else
					operacion = "SHORTEAR";
				
				if(esVisible)
				System.out.println(
					  MetodosFechas.Date_to_String(listaCotizaciones.get(r).getFecha()) +
					  tipoCierre + operacion + 
					  "- Prec:" + df.format(listaCotizaciones.get(r).getPrecio_cierre()));
				
				if(compra_activa)
					return true; 
				else
					return false;
		  }
		  
		  if(esVisible)
		  System.out.println(
				  MetodosFechas.Date_to_String(listaCotizaciones.get(r).getFecha()) +
				  tipoCierre + "NADA");
		  
		  return false;
		  
	}

	/**
	 * 
	 * @param nombrePapel
	 * @return
	 */
	private double getFuerteStochPapel(boolean isAsc, String nombrePapel) {
	
		//FUERTE_STOCH_ASC=GGAL:1-APBR:15-PAMP:1-AY24:10
		if(isAsc){
			if(REA_PropertiesController.FUERTE_STOCH_ASC != null){
				String[] cadenaStoch = REA_PropertiesController.FUERTE_STOCH_ASC.split(",");
				for(String cadena: cadenaStoch ){
					if (nombrePapel.equalsIgnoreCase(cadena.split(":")[0])){
						return Double.parseDouble(cadena.split(":")[1]);
					};
				}
			}
		}else{
			if(REA_PropertiesController.FUERTE_STOCH_DESC != null){
				String[] cadenaStoch = REA_PropertiesController.FUERTE_STOCH_DESC.split(",");
				for(String cadena: cadenaStoch ){
					if (nombrePapel.equalsIgnoreCase(cadena.split(":")[0])){
						return Double.parseDouble(cadena.split(":")[1]);
					};
				}
			}
		}
		 
		return 15; //valor default para ambos
	}

	/**
	 * 
	 * @param esVisible
	 * @param esCompra
	 * @param j
	 * @param listaCotizaciones
	 * @return
	 */
	private boolean cargamosSegunTendenciaTimeFrames(
			boolean esVisible, 
			boolean esCompra,
			int j, 
			List<Cotizacion> listaCotizaciones) 
	{
				
		String operacion = null;
		Cotizacion vCotizacion = null;
		boolean isAscendente = false;
		double precioAnterior = 0;
		double resultado=0;
		String cadena_tendenciaFinal=null;
		String tendenciaFinal=null;
		StringBuilder sb1 = new StringBuilder();
				
		//TimeFrame_A
		precioAnterior = 0;
		cadena_tendenciaFinal = null;
		
		if(REA_PropertiesController.CANTIDAD_TIME_FRAMES_MOSTRADOS == null){
			System.out.println("Setear CANTIDAD_TIME_FRAMES_MOSTRADOS");
		}
					
		int k = 0;
		int indexBuscado = j- (Integer.parseInt(timeFrame[0]) * Integer.parseInt(REA_PropertiesController.CANTIDAD_TIME_FRAMES_MOSTRADOS));
		
		if(indexBuscado > 0){
			sb1.append("\n");
			sb1.append("** TIME-FRAME_0" + timeFrame[0] + "\n");
			
			for(k=indexBuscado; k<j; k++){
				vCotizacion = listaCotizaciones.get(k);
	
				if(precioAnterior == 0) operacion = "-> X";
								
				resultado = vCotizacion.getPrecio_cierre()- precioAnterior;
				isAscendente = resultado >= 0;
				if (precioAnterior != 0 && isAscendente)
					operacion = "-> ASC (" + df.format(resultado) + ")";
				else if (precioAnterior != 0 && !isAscendente)
					operacion = "-> DESC (" + df.format(resultado) + ")";
				
				sb1.append(
						MetodosFechas.Date_to_String(vCotizacion.getFecha()) +  
						"- Prec:" + df.format(vCotizacion.getPrecio_cierre()) +
						operacion + "\n"
				);
							
				precioAnterior = vCotizacion.getPrecio_cierre();
				cadena_tendenciaFinal = cadena_tendenciaFinal + operacion;
				k= k-1+Integer.parseInt(REA_PropertiesController.CANTIDAD_TIME_FRAMES_MOSTRADOS);
			}
					
			tendenciaFinal = MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha());
			boolean cargadaDescripcion = false;
			if (!cargadaDescripcion && !cadena_tendenciaFinal.contains("DESC") && listaCotizaciones.get(j).getPrecio_cierre() >  vCotizacion.getPrecio_cierre()){
				tendenciaFinal = tendenciaFinal + "- **ASC Polenta- Recarga- PrecioActual > PrecioUltimoTimeFrame (" + df.format(vCotizacion.getPrecio_cierre()) + ")";
				cargadaDescripcion = true;
			}		
			
			if (!cargadaDescripcion && !cadena_tendenciaFinal.contains("ASC") && listaCotizaciones.get(j).getPrecio_cierre() <  vCotizacion.getPrecio_cierre()){
				tendenciaFinal = tendenciaFinal + "- **DESC Polenta- Recarga- PrecioActual < PrecioUltimoTimeFrame (" + df.format(vCotizacion.getPrecio_cierre()) + ")";
				cargadaDescripcion = true;
			}
			
			if (!cargadaDescripcion && listaCotizaciones.get(k).getPrecio_cierre() >  vCotizacion.getPrecio_cierre()){
				tendenciaFinal = tendenciaFinal + "- **ASC- Recarga- PrecioActual > PrecioUltimoTimeFrame (" + df.format(vCotizacion.getPrecio_cierre()) + ")";
				cargadaDescripcion = true;
			}
				
			if (!cargadaDescripcion && listaCotizaciones.get(k).getPrecio_cierre() <  vCotizacion.getPrecio_cierre()){
				tendenciaFinal = tendenciaFinal + "- **DESC- Recarga- PrecioActual < PrecioUltimoTimeFrame (" + df.format(vCotizacion.getPrecio_cierre()) + ")";
				cargadaDescripcion = true;
			}
				
			sb1.append(tendenciaFinal + "\n");
			
			if(esVisible)
			System.out.println(sb1.toString());
			
			//Decision Recarga
			if (esCompra && !tendenciaFinal.contains("DESC")){
			//	System.out.println("****" + tendenciaFinal);
				return true;
			}
	
			if (!esCompra && !tendenciaFinal.contains("ASC")){
			//	System.out.println("****" + tendenciaFinal);
				return true;
			}
			
		}else{
			if(esVisible)
			System.out.println("No hay datos para TIME-FRAME_0" + timeFrame[0]);
		}
	
	return false;
	}

	/**
	 * @param j
	 * @param listaCotizaciones
	 * @param balance_compra_actual
	 * @return
	 */
	private double descargaUnidades(int j,
			List<Cotizacion> listaCotizaciones,
			double balance_compra_actual,
			boolean esCompra) 
	{
		
		if(MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) == 0){
			 FUERTE_AGOTAMIENTO = false;
			 PRE_EVENTO = false;
			 REGLA_DESCARGA_BSUPERIOR = false;
			 REGLA_TARGET_PREVENTIVO = false;
			 PELIGRO_INVISIBLE = false;
			 DESCARGA_VENCIMIENTO = false;
			 DESCARGA_POR_SALIDA = false;
					 
		 }else{
			 //unidades a descargar
			 int unidadesAbsolutasVender = listaUnidadesOperadas.size();
			 long cantidadTotalVender = MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas);
			 List <UnidadOperada> auxUnidadesOperadas = new ArrayList<UnidadOperada>();
			 for(int k=0; k<unidadesAbsolutasVender; k++){
				 UnidadOperada vUnidadOperada = listaUnidadesOperadas.get(listaUnidadesOperadas.size() -1);
				 auxUnidadesOperadas.add(vUnidadOperada);
				 boolean fueEliminada = listaUnidadesOperadas.remove(vUnidadOperada);
				 //if(fueEliminada)System.out.println("Descargada unidad");
			 }
			 
		     //recalculo resultado total
			 for(UnidadOperada vUnidadOperada: auxUnidadesOperadas){
				 balance_compra_actual = calcularBalance(true,esCompra,vUnidadOperada,j,listaCotizaciones);
			 }
			 
			 String mensaje = ":";
			 if(FUERTE_AGOTAMIENTO)mensaje = "(F.AGOT):";
			 if(REGLA_DESCARGA_BSUPERIOR)mensaje = "(B.SUP):";
			 if(REGLA_TARGET_PREVENTIVO)mensaje = "(TARGET):";
			 if(PRE_EVENTO)mensaje = "(PRE_EVENTO):";
			 if(PELIGRO_INVISIBLE)mensaje = "(PELIGRO_INVISIBLE):";
			 if(DESCARGA_VENCIMIENTO)mensaje = "(DESCARGA_VENCIMIENTO):";
			 if(DESCARGA_POR_SALIDA)mensaje = "(DESCARGA_POR_SALIDA):";
			 			 			 
			 FUERTE_AGOTAMIENTO = false;
			 REGLA_DESCARGA_BSUPERIOR = false;
			 REGLA_TARGET_PREVENTIVO = false;
			 PRE_EVENTO = false;
			 PELIGRO_INVISIBLE = false;
			 DESCARGA_VENCIMIENTO = false;
			 DESCARGA_POR_SALIDA = false;
			 			 
			 String operacion = "- ASC";
			 if(!esCompra)operacion = "- DESC";
				 
			 System.out.println(
					 MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
					 operacion + "->>SE DESCARGARON UNIDADES" + mensaje + cantidadTotalVender + "- QUEDAN:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas));
			 
			 CONT_BLOQUEO_CARGA_NUEVOS_MINIMOS = 0;
			 
		}
		return balance_compra_actual;
	}

	/**
	 * 
	 * @param listaCotizaciones
	 * @param k
	 * @param balance_compra_actual
	 * @return
	 */
	private boolean cierreForzado(List<Cotizacion> listaCotizaciones, int k, double balance_compra_actual ) {
		
		String fechaCorte = null;
		
		if(MetodosDeLogica.validar_fecha_ejercicio_papel(MetodosFechas.Date_to_String(listaCotizaciones.get(k).getFecha()))){
			fechaCorte = MetodosFechas.Date_to_String(listaCotizaciones.get(k).getFecha());
		}
		
		if(FECHA_CIERRE_FORZADO != null){
			fechaCorte = MetodosFechas.Date_to_String(FECHA_CIERRE_FORZADO.getTime());
		}
		
		if(MetodosFechas.Date_to_String(listaCotizaciones.get(k).getFecha()).equals(fechaCorte)){
			//Estadisticas
		     final Operacion vOperacion = new Operacion(listaCotizaciones.get(k).getFecha(), listaCotizaciones.get(k).getPrecio_cierre(), balance_compra_actual, false);
		     lista_resultados_operaciones.add(vOperacion);
		     
		     String desgloseResultado = "(RMoney:"+ df.format(resultadoMoney) + "- RJuego:" + df.format(resultadoJuego) + ")";
		     
		     String mensaje = "- XXX->>*** CIERRE DE EJERCICIO:"; 
		     if(FECHA_CIERRE_FORZADO != null){
		    	 mensaje = "- XXX->>*** CIERRE FORZADO:";
		     }
		    		 
		     System.out.println(
	   			 MetodosFechas.Date_to_String(listaCotizaciones.get(k).getFecha()) +
			 			mensaje +
	   					df.format(balance_compra_actual) +
			 			desgloseResultado +
			 			"- Unid:" + MetodosDeLogica.calculaSizeUnidadesCargadas(listaUnidadesOperadas) +
			 			"- Prec:" + df.format(listaCotizaciones.get(k).getPrecio_cierre())
		     );
			return true;
		}
		return false;
		
	}



	/**
	 * 
	 * @param pUnidadOperada
	 * @param j
	 * @param listaCotizaciones
	 * @return
	 */
	private double calcularBalance(boolean esDescarga,boolean esCompra, UnidadOperada pUnidadOperada, int j,List<Cotizacion> listaCotizaciones) {
		double resultadoCalculado;
		resultadoJuego = 0;
		
		String mensaje = "- ASC->>";
		if(!esCompra)
			mensaje = "- DESC->>";
		
		//calculo resultadoJuego
		for(UnidadOperada unidadOperada: listaUnidadesOperadas){
			if(esCompra){
				resultadoCalculado = unidadOperada.calculoResultado(esDescarga, true, listaCotizaciones.get(j).getPrecio_cierre());
			}else{
				resultadoCalculado = unidadOperada.calculoResultado(esDescarga, false, listaCotizaciones.get(j).getPrecio_cierre());
			}
			
			if(unidadOperada.getCantidad() > 1)
				resultadoCalculado = resultadoCalculado* unidadOperada.getCantidad();
			
			resultadoJuego = resultadoJuego + resultadoCalculado;
		}
		
		if(pUnidadOperada != null){

			//calculo resultadoMoney
			if(esCompra){
				resultadoCalculado = pUnidadOperada.calculoResultado(esDescarga, true, listaCotizaciones.get(j).getPrecio_cierre());
			}else{
				resultadoCalculado = pUnidadOperada.calculoResultado(esDescarga, false, listaCotizaciones.get(j).getPrecio_cierre());
			}
			
			if(pUnidadOperada.getCantidad() > 1)
				resultadoCalculado = resultadoCalculado* pUnidadOperada.getCantidad();
			
			resultadoMoney = resultadoMoney + resultadoCalculado;
						
			System.out.println(
				MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
				mensaje +
				"DESCARGANDO (Fecha:" + MetodosFechas.Date_to_String(pUnidadOperada.getFecha()) + 
				"- P_Origen:" + df.format(pUnidadOperada.getPrecioOriginal()) + ")" +
				"- Rescatada:" +  pUnidadOperada.estaRescatadaSemilla() + 
				"- Cant:" +  pUnidadOperada.getCantidad() + 
				"- RMoney:" + df.format(resultadoMoney) + 
				"- RJuego:" + df.format(resultadoJuego));
		}
		return (resultadoMoney + resultadoJuego);
	}

	/**
	 * 
	 */
	private void mostrarRendimientos() {
		
		if(REA_PropertiesController.SIN_RENDIMIENTOS != null){
			if(!Boolean.valueOf(REA_PropertiesController.SIN_RENDIMIENTOS).booleanValue())
				return;
		}
			
		System.out.println("");
		System.out.println("Rendimientos %");
		for(Rendimiento vRendimiento: listaRendimientos){
			System.out.println(vRendimiento.toString());
		}
		System.out.println("");
	}

	/**
	 * 
	 * @param resumen
	 */
	private void mostrarResultadosEstadisticos(Resumen resumen) {

		//Shorts
		double montoOperacionesPositivas_S = 0;
		double montoOperacionesNegativas_S = 0;
		
		boolean isBono = MetodosDeLogica.validarEsBono(nombrePapel);
		if(!isBono){
			for (Operacion vOperacion : lista_resultados_operaciones){
				if (vOperacion.isShort()){
					//System.out.println(MetodosFechas.Date_to_String(vOperacion.getFecha()) + ": "+ df.format(vOperacion.getResultado()));
					if (vOperacion.getResultado() > 0){
						montoOperacionesPositivas_S += vOperacion.getResultado();
					} else {
						montoOperacionesNegativas_S += (vOperacion.getResultado()*-1);	
					}
				}
			}
		}
		
		//Compras
		double montoOperacionesPositivas_C = 0;
		double montoOperacionesNegativas_C = 0;
		for (Operacion vOperacion : lista_resultados_operaciones){
			if (!vOperacion.isShort()){
				//System.out.println(MetodosFechas.Date_to_String(vOperacion.getFecha()) + ": "+ df.format(vOperacion.getResultado()));
				if (vOperacion.getResultado() > 0){
					montoOperacionesPositivas_C += vOperacion.getResultado();
				} else {
					montoOperacionesNegativas_C += (vOperacion.getResultado()*-1);	
				}
			}
		}
	
		double totales = 
				        (montoOperacionesPositivas_S - montoOperacionesNegativas_S )+			
						(montoOperacionesPositivas_C - montoOperacionesNegativas_C);
				
		double totalesParaPorcentaje = 
		        montoOperacionesPositivas_S + montoOperacionesNegativas_S +			
				montoOperacionesPositivas_C + montoOperacionesNegativas_C;

		
		double aciertosTotales = (montoOperacionesPositivas_S + montoOperacionesPositivas_C)*100/totalesParaPorcentaje;
		double desaciertosTotales = (montoOperacionesNegativas_C + montoOperacionesNegativas_S)*-100/totalesParaPorcentaje;
			
		StringBuilder sb = new StringBuilder();
		sb.append("*************** ARS Resultados ****************"+"\n");
		sb.append("Compras++:" + df.format(montoOperacionesPositivas_C) +"\n");
		sb.append("Compras--:" + df.format(montoOperacionesNegativas_C) +"\n");
		
		if(!isBono){
			sb.append("******************************************"+"\n");
			sb.append("Short++:" + df.format(montoOperacionesPositivas_S) +"\n");
			sb.append("Short--:" + df.format(montoOperacionesNegativas_S) +"\n");
		}
			
		sb.append("************** % Beneficios *************" +"\n");
		sb.append("ResAcumulado:" + df.format(totales) +"\n");
		sb.append("% Aciertos:" + df.format(aciertosTotales) +"\n");
		sb.append("% NoAciertos:" + df.format(desaciertosTotales) +"\n");
			
		System.out.println(sb.toString());
		
		resumen.setAciertos(df.format(aciertosTotales));
		resumen.setResultadoTotal(df.format(totales));
		
	}

	/**
	 * @param longitudes_array
	 * @return
	 */
	private int calculoMinimos(int[] longitudes_array) {
		
		int aux = 10000;
		for(int j=0; j<longitudes_array.length; j++){
				
			if (longitudes_array[j] <= aux){
				aux = longitudes_array[j];
			}
		}
		//System.out.println("calculoMinimos: " + aux);
		return aux;
	}
	
	/**
	 * 
	 * @param listaCotizaciones
	 * @param j
	 */
	private void regenerarElliott(List <Cotizacion> listaCotizaciones, int j) {
		 if(vElliottData == null){
			if(MetodosDeLogica.aplica_Elliott(nombrePapel)){
				if(MetodosDeLogica.aplica_TrazeoFull())
					System.out.println(
						MetodosFechas.Date_to_String(listaCotizaciones.get(j).getFecha()) +
						"- ASC->>REGENERANDO ELLIOTT!!");
				 
				vElliottData = new ElliottData();
				buscando_E0_E1_E2(listaCotizaciones,j);
			}
		 }		
	}
	
	/**
	 * 
	 * @param listaCotizaciones
	 * @param j
	 */
	private void regenerarElliottForzado(List <Cotizacion> listaCotizaciones, int j) {
		vElliottData = null;
		regenerarElliott(listaCotizaciones,j);
	}


}

