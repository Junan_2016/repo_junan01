package logica;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class LimpiezaFileSimula {

	public static void limpiarArchivo(String rutaFileLimpiar){
		
		BufferedReader br = null;
		String line = "";
		String cadenaInicio = "{\"success\":true";
		StringBuffer sb = new StringBuffer();
				
		try {
			br = new BufferedReader(new FileReader(rutaFileLimpiar));
			int count = 0;

			boolean comiezoLectura = false;
			while ((line = br.readLine()) != null) {				
				
				if (!line.equals("")){

					if(line.contains(cadenaInicio) && count > 0){
						comiezoLectura = true;
					}
					
					if(comiezoLectura){
						int index_inicio = line.indexOf(cadenaInicio);
						String nuevaCadena = line.substring(index_inicio,line.length());
						sb.append(nuevaCadena);
					}
				}
				count++;
			}
			
			if(comiezoLectura){
				escribirFichero(rutaFileLimpiar, sb.toString());
				System.out.println("Nuevo archivo creado en:" + rutaFileLimpiar);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	private static void escribirFichero(String fichero_a_escribir, String contenido)
		    throws UnsupportedEncodingException, FileNotFoundException, IOException{
		
			//System.out.println("contenido: " + contenido);
		    OutputStream fout = new FileOutputStream(fichero_a_escribir);
		    OutputStreamWriter out = new OutputStreamWriter(fout, "UTF8");
	        out.write(contenido);
		    out.close();
		    fout.close();
	}

}
