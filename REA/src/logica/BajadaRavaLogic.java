package logica;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import pojo.Cotizacion;
import util.Validacion;
import control.REA_PropertiesController;



public class BajadaRavaLogic {
	
	static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
	/**
	 * Procesa HTML desde file
	 * @return
	 */
	public static List<Cotizacion> procesaHTML_from_file(String especie){
		
		File input = null;
		Document doc = null;
		List<Cotizacion> listaEspecies = null;
		String rutaArchivo = null;
				
		try {
				
			rutaArchivo = REA_PropertiesController.RUTA_CARPETA_BAJADAS_CSV + especie + "_" + formatter.format(new Date()) + ".html";
			input = new File(rutaArchivo);
			
			//if (!input.exists())
			bajarEspecies(especie);		
			
			doc = Jsoup.parse(input,null);
			listaEspecies = parseo_HTML(doc, rutaArchivo);	
			
			return listaEspecies;	
			
		} catch (IOException e) {
			System.err.println("Error al procesar el archivo: " + rutaArchivo);
			e.printStackTrace();
			return null;
			
		} catch (Exception e) {
			System.err.println("Error al parsear el archivo: " + rutaArchivo);
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * 
	 */
	private static void bajarEspecies(String especie) {
		
		final String urlTemplate = REA_PropertiesController.URL_DOWNLOAD_HTML;
		
		try {
			final String url = urlTemplate.replace("<TIPO_ESPECIE>", especie);

	 	    Document doc = Jsoup.connect(url).data("query", "Java")
	 	    		.userAgent("Mozilla").cookie("auth", "token")
	 	    		.timeout(3000).post();
 	    
	 	    guardarHTML(doc,especie);
			
		} catch (IOException e) {
			System.err.println("Error Properties URL_DOWNLOAD_HTML es nula");
			e.printStackTrace();
		}

	}

	/**
	 * Guardar web en HTML en ruta especificada
	 * @param doc
	 * @param especie
	 */
	private static void guardarHTML(Document doc, String especie) {
	   	
 	   String fileName = especie + "_" + formatter.format(new Date()) + ".html"; 
 	   String outputFile = REA_PropertiesController.RUTA_CARPETA_BAJADAS_CSV + fileName;
 	   
 	  try {
	 	   BufferedWriter htmlWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8"));
	 	   htmlWriter.write(doc.toString());
	 	   htmlWriter.flush();
	 	   htmlWriter.close();
	 	   
		} catch (IOException e) {
			System.err.println("Error Properties RUTA_CARPETA_OPCIONES es nula");
			e.printStackTrace();
		}

	}

	
	/**
	 * Metodo de parseo de HTML
	 * @param doc
	 * @return
	 */
	private static List<Cotizacion> parseo_HTML(Document doc, String rutaArchivo) throws Exception {
		
		List<Cotizacion> listaEspecies = new ArrayList<Cotizacion>();		
		Elements tablaEspecie = doc.getElementsByClass("tablapanel");		
		Elements listaTR = tablaEspecie.get(0).getElementsByTag("tr");
		
		if (listaTR != null){
			
			listaTR.remove(0);			
			for (Element tr : listaTR) {
				
				Elements listaTD = tr.getElementsByTag("td");
				Cotizacion vEspecie = new Cotizacion();
				
				if (!listaTD.get(0).text().equalsIgnoreCase("")) 
					vEspecie.setNombrePapel(listaTD.get(0).text());
				
				if (!listaTD.get(1).text().equalsIgnoreCase("") && Validacion.esIdentificadorValido(listaTD.get(1).text()))
					vEspecie.setPrecio_cierre(Double.valueOf(listaTD.get(1).text().replace(".","").replace(",",".")));
				
				if (!listaTD.get(4).text().equalsIgnoreCase("") && Validacion.esIdentificadorValido(listaTD.get(4).text())) 
					vEspecie.setPrecio_apertura(Double.valueOf(listaTD.get(4).text().replace(".","").replace(",",".")));
				
				if (!listaTD.get(5).text().equalsIgnoreCase("") && Validacion.esIdentificadorValido(listaTD.get(5).text())) 
					vEspecie.setPrecio_minimo(Double.valueOf(listaTD.get(5).text().replace(".","").replace(",",".")));
				
				if (!listaTD.get(6).text().equalsIgnoreCase("") && Validacion.esIdentificadorValido(listaTD.get(6).text())) 
					vEspecie.setPrecio_maximo(Double.valueOf(listaTD.get(6).text().replace(".","").replace(",",".")));
				
				if (!listaTD.get(7).text().equalsIgnoreCase("")) 
					vEspecie.setFecha(new Date());
				
				if (!listaTD.get(8).text().equalsIgnoreCase("") && Validacion.esIdentificadorValido(listaTD.get(8).text())) 
					vEspecie.setVolumen(Double.valueOf(listaTD.get(8).text().replace(".","").replace(",",".")));

				listaEspecies.add(vEspecie);
			}
		}		
		return listaEspecies;
	}


}
