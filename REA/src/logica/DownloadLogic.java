package logica;

import java.text.SimpleDateFormat;
import java.util.Date;

import control.Posicion_PropertiesController;
import control.REA_PropertiesController;
import util.ParseadorProperties;

public class DownloadLogic {

	private static String propertiesSimbolos = null;//JFM ver refactorizar
	
	private static String [] simbolos = null;
	private static String [] simbolosYH = null;
	private static String url = null; 
	private static String urlTemplate = null; 
	
	
	public static String[] obtenerSimbolos() {
		propertiesSimbolos = Posicion_PropertiesController.SIMBOLOS;
		if (propertiesSimbolos !=  null){
			simbolos = ParseadorProperties.parsearPropeties(propertiesSimbolos);
			return simbolos;
		}else{
			System.out.println("Properties SIMBOLOS es nula");
		}
		return null;
	}
	
	public static String[] obtenerSimbolos_YH() {
		
		if(simbolosYH == null){
			if (Posicion_PropertiesController.SIMBOLOS_YH !=  null){
				simbolosYH = ParseadorProperties.parsearPropeties(Posicion_PropertiesController.SIMBOLOS_YH);
				return simbolosYH;
			}else{
				System.out.println("Properties simbolosYH es nula");
				return null;
			}
		}
		
		if(simbolosYH.length == 0)
			simbolosYH = null;
		
		return simbolosYH;
	}
	
	


	public static String obtenerURLSimbolo(String simbolo) {
		
		urlTemplate = REA_PropertiesController.URL_DOWNLOAD_CSV;
		
		if (urlTemplate != null){
			
			url = urlTemplate.replace("<SIMBOLO>", simbolo);
			
		}else{
			
			System.out.println("Properties URL_DOWNLOAD_CSV es nula");
		}		
		
		return url;
	}
	
	public static String obtenerURLSimboloYH(String simbolo) {
		
		urlTemplate = REA_PropertiesController.URL_DOWNLOAD_YH_CSV;
		
		if (urlTemplate != null){
			
			url = urlTemplate.replace("<SIMBOLO>", simbolo);
			
		}else{
			
			System.out.println("Properties URL_DOWNLOAD_YH_CSV es nula");
		}		
		
		return url;
	}


	public static String obtenerNombreArchivo(String simbolo) {
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		return "hist_" + simbolo + "_" + formatter.format(new Date()) + ".csv";
	}
	
	
}
