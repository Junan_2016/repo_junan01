package logica;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonReaderFactory;
import javax.json.JsonValue;

import pojo.Cotizacion;
import util.ParseadorProperties;

public class BajadaLogic {
	
	/**
	 * 
	 * @param rutaArchivo
	 * @return
	 * @throws IOException 
	 */
	public static Cotizacion procesaCotizacionHTML_from_file(final String rutaArchivo){
		
		LimpiezaFileSimula.limpiarArchivo(rutaArchivo);
		
		InputStream fis = null;
		try {
				fis = new FileInputStream(rutaArchivo);
		} catch (FileNotFoundException e) {
			System.err.println("No existe el archivo: " + rutaArchivo);
			return null;
		}
		       
		JsonReaderFactory factory = Json.createReaderFactory(null);
		JsonReader jsonReader = factory.createReader(fis);
		JsonObject jsonObject = null;
		try {
				jsonObject = jsonReader.readObject();
				
		} catch (Exception e) {
			System.err.println("Eliminar cabeceras html en el archivo: " + rutaArchivo);
			return null;
		}
		
		try {
			jsonReader.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
        //reading arrays from json
        JsonArray price_history_array = jsonObject.getJsonArray("price_history");
        String aperturaCotizacion = price_history_array.get(price_history_array.size()-1).toString();
        
        Cotizacion vCotizacion = new Cotizacion();
		vCotizacion.setFecha(Calendar.getInstance().getTime());
		
		vCotizacion.setPrecio_cierre(ParseadorProperties.parsearStringIOL_Double(jsonObject.getString("last"),"Precio"));
		vCotizacion.setPrecio_apertura(ParseadorProperties.parsearCadenaCotizacionIOL_Double(aperturaCotizacion,"Apertura"));
		vCotizacion.setPrecio_maximo(ParseadorProperties.parsearStringIOL_Double(jsonObject.getString("maxDia"),"Max"));
		vCotizacion.setPrecio_minimo(ParseadorProperties.parsearStringIOL_Double(jsonObject.getString("minDia"),"Min"));
		vCotizacion.setVolumen(ParseadorProperties.parsearStringIOL_Double(jsonObject.getString("volumen"),"Volumen"));
        
        return vCotizacion;
	}
	

}
