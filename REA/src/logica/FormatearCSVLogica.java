package logica;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import control.REA_PropertiesController;
import pojo.CotizacionFormateada;

public class FormatearCSVLogica {

	public static List<CotizacionFormateada> formatCSVforMeta() {
		
		String csvFile = REA_PropertiesController.RUTA_ARCHIVO_BAJADA_CSV_MT;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		
		List<CotizacionFormateada> cotizaciones = null;
		
		try {
			
			br = new BufferedReader(new FileReader(csvFile));
			
			cotizaciones = new ArrayList<CotizacionFormateada>();
			
			int band = 0;
						
			while ((line = br.readLine()) != null) {				
			    //use comma as separator
				String[] datoslinea = line.split(cvsSplitBy);
				
				band++;
				if (band != 1){
					
					//array.add(datoslinea);					

					CotizacionFormateada cotizacionFormateada = CotizacionFormateadaParser.parseEntry(datoslinea);					
					cotizaciones.add(cotizacionFormateada);

				}
			}
			
			final String rutaArchivoCreado = REA_PropertiesController.RUTA_ARCHIVO_BAJADA_CSV_MT + "/NuevoArchivoMT.csv";
			escribirFicheroCsv(rutaArchivoCreado, cotizaciones);
			
			System.out.println("Nuevo archivo CSV creado en:" + rutaArchivoCreado);
						
			/*
			for (Cotizacion cotizacion : cotizaciones) {					
				System.out.println(cotizacion);
		    }
		    */
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
				
/*		
		System.out.println("size: " + cotizaciones.size());
		System.out.println("Done");
*/
		return cotizaciones;
	}
	
	
	public static void escribirFicheroCsv(String fichero_a_escribir, List<CotizacionFormateada> cotizaciones)
		    throws UnsupportedEncodingException, FileNotFoundException, IOException{
		
		    OutputStream fout = new FileOutputStream(fichero_a_escribir);

		    //para ficheros con símbolos propios del español,
		    //utilizar la codificación "ISO-8859-1"
		    OutputStreamWriter out = new OutputStreamWriter(fout, "UTF8");

	        for(CotizacionFormateada cotizacion: cotizaciones){
		            out.write(cotizacion.getFecha() + ",");
		            out.write(cotizacion.getHora() + ",");
		            out.write(String.valueOf(cotizacion.getPrecio_apertura()) + ",");
		            out.write(String.valueOf(cotizacion.getPrecio_maximo()) + ",");
		            out.write(String.valueOf(cotizacion.getPrecio_minimo()) + ",");
		            out.write(String.valueOf(cotizacion.getPrecio_cierre()) + ",");		            
		            out.write(String.valueOf(cotizacion.getVolumen()));
		            out.write("\n");
		    }
		    out.close();
		    fout.close();
	}

}
