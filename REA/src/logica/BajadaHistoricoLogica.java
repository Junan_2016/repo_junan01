package logica;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import pojo.Cotizacion;


public class BajadaHistoricoLogica {
		
	public static List<Cotizacion> convertCSVtoList(String rutaArchivo){
		
		String csvFile = rutaArchivo;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		
		List<Cotizacion> cotizaciones = null;
		
		try {
			
			br = new BufferedReader(new FileReader(csvFile));
			
			cotizaciones = new ArrayList<Cotizacion>();
			
			int band = 0;
			while ((line = br.readLine()) != null) {				
			    //use comma as separator
				String[] datoslinea = line.split(cvsSplitBy);
				band++;
				if (band != 1){
					Cotizacion cotizacion = CotizacionParser.parseEntry(datoslinea);					
					cotizaciones.add(cotizacion);
				}
			}
			
/*			for (Cotizacion cotizacion : cotizaciones) {					
				System.out.println(cotizacion.toString());
		    }
*/
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
				
/*		System.out.println("size: " + cotizaciones.size());
		System.out.println("Done");
*/
		return cotizaciones;
	}

}
