package logica;

import pojo.Cotizacion;
import util.MetodosFechas;

public class ElliottData {

	private Cotizacion E0;
	private Cotizacion E1;
	private Cotizacion E2;
	private Cotizacion E3;
	private Cotizacion E4;
	private Cotizacion E5;

	private boolean habilitaCarga_E1;
	private boolean habilitaCarga_E2;
	private boolean habilitaCarga_E3;
	private boolean habilitaCarga_E4;
	private boolean habilitaCarga_E5;
	
	public int countE2 = 0;
	public int countE4 = 0;
		
	public Cotizacion getE0() {
		return E0;
	}
	public void setE0(Cotizacion e0) {
		E0 = e0;
	}
	public Cotizacion getE1() {
		return E1;
	}
	public void setE1(Cotizacion e1) {
		E1 = e1;
	}
	public Cotizacion getE2() {
		return E2;
	}
	public void setE2(Cotizacion e2) {
		E2 = e2;
	}
	public Cotizacion getE3() {
		return E3;
	}
	public void setE3(Cotizacion e3) {
		E3 = e3;
	}
	public Cotizacion getE4() {
		return E4;
	}
	public void setE4(Cotizacion e4) {
		E4 = e4;
	}
	public Cotizacion getE5() {
		return E5;
	}
	public void setE5(Cotizacion e5) {
		E5 = e5;
	}
			
    public boolean isHabilitaCarga_E1() {
		return habilitaCarga_E1;
	}
	public void setHabilitaCarga_E1(boolean habilitaCarga_E1) {
		this.habilitaCarga_E1 = habilitaCarga_E1;
	}
	public boolean isHabilitaCarga_E2() {
		return habilitaCarga_E2;
	}
	public void setHabilitaCarga_E2(boolean habilitaCarga_E2) {
		this.habilitaCarga_E2 = habilitaCarga_E2;
	}
		
	public boolean isHabilitaCarga_E3() {
		return habilitaCarga_E3;
	}
	public void setHabilitaCarga_E3(boolean habilitaCarga_E3) {
		this.habilitaCarga_E3 = habilitaCarga_E3;
	}
	public boolean isHabilitaCarga_E4() {
		return habilitaCarga_E4;
	}
	public boolean isHabilitaCarga_E5() {
		return habilitaCarga_E5;
	}
	public void setHabilitaCarga_E5(boolean habilitaCarga_E5) {
		this.habilitaCarga_E5 = habilitaCarga_E5;
	}
	public void setHabilitaCarga_E4(boolean habilitaCarga_E4) {
		this.habilitaCarga_E4 = habilitaCarga_E4;
	}
		
	public boolean es_valido_carga_E1_E2(){

    	if(habilitaCarga_E1 || habilitaCarga_E2)
    		return true;

    	return false;
    }
	
	public boolean es_valido_carga_E3_E4(){

    	if(habilitaCarga_E3 || habilitaCarga_E4)
    		return true;
    			
    	//System.out.println("E3/E4 deshabilitados para carga");
    	return false;
    }
	
	public boolean es_valido_carga_E5(){

    	if(habilitaCarga_E5)
    		return true;

    	return false;
    }
   		
	public void mostrarValoresElliott(Cotizacion pCotizacion, String mensaje) {
		
		if(MetodosDeLogica.aplica_TrazeoFull() || pCotizacion == null){
			
			String fecha = "ELLIOTT: ";
			if(pCotizacion != null && pCotizacion.getFecha() != null){
				fecha = MetodosFechas.Date_to_String(pCotizacion.getFecha()) + "- ASC->>";
			}
			
			StringBuffer sb = new StringBuffer();
			sb.append(fecha);
			if(getE0() != null) sb.append("E0:" + getE0().trazaElliott(false) + "- ");
			if(getE1() != null)	sb.append("E1:" + getE1().trazaElliott(true) + "- ");
			if(getE2() != null)	sb.append("E2:" + getE2().trazaElliott(false) + "- ");
			if(getE3() != null)	sb.append("E3:" + getE3().trazaElliott(true) + "- ");
			if(getE4() != null)	sb.append("E4:" + getE4().trazaElliott(false) + "- ");
			if(getE5() != null)	sb.append("E5:" + getE5().trazaElliott(true) + "- ");
			if(mensaje != null) sb.append(mensaje);
			System.out.println(sb.toString());
		}
	}
}
