package logica;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pojo.CalculoMetrica;
import pojo.Cotizacion;
import pojo.UnidadOperada;
import util.MetodosFechas;
import control.Eventos_PropertiesController;
import control.REA_PropertiesController;

public class MetodosDeLogica {

	private static String[] girosAutorizados = null;
	private static String[] bonosConocidos = null;
			
	private static Map<String,String> MapEventos = null;
	private static Map<String,String> MapPapelesSector = null;
	
	private static Map<String,Cotizacion> MapCotizacionesHardcode = null;
	private static Map<String,Double> Map_DELTA_PORCENTAJE_PRECIO = null;
	
	private static String [] papeles_no_aplican_Elliot_array = null;
	private static String [] RUTA_ATR_array = null;

	private static Map<String,Double> MapPorcenjePeligroInvisble = null;
	final private static double porcentajeGananciaDefault = 10;
	private static double mesesPermitidosElliott = 0;
	
	private static Map <String,Double> DESVIOS_MAXIMOS = new HashMap <String,Double>();

	public static boolean validar48hsPreEvento(List<UnidadOperada> listaUnidadesOperadas, Date fechaCotizacion){
		
		//#1-busca cache
		if (MapEventos == null){
			if (Eventos_PropertiesController.EVENTOS != null){
				MapEventos = new HashMap<String,String>();
				final String[] arrayCadena = Eventos_PropertiesController.EVENTOS.split("-");
				
				for(int k=0; k<arrayCadena.length; k++){
					final String cadena = arrayCadena[k];
					final String fechaEvento = cadena.split(":")[0];
					final String nombreEvento = cadena.split(":")[1];
					MapEventos.put(fechaEvento, nombreEvento);
				}
			};				
		}
		
		//#2-logica 
		Calendar calendarCotizacion = Calendar.getInstance();
		calendarCotizacion.setTimeInMillis(fechaCotizacion.getTime());
		calendarCotizacion.add(Calendar.DATE, 2);

		String fecha_D1 = MetodosFechas.Date_to_String(calendarCotizacion);
		fecha_D1 = fecha_D1.replace("-", "");	
		
		if(MapEventos != null && MapEventos.get(fecha_D1) != null){
			System.out.println( 
					MetodosFechas.Date_to_String(fechaCotizacion) +
					"- ASC->>" +
					"Eventos: " + MetodosFechas.Date_to_String(calendarCotizacion) + 
					"- " + MapEventos.get(fecha_D1) +
					"- Unid:" + listaUnidadesOperadas.size()
			);
			return true;
		}
 
		//Accion preventiva - 48hs previas
		calendarCotizacion.add(Calendar.DATE, 1);

		String fecha_D2 = MetodosFechas.Date_to_String(calendarCotizacion);
		fecha_D2 = fecha_D2.replace("-", "");	
		
		if(MapEventos != null && MapEventos.get(fecha_D2) != null){
			System.out.println(
					MetodosFechas.Date_to_String(fechaCotizacion) +
					"- ASC->>" +
					"Eventos: " + MetodosFechas.Date_to_String(calendarCotizacion) + 
					"- " + MapEventos.get(fecha_D2) +
					"- Unid:" + listaUnidadesOperadas.size()
			);
			return true;
		}
		
		return false;
	}
	
    public static String semaforoDiaPreEvento(List<UnidadOperada> listaUnidadesOperadas, Date fechaCotizacion, String mensaje_ATR_pMax, double STOCH_DELTA_ANTERIOR,  double STOCH_DELTA){
		
    	String mensaje = "WARNING";
    	if (mensaje_ATR_pMax.contains("(A)") && STOCH_DELTA_ANTERIOR >= STOCH_DELTA){
    		 mensaje = "DANGER !!!";
    	}
    	
		//#1-busca cache
		if (MapEventos == null){
			if (Eventos_PropertiesController.EVENTOS != null){
				MapEventos = new HashMap<String,String>();
				final String[] arrayCadena = Eventos_PropertiesController.EVENTOS.split("-");
				
				for(int k=0; k<arrayCadena.length; k++){
					final String cadena = arrayCadena[k];
					final String fechaEvento = cadena.split(":")[0];
					final String nombreEvento = cadena.split(":")[1];
					MapEventos.put(fechaEvento, nombreEvento);
				}
			};				
		}
		
		//Warning dia evento 
		Calendar calendarCotizacion = Calendar.getInstance();
		calendarCotizacion.setTimeInMillis(fechaCotizacion.getTime());
		calendarCotizacion.add(Calendar.DATE, 2);
		
		String fecha_D2 = MetodosFechas.Date_to_String(calendarCotizacion);
		fecha_D2 = fecha_D2.replace("-", "");	
		if(MapEventos != null && MapEventos.get(fecha_D2) != null){
			System.out.println(
					 MetodosFechas.Date_to_String(fechaCotizacion) +
					 "- ASC->>" +
					 mensaje + " de Evento en 24hs: " +
						       MetodosFechas.Date_to_String(calendarCotizacion) + 
						       "- " + MapEventos.get(fecha_D2) +
						       "- Unid:" + listaUnidadesOperadas.size()
			);
		}

		calendarCotizacion.add(Calendar.DATE, 1);
		String fecha_D3 = MetodosFechas.Date_to_String(calendarCotizacion);
		fecha_D3 = fecha_D3.replace("-", "");	
		if(MapEventos != null && MapEventos.get(fecha_D3) != null){
			System.out.println(
					   MetodosFechas.Date_to_String(fechaCotizacion) +
					   "- ASC->>" +
  					   mensaje +" de Evento en 48hs: " +
				       MetodosFechas.Date_to_String(calendarCotizacion) +
				       "- " + MapEventos.get(fecha_D3)+
				       "- Unid:" + listaUnidadesOperadas.size()
			);
		}

		calendarCotizacion.add(Calendar.DATE, 1);
		String fecha_D4 = MetodosFechas.Date_to_String(calendarCotizacion);
		fecha_D4 = fecha_D4.replace("-", "");	
		if(MapEventos != null && MapEventos.get(fecha_D4) != null){
			System.out.println(
					   MetodosFechas.Date_to_String(fechaCotizacion) +
					   "- ASC->>" +
					   mensaje +" de Evento en 72hs: " + 
				       MetodosFechas.Date_to_String(calendarCotizacion) + 
				       "- " + MapEventos.get(fecha_D4) +
				       "- Unid:" + listaUnidadesOperadas.size()
			);
		}
		
		calendarCotizacion.add(Calendar.DATE, 1);
		String fecha_D5 = MetodosFechas.Date_to_String(calendarCotizacion);
		fecha_D5 = fecha_D5.replace("-", "");	
		if(MapEventos != null && MapEventos.get(fecha_D5) != null){
			System.out.println(
					   MetodosFechas.Date_to_String(fechaCotizacion) +
					   "- ASC->>" +
					   mensaje +" de Evento en 96hs: " +
					   MetodosFechas.Date_to_String(calendarCotizacion) + 
					   "- " + MapEventos.get(fecha_D5)+
					   "- Unid:" + listaUnidadesOperadas.size()
			);
		}
		return mensaje;
	}
	
	public static boolean validarGiros_papel(String nombrePapel) {

		//#1-busca cache
		if (girosAutorizados == null){
			if (REA_PropertiesController.GIROS_AUTORIZADOS != null){
				girosAutorizados = control.REA_PropertiesController.GIROS_AUTORIZADOS.split(",");
			}
		}
		
		//#2-logica
		if (girosAutorizados != null){
			for(String papel: girosAutorizados){
				if(nombrePapel.contains(papel))
					return true;
			}
		}
		return false;
	}
	
	public static boolean validarEsBono(String nombrePapel) {
		
		//#1-busca cache
		if (bonosConocidos == null){
			if (REA_PropertiesController.BONETES != null){
				bonosConocidos = REA_PropertiesController.BONETES.split(",");
			}
		}
		
		//#2-logica
		if(bonosConocidos != null){
			for(String bono: bonosConocidos){
				if(nombrePapel.contains(bono))
					return true;
			}
		}
		return false;
	}
	
	
	public static boolean validar_fecha_ejercicio_papel(String fecha) {
		
		if(fecha == null || REA_PropertiesController.CORTAR_FECHAS_EJERCICIOS == null)
			return false;

		try{
			//2015-12-03
			int mes = Integer.valueOf(fecha.substring(5,7));
			int dia = Integer.valueOf(fecha.substring(8,10));
			int diaEjercicio = 15;
			
			//mes par
			if(mes%2 == 0 && (diaEjercicio+1 <= dia) && (dia-diaEjercicio <= 2)){
				if("0".equals(REA_PropertiesController.CORTAR_FECHAS_EJERCICIOS)){
					System.out.println("*** CIERRE DEL EJERCICIO");
					return false;
				}
				return true;
			}
		}catch (Exception e){
			System.err.println("Error en validar_fecha_ejercicio_papel");
		}
		return false;
	}

	public static String semaforoPeligroInvisible(
			List<CalculoMetrica> listaCalculoMetrica_MMT_15,
			List<Cotizacion> listaCotizaciones, 
			int j, 
			double balanceAcumulado,
			double resultadoJuego, 
			double porcentaje,
			String estadoATR,
			String papel) 
	{
		
		if(porcentaje < retornar_PorcentajePeligroInvisible(papel)){
			return null;
		}
   		
		//evita porcentajes redefinidos
		if(retornar_PorcentajePeligroInvisible(papel) == porcentajeGananciaDefault){
			//Por default ATR(B) no valida
		    if (!estadoATR.contains("(A)")) return null;
		}
		
		if(balanceAcumulado >= 0 || resultadoJuego >=0){
			
			boolean REGLA_MMT_DESC_3DIAS = listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor() > listaCalculoMetrica_MMT_15.get(j).getIndicador_valor();
			boolean REGLA_MMT_DESC_5DIAS = listaCalculoMetrica_MMT_15.get(j-5).getIndicador_valor() > listaCalculoMetrica_MMT_15.get(j).getIndicador_valor();

			final String mensaje = "MMT:" + listaCalculoMetrica_MMT_15.get(j).getIndicador_valor_formateado() + 
					   " -MMT_03:" + listaCalculoMetrica_MMT_15.get(j-3).getIndicador_valor_formateado() +
					   " -MMT_05:" + listaCalculoMetrica_MMT_15.get(j-5).getIndicador_valor_formateado();
												
			if(REGLA_MMT_DESC_3DIAS && REGLA_MMT_DESC_5DIAS){
				boolean REGLA_MMT_ASC_24HS = listaCalculoMetrica_MMT_15.get(j-1).getIndicador_valor() <= listaCalculoMetrica_MMT_15.get(j).getIndicador_valor();				
				
				if(!REGLA_MMT_ASC_24HS && porcentaje > retornar_PorcentajePeligroInvisible(papel)){
					return mensaje;
				}
		    }
	    }
		return null;
	}
	

	
	public static double retornar_PorcentajePeligroInvisible(String papel){
		//carga
		if(MapPorcenjePeligroInvisble == null){
			MapPorcenjePeligroInvisble = new HashMap<String, Double>();
			if(REA_PropertiesController.PORCENTAJE_PELIGRO_INVISIBLE != null){
				String[] cadenaGeneral = REA_PropertiesController.PORCENTAJE_PELIGRO_INVISIBLE.split("-");
				for(String cadena: cadenaGeneral){
					final String simboloPapel = cadena.split(":")[0];
					final String value = cadena.split(":")[1];
					MapPorcenjePeligroInvisble.put(simboloPapel,Double.valueOf(value));		
				}
			}	
		}
	
		if (MapPorcenjePeligroInvisble.get(papel)!= null){
			return MapPorcenjePeligroInvisble.get(papel).intValue();
		}
		
		return porcentajeGananciaDefault;		
	}
	
	public static boolean aplica_Elliott(String papel){
		if (papeles_no_aplican_Elliot_array == null){
			if (REA_PropertiesController.NO_APLICA_ELLIOTT != null){
				//NO_APLICA_ELLIOTT=AY24,AA17
				papeles_no_aplican_Elliot_array = REA_PropertiesController.NO_APLICA_ELLIOTT.split(",");
			}else{
				papeles_no_aplican_Elliot_array = new String [1];
				papeles_no_aplican_Elliot_array[0] = "-";
			}	
		}
		
		for(int m=0; m<papeles_no_aplican_Elliot_array.length; m++){
			if(papeles_no_aplican_Elliot_array[m].equalsIgnoreCase(papel)){
				return false;
			}
		}
		return true;
	}
	
	public static boolean aplica_TrazeoFull(){
		if (REA_PropertiesController.TRAZEO_FULL == null) return true;
			
		if("1".equalsIgnoreCase(REA_PropertiesController.TRAZEO_FULL)){
			return true;
		} 
		return false;
	}
		
	private static Map <String,Cotizacion> retornaCotizacionesHardcode(){
		
		if (MapCotizacionesHardcode == null){
			if (Eventos_PropertiesController.COTIZACIONES_HARDCODE != null){
				MapCotizacionesHardcode = new HashMap<String,Cotizacion>();

				final String[] arrayPapeles = Eventos_PropertiesController.COTIZACIONES_HARDCODE.split("==");
				
				if(aplica_TrazeoFull()) System.out.println("*** COTIZACIONES_HARDCODE");
				
				for(int k=0; k<arrayPapeles.length; k++){
					//APBR:2015-12-04,37.90,37.90,36.35,36.60,487768,0--xxxxx
					final String caramelo = arrayPapeles[k];
					final String nombre = caramelo.split(":")[0];
					if(aplica_TrazeoFull()) System.out.println("Papel: " + nombre);
					final String cotizacionesPapel = caramelo.split(":")[1];

					int count = 0;
					final String[] cotizacionesPapelArray = cotizacionesPapel.split("--");
					for(int m=0; m<cotizacionesPapelArray.length; m++){
						final String cotizacion = cotizacionesPapelArray[m];
						final double precio_apertura = Double.parseDouble(cotizacion.split(",")[1]);
						final double precio_maximo = Double.parseDouble(cotizacion.split(",")[2]);
						final double precio_minimo = Double.parseDouble(cotizacion.split(",")[3]);
						final double precio_cierre = Double.parseDouble(cotizacion.split(",")[4]);
						final double volumen = Double.parseDouble(cotizacion.split(",")[5]);
						
						final String fechaString = cotizacion.split(",")[0];
						final Calendar fecha = MetodosFechas.crearFechaString(fechaString.split("-"));
												
						Cotizacion vCotizacion = new Cotizacion(
								nombre,
								fecha.getTime(), 
								precio_apertura,
								precio_maximo, 
								precio_minimo,
								precio_cierre,
								volumen
						);
						if(aplica_TrazeoFull()) System.out.println(vCotizacion);
						MapCotizacionesHardcode.put(nombre + "_" + count++, vCotizacion);
					}
				}
			}				
		}
		return MapCotizacionesHardcode;
	}
	
	public static List <Cotizacion> buscaCotizacionHardcode(String nombrePapel){
		Map <String,Cotizacion> mapa = retornaCotizacionesHardcode();
		List <Cotizacion> cotizaciones = new ArrayList<Cotizacion>();
		
		if(mapa == null || mapa.size() == 0) return null;
		
		Cotizacion vCotizacion = null; 
		for(int j=0; j<mapa.size(); j++){
			vCotizacion = mapa.get(nombrePapel + "_" + j);
			
			if(vCotizacion != null) {
				cotizaciones.add(vCotizacion);
			}
		}
		
		return cotizaciones;
	}

	/**
	 * Metodo para conocer si es fecha vencimiento
	 * @param cotizacion
	 * @return
	 */
	public static boolean esFechaVencimiento(Cotizacion cotizacion){
		
		//JFM if(true)return true;
		
		int[] datosFecha = MetodosFechas.extraeMesFecha(cotizacion.getFecha());
		
		final int mes = datosFecha[1];
		if((mes%2)==0){
			final int dia = datosFecha[2];
			if(dia<20){
				return true;	
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param cotizacion
	 * @param vElliottData
	 * @return
	 */
    public static boolean estaElliottTimeout(Cotizacion cotizacion, ElliottData vElliottData){

    	String fechaStringCorteDebug =null;//String fechaStringCorteDebug = "2015-06-22";
		if(MetodosFechas.Date_to_String(cotizacion.getFecha()).equals(fechaStringCorteDebug)){
			System.out.println("");
    	}
    	
		if(vElliottData == null) return true;
		
		final int diasLaborablesMes = 21;
		final int diasNoLaborablesMes = 8;
		final double mesesPermitidosElliott = getMesesPermitidosElliott();
		
		if(vElliottData.es_valido_carga_E1_E2()){

			final int cantDiasBruto_E1 = MetodosFechas.calculaDias(vElliottData.getE1().getFecha(), cotizacion.getFecha());
			double cantDiasNeto_E1 = cantDiasBruto_E1 - (diasNoLaborablesMes*mesesPermitidosElliott); 
			
			if(cantDiasNeto_E1 > diasLaborablesMes*mesesPermitidosElliott){
				if(MetodosDeLogica.aplica_TrazeoFull())
					System.out.println(
						MetodosFechas.Date_to_String(cotizacion.getFecha()) +
						"- ASC->>TIMEOUT ELLIOTT E1: " + cantDiasNeto_E1);
				
				return true;
			}
		}
		
		if(vElliottData.es_valido_carga_E3_E4()){

			final int cantDiasBruto_E3 = MetodosFechas.calculaDias(vElliottData.getE3().getFecha(), cotizacion.getFecha());
			double cantDiasNeto_E3 = cantDiasBruto_E3 - (diasNoLaborablesMes*mesesPermitidosElliott); 
			
			if(cantDiasNeto_E3 > diasLaborablesMes*mesesPermitidosElliott){
				if(MetodosDeLogica.aplica_TrazeoFull())
					System.out.println(
						MetodosFechas.Date_to_String(cotizacion.getFecha()) +
						"- ASC->>TIMEOUT ELLIOTT E3: " + cantDiasNeto_E3);
				
				return true;
			}
		}
		
		if(vElliottData.es_valido_carga_E5()){

			final int cantDiasBruto_E5 = MetodosFechas.calculaDias(vElliottData.getE5().getFecha(), cotizacion.getFecha());
			double cantDiasNeto_E5 = cantDiasBruto_E5 - (diasNoLaborablesMes*mesesPermitidosElliott); 
			
			if(cantDiasNeto_E5 > diasLaborablesMes*mesesPermitidosElliott){
				if(MetodosDeLogica.aplica_TrazeoFull())
					System.out.println(
						MetodosFechas.Date_to_String(cotizacion.getFecha()) +
						"- ASC->>TIMEOUT ELLIOTT E5: " + cantDiasNeto_E5);
				
				return true;
			}
		}
		
    	return false;
	}

    /**
	 * @param mesesPermitidosElliott
	 * @return
	 */
	public static double getMesesPermitidosElliott() {
		
		if (mesesPermitidosElliott == 0){
			
			if(REA_PropertiesController.MESES_PERMITIDOS_ELLIOTT != null)
				mesesPermitidosElliott = Double.parseDouble(REA_PropertiesController.MESES_PERMITIDOS_ELLIOTT);
			else
				mesesPermitidosElliott = 1.5; // default
		}
		return mesesPermitidosElliott;
	}
	
	/**
	 * 
	 * @param listaUnidadesOperadas
	 * @return
	 */
	public static long calculaSizeUnidadesCargadas(List<UnidadOperada> listaUnidadesOperadas){
		
		if(listaUnidadesOperadas == null || listaUnidadesOperadas.size() == 0)
			return 0;	
		
		long cantidadUnidades = 0;
		for(UnidadOperada operada: listaUnidadesOperadas)
			cantidadUnidades = cantidadUnidades + operada.getCantidad();
		
		return cantidadUnidades;
	}
	

	public static boolean aplica_RutaATR(String papel){
		
		if (RUTA_ATR_array == null){
			if (REA_PropertiesController.RUTA_ATR != null){
				RUTA_ATR_array = REA_PropertiesController.RUTA_ATR.split("-");
			}else{
				RUTA_ATR_array = new String [1];
				RUTA_ATR_array[0] = "-";
			}	
		}
		
		for(int m=0; m<RUTA_ATR_array.length; m++){
			if(RUTA_ATR_array[m].equalsIgnoreCase(papel)){
				return true;
			}
		}
		return false;
	}
	
	
public static double get_DELTA_PORCENTAJE_PRECIO(String papel){
		
		//#1-busca cache 
		if (Map_DELTA_PORCENTAJE_PRECIO == null){
			if (REA_PropertiesController.DELTA_PORCENTAJE_PRECIO != null){
				Map_DELTA_PORCENTAJE_PRECIO = new HashMap<String,Double>();
				final String[] arrayCadena = REA_PropertiesController.DELTA_PORCENTAJE_PRECIO.split("-");
				
				for(int k=0; k<arrayCadena.length; k++){
					final String cadena = arrayCadena[k];
					final String papel_ = cadena.split(":")[0];
					final Double porcentaje = Double.parseDouble(cadena.split(":")[1]);
					Map_DELTA_PORCENTAJE_PRECIO.put(papel_, porcentaje);
				}
			};				
		}
		
		//valor por default
		if(Map_DELTA_PORCENTAJE_PRECIO == null || Map_DELTA_PORCENTAJE_PRECIO.get(papel) == null) return 1.00;
		
		return Map_DELTA_PORCENTAJE_PRECIO.get(papel).doubleValue();
		
	}

	public static double getDesviosMaximosAlcanzados(String papel) {
			
		final double desvio_default = 0.03;
		
		if (DESVIOS_MAXIMOS.get(papel)!= null){
			//System.out.println("DESVIO_MAXIMO de " + papel + ": " + DESVIOS_MAXIMOS.get(papel).doubleValue());
			return DESVIOS_MAXIMOS.get(papel).doubleValue();
		}
		
		//carga desde properties
		if(REA_PropertiesController.DESVIOS_MAXIMOS_ALCANZADOS != null){
			
			String[] cadenaMaximos = REA_PropertiesController.DESVIOS_MAXIMOS_ALCANZADOS.split("-");
			
			for(String cadena: cadenaMaximos){
				final String simboloPapel = cadena.split(":")[0];
				final String maximo = cadena.split(":")[1];
				DESVIOS_MAXIMOS.put(simboloPapel,Double.valueOf(maximo));		
			}
			
		} else {
			System.err.println("No se encuentra DESVIOS_MAXIMOS_ALCANZADOS - Default " + desvio_default);
		}
		
		if (DESVIOS_MAXIMOS.get(papel)!= null){
			//System.out.println("DESVIO_MAXIMO de " + papel + ": " + DESVIOS_MAXIMOS.get(papel).doubleValue());
			return DESVIOS_MAXIMOS.get(papel).doubleValue();
		}
	
		//System.out.println("DESVIO_MAXIMO DEFAULT de " + papel + ": " + desvio_default);
		
		return desvio_default;		
	}
	
	public static boolean aplica_cargaBloque(){
		if (REA_PropertiesController.NO_APLICA_CARGA_BLOQUE == null) return true;
			
		if("1".equalsIgnoreCase(REA_PropertiesController.NO_APLICA_CARGA_BLOQUE)){
			return false;
		} 
		return true;
	}
	
	public static boolean aplica_cargaMultiBloque(){
		if (REA_PropertiesController.NO_APLICA_CARGA_MULTI_BLOQUE == null) return true;
			
		if("1".equalsIgnoreCase(REA_PropertiesController.NO_APLICA_CARGA_MULTI_BLOQUE)){
			return false;
		} 
		return true;
	}
	
	/**
	 * 
	 * @return
	 */
	public static boolean aplica_RescateSemillas(){
		
		if(REA_PropertiesController.NO_RESCATE_SEMILLAS == null)
			return false;
	
		if ("0".equalsIgnoreCase(REA_PropertiesController.NO_RESCATE_SEMILLAS))
			return true;
			
		return false;
	}
	
	/**
	 * 
	 * @return
	 */
	public static double getPORCENTAJE_RESCATE(){
		
		if(REA_PropertiesController.PORCENTAJE_RESCATE == null)
			return 5; // valor default
			
		return Double.parseDouble(REA_PropertiesController.PORCENTAJE_RESCATE);
		
	}

	/**
	 * 
	 * @param nombrePapelPrincipal
	 * @return
	 */
	public static String getPapelSector(String nombrePapelPrincipal){
		if (MapPapelesSector == null){
			if (REA_PropertiesController.PAPELES_SECTOR != null){
				MapPapelesSector = new HashMap<String,String>();
				final String[] arrayCadena = REA_PropertiesController.PAPELES_SECTOR.split("-");
				
				for(int k=0; k<arrayCadena.length; k++){
					final String cadena = arrayCadena[k];
					final String principal = cadena.split("_")[0];
					final String asociado = cadena.split("_")[1];
					MapPapelesSector.put(principal, asociado);
					MapPapelesSector.put(asociado, principal);
				}
			};				
		}
		
		return MapPapelesSector.get(nombrePapelPrincipal);
	}
}


