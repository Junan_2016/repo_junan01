package pojo;

import java.text.DecimalFormat;
import java.util.Date;

import util.MetodosFechas;

public class UnidadOperada {
	
	final private static DecimalFormat df = new DecimalFormat("0.00");
	private Date fecha;
	private double precioOriginal;
	private Cotizacion vCotizacion;
	private long cantidad=1;
	private boolean estaRescatadaSemilla;
	private double resultadoRescate;
	private boolean fueRescateForzado;
	
	public UnidadOperada(Date fecha, double precioOriginal,Cotizacion pCotizacion) {
		super(); 
		this.fecha = fecha;
		this.precioOriginal = precioOriginal;	
		vCotizacion = pCotizacion;
	}
	
	public UnidadOperada(Date fecha, double precioOriginal,Cotizacion pCotizacion, long pCantidad) {
		super();
		this.fecha = fecha;
		this.precioOriginal = precioOriginal;
		this.cantidad = pCantidad;
		vCotizacion = pCotizacion;
	}
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public double getPrecioOriginal() {
		return precioOriginal;
	}
	public void setPrecioOriginal(double precioOriginal) {
		this.precioOriginal = precioOriginal;
	}
	public Cotizacion getvCotizacion() {
		return vCotizacion;
	}
	public void setvCotizacion(Cotizacion vCotizacion) {
		this.vCotizacion = vCotizacion;
	}
	public long getCantidad() {
		return cantidad;
	}
	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}
	
	public boolean estaRescatadaSemilla() {
		return estaRescatadaSemilla;
	}

	public void setEstaRescatadaSemilla(boolean estaRescatadaSemilla) {
		this.estaRescatadaSemilla = estaRescatadaSemilla;
	}
	
	public double getResultadoRescate() {
		return resultadoRescate;
	}

	public void setResultadoRescate(double resultadoRescate) {
		this.resultadoRescate = resultadoRescate;
	}
	
	public boolean fueRescateForzado() {
		return fueRescateForzado;
	}

	public void setRescateForzado(boolean rescateForzado) {
		this.fueRescateForzado = rescateForzado;
	}

	@Override
	public String toString() {
		
		if(!estaRescatadaSemilla)
			return "fecha=" +  MetodosFechas.Date_to_String(fecha) + ", P_Origen=" + df.format(precioOriginal);		
		
		return "fecha=" +  MetodosFechas.Date_to_String(fecha) + 
				", P_Origen=" + df.format(precioOriginal) + 
				", rescate=" + df.format(resultadoRescate);
	}
	
	/**
	 * 
	 * @param esCompra
	 * @param precioActual
	 * @return
	 */
	public double calculoResultado (boolean esDescarga, boolean esCompra, double precioActual){
		
		double resultado = 0;
		
		if(esCompra)
			resultado = precioActual - precioOriginal;
		else
			resultado = precioOriginal - precioActual;

		if(estaRescatadaSemilla){
			if(resultado > resultadoRescate) 
				return resultado;
			else
			    return resultadoRescate;
			
		} 
		
		return resultado;
	}
	
}
