package pojo;

import java.util.Date;
import java.util.List;

public class MetricasPapel {
	
	private String nombrePapel = null;
	
	private List <Cotizacion>listaCotizaciones = null;
	private List <CalculoMetrica> listaCalculoMetrica_OBV = null;	
	private List <CalculoMetricaSTOCH> listaCalculoMetrica_STOCH = null;	
	private List <CalculoMetricaBand> listaCalculoMetrica_BAND = null;	
	private List <CalculoMetrica> listaCalculoMetrica_ATR = null;	
	private List <CalculoMetrica> listaCalculoMetrica_MMT_15 = null;
	private List <CalculoMetrica> listaCalculoMetrica_MMT_20 = null;
		
	public MetricasPapel(String nombrePapel,
			List<Cotizacion> listaCotizaciones,
			List<CalculoMetrica> listaCalculoMetrica_OBV,
			List<CalculoMetricaSTOCH> listaCalculoMetrica_STOCH,
			List<CalculoMetricaBand> listaCalculoMetrica_BAND,
			List<CalculoMetrica> listaCalculoMetrica_ATR,
			List<CalculoMetrica> listaCalculoMetrica_MMT_15,
			List<CalculoMetrica> listaCalculoMetrica_MMT_20) {
		super();
		this.nombrePapel = nombrePapel;
		this.listaCotizaciones = listaCotizaciones;
		this.listaCalculoMetrica_OBV = listaCalculoMetrica_OBV;
		this.listaCalculoMetrica_STOCH = listaCalculoMetrica_STOCH;
		this.listaCalculoMetrica_BAND = listaCalculoMetrica_BAND;
		this.listaCalculoMetrica_ATR = listaCalculoMetrica_ATR;
		this.listaCalculoMetrica_MMT_15 = listaCalculoMetrica_MMT_15;
		this.listaCalculoMetrica_MMT_20 = listaCalculoMetrica_MMT_20;

	}
	public String getNombrePapel() {
		return nombrePapel;
	}
	public void setNombrePapel(String nombrePapel) {
		this.nombrePapel = nombrePapel;
	}
	public List<Cotizacion> getListaCotizaciones() {
		return listaCotizaciones;
	}
	public void setListaCotizaciones(List<Cotizacion> listaCotizaciones) {
		this.listaCotizaciones = listaCotizaciones;
	}
	public List<CalculoMetrica> getListaCalculoMetrica_OBV() {
		return listaCalculoMetrica_OBV;
	}
	public void setListaCalculoMetrica_OBV(
			List<CalculoMetrica> listaCalculoMetrica_OBV) {
		this.listaCalculoMetrica_OBV = listaCalculoMetrica_OBV;
	}
	public List<CalculoMetricaSTOCH> getListaCalculoMetrica_STOCH() {
		return listaCalculoMetrica_STOCH;
	}
	public void setListaCalculoMetrica_STOCH(
			List<CalculoMetricaSTOCH> listaCalculoMetrica_STOCH) {
		this.listaCalculoMetrica_STOCH = listaCalculoMetrica_STOCH;
	}
	public List<CalculoMetricaBand> getListaCalculoMetrica_BAND() {
		return listaCalculoMetrica_BAND;
	}
	public void setListaCalculoMetrica_BAND(
			List<CalculoMetricaBand> listaCalculoMetrica_BAND) {
		this.listaCalculoMetrica_BAND = listaCalculoMetrica_BAND;
	}
	public List<CalculoMetrica> getListaCalculoMetrica_ATR() {
		return listaCalculoMetrica_ATR;
	}
	public void setListaCalculoMetrica_ATR(
			List<CalculoMetrica> listaCalculoMetrica_ATR) {
		this.listaCalculoMetrica_ATR = listaCalculoMetrica_ATR;
	}
	public List<CalculoMetrica> getListaCalculoMetrica_MMT_15() {
		return listaCalculoMetrica_MMT_15;
	}
	public void setListaCalculoMetrica_MMT_15(
			List<CalculoMetrica> listaCalculoMetrica_MMT_15) {
		this.listaCalculoMetrica_MMT_15 = listaCalculoMetrica_MMT_15;
	}
	public List<CalculoMetrica> getListaCalculoMetrica_MMT_20() {
		return listaCalculoMetrica_MMT_20;
	}
	public void setListaCalculoMetrica_MMT_20(
			List<CalculoMetrica> listaCalculoMetrica_MMT_20) {
		this.listaCalculoMetrica_MMT_20 = listaCalculoMetrica_MMT_20;
	}
		
}
