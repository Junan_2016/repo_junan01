package pojo;

import java.text.DecimalFormat;
import java.util.Date;

public class CalculoMetrica {

		private Date fecha_apertura = null;
		private double precio_cierre;
		private String indicador_nombre;
		private double indicador_valor;
		 
		public Date getFecha_apertura() {
			return fecha_apertura;
		}
		public void setFecha_apertura(Date fecha_apertura) {
			this.fecha_apertura = fecha_apertura;
		}
		public String getIndicador_nombre() {
			return indicador_nombre;
		}
		public void setIndicador_nombre(String indicador_nombre) {
			this.indicador_nombre = indicador_nombre;
		}
		public double getIndicador_valor() {
			return indicador_valor;
		}
		public void setIndicador_valor(double indicador_valor) {
			this.indicador_valor = indicador_valor;
		}
		public double getPrecio_cierre() {
			return precio_cierre;
		}
		public void setPrecio_cierre(double precio_cierre) {
			this.precio_cierre = precio_cierre;
		}	
		
		public String getIndicador_valor_formateado() {			
			DecimalFormat df = new DecimalFormat("0.00");			
			return df.format(indicador_valor);
		}	
}
