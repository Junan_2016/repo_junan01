package pojo;

import java.util.Date;

public class Operacion {

	private Date fecha;
	private double precio_cierre_operacion;
	private double resultado;
	private boolean isShort;
		
	public Operacion(Date fecha, double precio_cierre_operacion,double resultado, boolean isShort) {
		super();
		this.fecha = fecha;
		this.precio_cierre_operacion = precio_cierre_operacion;
		this.resultado = resultado;
		this.isShort = isShort;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public double getPrecio_cierre_operacion() {
		return precio_cierre_operacion;
	}
	public void setPrecio_cierre_operacion(double precio_cierre_operacion) {
		this.precio_cierre_operacion = precio_cierre_operacion;
	}
	public double getResultado() {
		return resultado;
	}
	public void setResultado(double resultado) {
		this.resultado = resultado;
	}
	public boolean isShort() {
		return isShort;
	}
	public void setShort(boolean isShort) {
		this.isShort = isShort;
	}
		
}
