package pojo;

import java.util.Date;

import util.MetodosFechas;

public class Rendimiento {

    private Date fecha;
	private String monto;
	private String porcentaje;
	private boolean esCompra;
	private long unidades_operadas = 0; 
	private int diasCargando = 0;
			
	public Rendimiento(
			Date fecha, String monto, String porcentaje, boolean esCompra,
			long unidades_operadas, int diasCargando) {
		super();
		this.fecha = fecha;
		this.monto = monto;
		this.porcentaje = porcentaje;
		this.esCompra = esCompra;
		this.unidades_operadas = unidades_operadas;
		this.diasCargando = diasCargando;
	}

	@Override
	public String toString() {
		
		String operacion = "Compra";
		if(!esCompra)operacion = "Venta";
		
		return  MetodosFechas.Date_to_String(fecha) + "- " + 
				operacion + ": " + monto + 
				"- Unid:" + unidades_operadas + 
				"- DiasCarg:" + diasCargando;
	}
	
}
