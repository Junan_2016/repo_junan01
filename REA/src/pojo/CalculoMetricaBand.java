package pojo;

public class CalculoMetricaBand extends CalculoMetrica {

	private double valor_banda_superior;
	private double valor_banda_media;
	private double valor_banda_inferior;
	
	public double getValor_banda_superior() {
		return valor_banda_superior;
	}
	public void setValor_banda_superior(double valor_banda_superior) {
		this.valor_banda_superior = valor_banda_superior;
	}
	public double getValor_banda_media() {
		return valor_banda_media;
	}
	public void setValor_banda_media(double valor_banda_media) {
		this.valor_banda_media = valor_banda_media;
	}
	public double getValor_banda_inferior() {
		return valor_banda_inferior;
	}
	public void setValor_banda_inferior(double valor_banda_inferior) {
		this.valor_banda_inferior = valor_banda_inferior;
	}
	
	
}
