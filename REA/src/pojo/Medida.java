package pojo;

import util.MetodosFechas;

public class Medida {

	double valor=0;
	Cotizacion cotizacion;
	
	public Medida(double valor, Cotizacion cotizacion) {
		super();
		this.valor = valor;
		this.cotizacion = cotizacion;
	}
	
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public Cotizacion getCotizacion() {
		return cotizacion;
	}
	public void setCotizacion(Cotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}
	
	public String getFechaString(){
		return MetodosFechas.Date_to_String(cotizacion.getFecha());
	}
		
}
