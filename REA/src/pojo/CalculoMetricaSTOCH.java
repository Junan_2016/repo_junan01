package pojo;

public class CalculoMetricaSTOCH extends CalculoMetrica {

	private double valor_main;
	private double valor_signal;
	
	public double getValor_main() {
		return valor_main;
	}
	public void setValor_main(double valor_main) {
		this.valor_main = valor_main;
	}
	public double getValor_signal() {
		return valor_signal;
	}
	public void setValor_signal(double valor_signal) {
		this.valor_signal = valor_signal;
	}
	
}
