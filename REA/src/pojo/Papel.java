package pojo;

public class Papel {
	
	private double precio_papel = 0;
	private String simbolo_papel = null;  //ej. GGAL
	
	private double precio_resistencia = 0;
	private double precio_soporte = 0;
	
	private String base_papel = null; //ej:YPFC380
	
	public Papel() {
		super();
	}
	
	
	public double getPrecio_papel() {
		return precio_papel;
	}
	public void setPrecio_papel(double precio_papel) {
		this.precio_papel = precio_papel;
	}
	
	
	public String getSimbolo_papel() {
		return simbolo_papel;
	}
	public void setSimbolo_papel(String simbolo_papel) {
		this.simbolo_papel = simbolo_papel;
	}


	public double getPrecio_resistencia() {
		return precio_resistencia;
	}


	public void setPrecio_resistencia(double precio_resistencia) {
		this.precio_resistencia = precio_resistencia;
	}


	public double getPrecio_soporte() {
		return precio_soporte;
	}


	public void setPrecio_soporte(double precio_soporte) {
		this.precio_soporte = precio_soporte;
	}


	public String getBase_papel() {
		return base_papel;
	}


	public void setBase_papel(String base_papel) {
		this.base_papel = base_papel;
	}
	
	

}
