package pojo;

import logica.ElliottData;

public class Resumen {
	
	private String simbolo_papel = null;  //ej. GGAL
	private String operacion = null; 	
	private String resultado = null; 
	private String desglose_resultado = null; 	
	private long unidades_operadas = 0; 
	private double ultimo_precio_papel = 0;
	private boolean haySegnalIngreso = false; 
	private ElliottData vElliottData;
	private String aciertos;
	private String resultadoTotal;
	private int diasCargando = 0;
		
	public String getSimbolo_papel() {
		return simbolo_papel;
	}
	public void setSimbolo_papel(String simbolo_papel) {
		this.simbolo_papel = simbolo_papel;
	}
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	public String getDesglose_resultado() {
		return desglose_resultado;
	}
	public void setDesglose_resultado(String desglose_resultado) {
		this.desglose_resultado = desglose_resultado;
	}
	public long getUnidades_operadas() {
		return unidades_operadas;
	}
	public void setUnidades_operadas(long unidades_operadas) {
		this.unidades_operadas = unidades_operadas;
	}
	public double getUltimo_precio_papel() {
		return ultimo_precio_papel;
	}
	public void setUltimo_precio_papel(double ultimo_precio_papel) {
		this.ultimo_precio_papel = ultimo_precio_papel;
	}
	public boolean haySegnalIngreso() {
		return haySegnalIngreso;
	}
	public void setSegnalIngreso(boolean resultado_final) {
		this.haySegnalIngreso = resultado_final;
	}
	public ElliottData getvElliottData() {
		return vElliottData;
	}
	public void setvElliottData(ElliottData vElliottData) {
		this.vElliottData = vElliottData;
	}
	public String getAciertos() {
		return aciertos;
	}
	public void setAciertos(String aciertos) {
		this.aciertos = aciertos;
	}
	public String getResultadoTotal() {
		return resultadoTotal;
	}
	public void setResultadoTotal(String resultadoTotal) {
		this.resultadoTotal = resultadoTotal;
	}
	public int getDiasCargando() {
		return diasCargando;
	}
	public void setDiasCargando(int diasCargando) {
		this.diasCargando = diasCargando;
	}
	
}
