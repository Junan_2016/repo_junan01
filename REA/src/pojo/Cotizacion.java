package pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Cotizacion {

	String nombrePapel = null;
	Date fecha = null;
	double precio_apertura = 0;
	double precio_maximo = 0; 
	double precio_minimo = 0;
	double precio_cierre = 0;
	double volumen = 0;
		
	public Cotizacion(){}
	
	public Cotizacion(String nombrePapel, Date fecha, double precio_apertura,
					  double precio_maximo, double precio_minimo, double precio_cierre,
					  double volumen) 
	{
		this.nombrePapel = nombrePapel;
		this.fecha = fecha;
		this.precio_apertura = precio_apertura;
		this.precio_maximo = precio_maximo;
		this.precio_minimo = precio_minimo;
		this.precio_cierre = precio_cierre;
		this.volumen = volumen;
	}
	
	public double getPrecio_maximo() {
		return precio_maximo;
	}
	public void setPrecio_maximo(double precio_maximo) {
		this.precio_maximo = precio_maximo;
	}
	
	public double getPrecio_minimo() {
		return precio_minimo;
	}
	public void setPrecio_minimo(double precio_minimo) {
		this.precio_minimo = precio_minimo;
	}
	
	public double getPrecio_cierre() {
		return precio_cierre;
	}
	public void setPrecio_cierre(double precio_cierre) {
		this.precio_cierre = precio_cierre;
	}
	
	public double getVolumen() {
		return volumen;
	}
	public void setVolumen(double volumen) {
		this.volumen = volumen;
	}
		
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public double getPrecio_apertura() {
		return precio_apertura;
	}
	public void setPrecio_apertura(double precio_apertura) {
		this.precio_apertura = precio_apertura;
	}
	
	public String getNombrePapel() {
		return nombrePapel;
	}
	public void setNombrePapel(String nombrePapel) {
		this.nombrePapel = nombrePapel;
	}
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		result.append("fecha: " + formatter.format(this.fecha) +
		" - precio_apertura: " + this.precio_apertura +
		" - precio_maximo: " + this.precio_maximo +
		" - precio_minimo: " + this.precio_minimo +
		" - precio_cierre: " + this.precio_cierre +
		" - volumen: " + this.volumen);
		
		return result.toString();
	}

	public String trazaElliott(boolean esMax) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder result = new StringBuilder();
		
		if(esMax)
			result.append(formatter.format(this.fecha) + "(Max:" + this.precio_maximo +")");
		else
			result.append(formatter.format(this.fecha) + "(Min:" + this.precio_minimo +")");
		
		return result.toString();
	}
}
