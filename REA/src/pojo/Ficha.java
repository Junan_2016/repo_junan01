package pojo;

import java.util.Date;

import util.MetodosFechas;
import util.MetodosUtiles;

public class Ficha extends Operacion {

	private boolean esSemilla = false;
	
	public Ficha(Date fecha, double precio_cierre_operacion, boolean esSemilla) {
		super(fecha, precio_cierre_operacion, 0, false);
		this.esSemilla = esSemilla;
	}

	@Override
	public String toString() {
		return "Fecha: " + MetodosFechas.Date_to_String(getFecha()) + " - PrecioFicha: " + MetodosUtiles.df.format(getPrecio_cierre_operacion());
	}

	public boolean isEsSemilla() {
		return esSemilla;
	}

	public void setEsSemilla(boolean esSemilla) {
		this.esSemilla = esSemilla;
	}
	
}
