package pojo;

import java.util.Date;

import util.MetodosFechas;
import util.MetodosUtiles;

public class FichaPiñon extends UnidadOperada {

	private int cantidadOperada;
	private double precioSalida;
		
	public FichaPiñon(Date fecha, double precioOriginal, int cantidad) {
		super(fecha, precioOriginal,null);
		this.cantidadOperada = cantidad;
	}
	
	public long getCantidad() {
		return cantidadOperada;
	}

	public void setCantidad(int cantidad) {
		this.cantidadOperada = cantidad;
	}

	public double getPrecioSalida() {
		return precioSalida;
	}

	public void setPrecioSalida(double precioSalida) {
		this.precioSalida = precioSalida;
	}

	@Override
	public String toString() {
		return "Fecha:" + MetodosFechas.Date_to_String(getFecha()) + 
			   " -Prec.Ficha:" + MetodosUtiles.df.format(super.getPrecioOriginal());
	}
	
}
