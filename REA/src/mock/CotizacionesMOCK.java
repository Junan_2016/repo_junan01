package mock;

import java.util.ArrayList;
import java.util.Calendar;

import java.util.List;

import pojo.Cotizacion;

public class CotizacionesMOCK {

	public static List <Cotizacion> getListaCotizaciones(){

	 List <Cotizacion> lista = new ArrayList <Cotizacion>();
	 Cotizacion vCotizacion = null;
		 
	 for (int j=0; j< 458; j++){
		 
		 vCotizacion = new Cotizacion();
		 vCotizacion.setPrecio_cierre(j);
		 vCotizacion.setVolumen(j*10);
		 		 
		 Calendar vCalendar = Calendar.getInstance();
		 vCalendar.set(2014, Calendar.JANUARY, 01);
		 vCalendar.add(Calendar.DATE, j);
	     vCotizacion.setFecha(vCalendar.getTime());
	     
		 lista.add(vCotizacion);
		 
	 }	
		
	 return lista;		
	}
	
	
}
